#if !defined(GAME_H)
#include "yamgine_shared.h"


#define meta_string_enum

#define STB_DEFINE

#ifdef YAMGINE_DEBUG
#define STB_DEBUG
#endif

#include "stb_dynamic_array.h"

#if FMOD_SUPPORT
#include "fmod_studio.h"
#endif

//#include "yamgine_memory_arena.h"
//#include "yamgine_profiling.h"
#include "yamgine_collision.h"

#include "yamgine_entity.h"

#include "yamgine_gui.h"

#define introspect(params)

#include "yamgine_meta.h"

#include "yamgine_network.h"
#include "game_editor.h"

#pragma warning(push)
#pragma warning(disable:4456)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#pragma warning(pop)

struct particle
{
    
    real32 DistanceFromCamera;
    v3 Position;
    v4 StartingColor;
    v4 Color;
    v3 Velocity;
    real32 TimeAlive;
    real32 AllottedLifespan;
    bool32 IsAlive;
    
};

struct gpu_particle
{
    v3 Position;
    v4 Color;
};

#define MAX_PARTICLE_COUNT 2000
struct particles
{
    particle CpuParticles[MAX_PARTICLE_COUNT];
    graphics_buffer Buffer;
};

struct game_state
{
    real32 ElapsedTime;

    entity* Player;
    entity* PlayerCamera;
    matrix4 PlayerViewProjection;
    
    entity* DirectionalLight;

    linked_list EntityMaster;
    collision_group CollisionGroup;

    render_assets RenderAssets;
    
    texture* Big1Texture;
    texture* Big2Texture;
    texture* Big3Texture;
    texture* Big4Texture;
    
    texture* ParticleTexture;
    texture* SplashScreenTexture;

    bool32 DrawBroadphaseViz;
    bool32 DrawNarrowphaseViz;

    real32 SimulationSpeed;

    bool32 GameplayStartTrigger;

    network_state NetworkState;

    bool32 DisplayingOptionsMenu;

    real32 WalkingStepFrequency;
    real32 RunningStepFrequency;
    real32 WalkingVelocityThreshold;
    real32 RunningVelocityThreshold;
    real32 LastStepTime;
    
    v2 MoveDirectionXZ;
    bool32 JumpHit;

    bool32 DisplayingSplashScreen;
    
    font Font;
    
    player_settings OptionsPlayerSettings;
    player_settings PlayerSettings;
    
    fbo GameFbo;
    fbo GameAAResolved;
    fbo FinalGameFbo;
    
    fbo PostProcessA;
    fbo PostProcessB;
    
    fbo Depthmap;
    
    bool32 QuitRequested;

    #if FMOD_SUPPORT
    FMOD_STUDIO_EVENTINSTANCE* MusicInstance;
    FMOD_STUDIO_EVENTINSTANCE* JumpInstance;
    FMOD_STUDIO_EVENTINSTANCE* StepInstance;
    FMOD_STUDIO_EVENTINSTANCE* WeaponPickupInstance;
    FMOD_STUDIO_EVENTINSTANCE* PlayerDeathInstance;
    FMOD* Fmod;
    #endif
    
    real32 KeyframeSpacing;
    real32 KeyframeNextSearchRadius;
    
    float LastFlipTime;
    
    fbo* FullScreenFbos[16];
    uint32 FullScreenFboCount;
    
    memory_arena GameArena;
    
    particles Particles;
};

// contains anything that should not be reset between game runs
struct game_and_editor_memory
{
    bool32 GameAndEditorMemoryInitialized;
    
    //editor_focus Focus;
    bool32 EditorMode;
    bool32 ShowUI;
    
    game_state GameState;
    game_state PreservedGameState;
    
    memory_arena PreservedGameStateArena;

    linked_list PreservedCollisionEntries;
    void* PreservedCollisionEntriesMemory;
    uint32 CollisionEntriesMemberCount;

    editor_state EditorState;

    real64 ElapsedTimeSinceAppStart;
};

#define GAME_H
#endif