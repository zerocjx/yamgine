#include "../platform_agnostic.h"
#include "../yamgine_math.h"

inline void
QuaternionToEulerTest(v4 QuaternionIn, v3 EulerAnglesOut)
{
    v3 Euler1 = QuaternionToEuler(QuaternionIn);
    Assert(ApproximatelyEqual(EulerAnglesOut.x, Euler1.x, 0.0001f));
    Assert(ApproximatelyEqual(EulerAnglesOut.y, Euler1.y, 0.0001f));
    Assert(ApproximatelyEqual(EulerAnglesOut.z, Euler1.z, 0.0001f));
}

inline void EulerToQuaternionTest(v3 EulerAnglesIn, v4 QuaternionOut)
{
    v4 Quat1 = EulerToQuaternion(EulerAnglesIn);
    Assert(ApproximatelyEqual(QuaternionOut.x, Quat1.x, 0.0001f));
    Assert(ApproximatelyEqual(QuaternionOut.y, Quat1.y, 0.0001f));
    Assert(ApproximatelyEqual(QuaternionOut.z, Quat1.z, 0.0001f));
    Assert(ApproximatelyEqual(QuaternionOut.w, Quat1.w, 0.0001f));
}

inline void EulerQuaternionEquivalent(v3 EulerAngles, v4 Quaternion)
{
    EulerToQuaternionTest(EulerAngles, Quaternion);
    QuaternionToEulerTest(Quaternion, EulerAngles);
}

// http://quaternions.online/ for verifications
//TODO(james): these are incorrect, gotta do these for
// Quaternion <-> Angle Axis
// Quaternion <-> Matrix4
inline void RotationTests()
{
#if 0
    // zero case
    EulerQuaternionEquivalent(V3(0, 0, 0), V4(0, 0, 0, 1));

    // x polar
    EulerQuaternionEquivalent(V3(180 * DEGREES_TO_RADIANS, 0, 0), V4(1, 0, 0, 0));

    // y polar, hit a singularity so it doesn't return 0,-180,0
    EulerToQuaternionTest(V3(0, 180 * DEGREES_TO_RADIANS, 0), V4(0, 1, 0, 0));
    QuaternionToEulerTest(V4(0, 1, 0, 0), V3(180 * DEGREES_TO_RADIANS, 0, 180 * DEGREES_TO_RADIANS));

    // z polar
    EulerQuaternionEquivalent(V3(0, 0, 180 * DEGREES_TO_RADIANS), V4(0, 0, 1, 0));

    // x 90 degrees
    EulerQuaternionEquivalent(V3(90 * DEGREES_TO_RADIANS, 0, 0), V4(0.707107f, 0, 0, 0.707107f));

    // x 45 degrees
    EulerQuaternionEquivalent(V3(45 * DEGREES_TO_RADIANS, 0, 0), V4(0.3826834f, 0, 0, 0.9238796f));

    // x -45 (315) degrees
    EulerQuaternionEquivalent(V3(-45 * DEGREES_TO_RADIANS, 0, 0), V4(-0.3826834f, 0, 0, 0.9238796f));

    // y 90 degrees
    EulerQuaternionEquivalent(V3(0, 90 * DEGREES_TO_RADIANS, 0), V4(0, 0.707107f, 0, 0.707107f));

    // z 90 degrees
    EulerQuaternionEquivalent(V3(0, 0, 90 * DEGREES_TO_RADIANS), V4(0, 0, 0.707107f, 0.707107f));
#endif
}