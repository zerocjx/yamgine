#include "../platform_agnostic.h"
#include "../yamgine_string.h"

uint32 __AssertCount = 0;
#define Assert(Expression)if (!(Expression)__AssertCount++

#define TestAssert(Expression) if (!(Expression)printf("String problem")

void CheckAssertOccurred()
{
    TestAssert(__AssertCount == 1);
    __AssertCount--;
}

void StringTests()
{
    STRING(Hello, 10);
    SetString(&Hello, "Hello");
    TestAssert(StringsAreEqualAbsolute(Hello, "Hello"));
    TestAssert(StringLength(Hello) == 5);

    STRING(T, 1);
    SetString(&T, "Overflow!");
    CheckAssertOccurred();

    SetString(&T, "T");
    TestAssert(StringLength(T) == 1);

    STRING(Empty, 0);
    TestAssert(StringLength(T) == 0);
}