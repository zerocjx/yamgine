mesh* CreateCubeMesh(memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);

    Result->MeshType = MeshType_Triangles;
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;

    C_AppendString(&Result->Path, "/Cube");
    PlatformAPI->CreateBuffer(&Result->Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, MeshResidency_Static);
    v3 CubeVertices[] = {
        //back
        { 0.5f, 0.5f, -0.5f }, //top right
        { 0.5f, -0.5f, -0.5f }, //bottom right
        { -0.5f, -0.5f, -0.5f }, //bottom left

        { -0.5f, 0.5f, -0.5f }, //top left
        { 0.5f, 0.5f, -0.5f }, //top right
        { -0.5f, -0.5f, -0.5f }, // bottom left

        //front
        { -0.5f, -0.5f, 0.5f },
        { 0.5f, -0.5f, 0.5f },
        { 0.5f, 0.5f, 0.5f },

        { 0.5f, 0.5f, 0.5f },
        { -0.5f, 0.5f, 0.5f },
        { -0.5f, -0.5f, 0.5f },

        //left
        { -0.5f, 0.5f, 0.5f },
        { -0.5f, 0.5f, -0.5f },
        { -0.5f, -0.5f, -0.5f },

        { -0.5f, -0.5f, -0.5f },
        { -0.5f, -0.5f, 0.5f },
        { -0.5f, 0.5f, 0.5f },

        //right
        { 0.5f, 0.5f, -0.5f },
        { 0.5f, 0.5f, 0.5f },
        { 0.5f, -0.5f, -0.5f },

        { 0.5f, -0.5f, 0.5f },
        { 0.5f, -0.5f, -0.5f },
        { 0.5f, 0.5f, 0.5f },
        //bottom
        { -0.5f, -0.5f, -0.5f },
        { 0.5f, -0.5f, -0.5f },
        { 0.5f, -0.5f, 0.5f },

        { 0.5f, -0.5f, 0.5f },
        { -0.5f, -0.5f, 0.5f },
        { -0.5f, -0.5f, -0.5f },
        //top
        { 0.5f, 0.5f, -0.5f },
        { -0.5f, 0.5f, -0.5f },
        { 0.5f, 0.5f, 0.5f },

        { -0.5f, 0.5f, 0.5f },
        { 0.5f, 0.5f, 0.5f },
        { -0.5f, 0.5f, -0.5f }
    };
    Result->Vertices.Count = ArrayCount(CubeVertices);
    Result->Vertices.Data = PushArray(AssetArena, Result->Vertices.Count, v3);
    for (uint32 i = 0; i < Result->Vertices.Count; i++)
    {
        ((v3*)(Result->Vertices.Data))[i] = CubeVertices[i];
    }

    //Result->HasNormals = true;
    PlatformAPI->CreateBuffer(&Result->Normals, CHANNEL_NORMAL, CHANNEL_NORMAL_WIDTH, MeshResidency_Static);
    v3 CubeNormals[] = {
        //back
        { 0, 0, -1 },
        { 0, 0, -1 },
        { 0, 0, -1 },
        { 0, 0, -1 },
        { 0, 0, -1 },
        { 0, 0, -1 },

        // front
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },

        //left
        { -1, 0, 0 },
        { -1, 0, 0 },
        { -1, 0, 0 },
        { -1, 0, 0 },
        { -1, 0, 0 },
        { -1, 0, 0 },
        //right
        { 1, 0, 0 },
        { 1, 0, 0 },
        { 1, 0, 0 },
        { 1, 0, 0 },
        { 1, 0, 0 },
        { 1, 0, 0 },

        //bottom
        { 0, -1, 0 },
        { 0, -1, 0 },
        { 0, -1, 0 },
        { 0, -1, 0 },
        { 0, -1, 0 },
        { 0, -1, 0 },

        //top
        { 0, 1, 0 },
        { 0, 1, 0 },
        { 0, 1, 0 },
        { 0, 1, 0 },
        { 0, 1, 0 },
        { 0, 1, 0 }
    };

    Result->Normals.Count = ArrayCount(CubeNormals);
    Result->Normals.Data = PushArray(AssetArena, Result->Normals.Count, v3);
    for (uint32 i = 0; i < Result->Normals.Count; i++)
    {
        ((v3*)(Result->Normals.Data))[i] = CubeNormals[i];
    }
    
    PlatformAPI->CreateBuffer(&Result->UVs, CHANNEL_UV, CHANNEL_UV_WIDTH, MeshResidency_Static);
    v2 CubeUVs[] = {
        //front
        { 1, 1 }, //topright
        { 0, 1 }, //bottomright
        { 0, 0 }, //bottomleft

        { 1, 0 }, //topleft
        { 1, 1 }, //topright
        { 0, 0 }, //bottomleft

        //back
        { 1, 1 }, //topright
        { 0, 1 }, //bottomright
        { 0, 0 }, //bottomleft

        { 0, 0 }, //bottomleft
        { 1, 0 }, //topleft
        { 1, 1 }, //topright

        //left
        { 1, 1 }, //topright
        { 0, 1 }, //bottomright
        { 0, 0 }, //bottomleft

        { 0, 0 }, //bottomleft
        { 1, 0 }, //topleft
        { 1, 1 }, //topright

        //right
        { 1, 0 },
        { 0, 0 },
        { 1, 1 },

        { 0, 1 },
        { 1, 1 },
        { 0, 0 },

        { 1, 1 }, //topright
        { 0, 1 }, //bottomright
        { 0, 0 }, //bottomleft

        { 0, 0 }, //bottomleft
        { 1, 0 }, //topleft
        { 1, 1 }, //topright

        //back
        { 1, 0 },
        { 0, 0 },
        { 1, 1 },

        { 0, 1 },
        { 1, 1 },
        { 0, 0 }
    };

    
    Result->UVs.Count = ArrayCount(CubeUVs);
    Result->UVs.Data = PushArray(AssetArena, Result->UVs.Count, v3);
    for (uint32 i = 0; i < Result->UVs.Count; i++)
    {
        ((v2*)(Result->UVs.Data))[i] = CubeUVs[i];
    }
    
    //CalculateExtents(Result);

    //PlatformAPI->AllocateMesh(Result);
    
    
    PlatformAPI->FillBuffer(&Result->Vertices);
    
    PlatformAPI->FillBuffer(&Result->Normals);
    
    PlatformAPI->FillBuffer(&Result->UVs);

    return Result;
}

mesh* CreateColliderCubeMesh(memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);

    Result->MeshType = MeshType_Triangles;
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;

    C_AppendString(&Result->Path, "/ColliderCube");
    
    PlatformAPI->CreateBuffer(&Result->Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, MeshResidency_Static);
    v3 CubeVertices[] = {
        { -0.5f, -0.5f, -0.5f }, //bottom left
        { 0.5f, -0.5f, -0.5f }, //bottom right
        { -0.5f, 0.5f, -0.5f }, //top left
        { 0.5f, 0.5f, -0.5f }, //top right

        { -0.5f, -0.5f, 0.5f }, //bottom left
        { 0.5f, -0.5f, 0.5f }, //bottom right
        { -0.5f, 0.5f, 0.5f }, //top left
        { 0.5f, 0.5f, 0.5f }, //top right

    };
    
    Result->Vertices.Count = ArrayCount(CubeVertices);
    Result->Vertices.Data = PushArray(AssetArena, Result->Vertices.Count, v3);
    for (uint32 i = 0; i < Result->Vertices.Count; i++)
    {
        ((v3*)(Result->Vertices.Data))[i] = CubeVertices[i];
    }
    
    
    PlatformAPI->FillBuffer(&Result->Vertices);

    //CalculateExtents(Result);

    return Result;
}

mesh* CreateQuadMesh(mesh_residency Residency, memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);

    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    C_SetString(&Result->Path, "/Quad");

    v3 QuadVertices[] = {
        //back
        { -0.5f, -0.5f, 0 },
        { -0.5f, 0.5f, 0 },
        { 0.5f, -0.5f, 0 },
        { -0.5f, 0.5f, 0 },
        { 0.5f, 0.5f, 0 },
        { 0.5f, -0.5f, 0 }
    };
    Result->MeshType = MeshType_Triangles;
    
    PlatformAPI->CreateBuffer(&Result->Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, MeshResidency_Static);
    Result->Vertices.Count = ArrayCount(QuadVertices);
    Result->Vertices.Data = PushArray(AssetArena, Result->Vertices.Count, v3);
    for (uint32 i = 0; i < Result->Vertices.Count; i++)
    {
        ((v3*)(Result->Vertices.Data))[i] = QuadVertices[i];
    }
    
    PlatformAPI->FillBuffer(&Result->Vertices);
    
    PlatformAPI->CreateBuffer(&Result->Normals, CHANNEL_NORMAL, CHANNEL_NORMAL_WIDTH, MeshResidency_Static);
    v3 QuadNormals[] = {
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 }
    };

    Result->Normals.Count = ArrayCount(QuadNormals);
    Result->Normals.Data = PushArray(AssetArena, Result->Normals.Count, v3);
    for (uint32 i = 0; i < Result->Normals.Count; i++)
    {
        ((v3*)(Result->Normals.Data))[i] = QuadNormals[i];
    }
    
    PlatformAPI->FillBuffer(&Result->Normals);
    
    PlatformAPI->CreateBuffer(&Result->UVs, CHANNEL_UV, CHANNEL_UV_WIDTH, MeshResidency_Static);
      v2 QuadUVs[] = {
          {0,1},
          {0,0},
          {1,1},
          {0,0},
          {1,0},
          {1,1}
      };
      
      Result->UVs.Count = ArrayCount(QuadUVs);
      Result->UVs.Data = PushArray(AssetArena, Result->UVs.Count, v2);
      for (uint32 i = 0; i < Result->UVs.Count; i++)
      {
          ((v2*)(Result->UVs.Data))[i] = QuadUVs[i];
      }
      
      
      PlatformAPI->FillBuffer(&Result->UVs);
      
    //CalculateExtents(Result);

    return Result;
}


mesh* CreateQuadVerticesMesh(mesh_residency Residency, memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);
    
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    C_SetString(&Result->Path, "/Quad");
    
    v3 QuadVertices[] = {
        //back
        { -0.5f, -0.5f, 0 },
        { -0.5f, 0.5f, 0 },
        { 0.5f, -0.5f, 0 },
        { -0.5f, 0.5f, 0 },
        { 0.5f, 0.5f, 0 },
        { 0.5f, -0.5f, 0 }
    };
    Result->MeshType = MeshType_Triangles;
    
    PlatformAPI->CreateBuffer(&Result->Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, MeshResidency_Static);
    Result->Vertices.Count = ArrayCount(QuadVertices);
    Result->Vertices.Data = PushArray(AssetArena, Result->Vertices.Count, v3);
    for (uint32 i = 0; i < Result->Vertices.Count; i++)
    {
        ((v3*)(Result->Vertices.Data))[i] = QuadVertices[i];
    }
    
    PlatformAPI->FillBuffer(&Result->Vertices);
    
    //CalculateExtents(Result);
    
    return Result;
}

#if 0
mesh* CreateTetrahedronMesh(memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);

    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    Result->MeshType = MeshType_Triangles;
    //AppendString(Result->Name, "Tetrahedron", 128);
    SetString(&Result->Path, "/Tetrahedron");

    //{-0.5f, -0.5f, -0.5f}, 0
    //{0.5f, -0.5f, -0.5f}, 1
    //{0, -0.5f, 0.5f}, 2
    //{0, 0.5f, 0}, 3

    v3 TetrahedronVertices[] = {
        //bottom
        { -0.5f, -0.5f, -0.5f }, //0
        { 0.5f, -0.5f, -0.5f }, //1
        { 0, -0.5f, 0.5f }, //2
        //back
        { 0.5f, -0.5f, -0.5f }, //1
        { -0.5f, -0.5f, -0.5f }, //0
        { 0, 0.5f, 0 }, //3
        //left
        { -0.5f, -0.5f, -0.5f }, //0
        { 0, -0.5f, 0.5f }, //2
        { 0, 0.5f, 0 }, //3
        //right
        { 0, -0.5f, 0.5f }, //2
        { 0.5f, -0.5f, -0.5f }, //1
        { 0, 0.5f, 0 } //3
    };

    Result->VertexCount = ArrayCount(TetrahedronVertices);
    Result->Vertices = PushArray(AssetArena, Result->VertexCount, v3);
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Vertices[i] = TetrahedronVertices[i];
    }

    v3 BottomLeft = V3(-0.5f, -0.5f, -0.5f);
    v3 BottomRight = V3(0.5f, -0.5f, -0.5f);
    v3 BottomFront = V3(0, -0.5f, 0.5f);
    v3 Top = V3(0, 0.5f, 0);
    v3 BackNormal = Cross(BottomLeft - Top, BottomRight - Top);
    v3 LeftNormal = Cross(BottomLeft - Top, BottomFront - Top);
    v3 RightNormal = Cross(BottomRight - Top, BottomFront - Top);
    v3 TetrahedronNormals[] = {
        //bottom
        { 0, -1, 0 },
        { 0, -1, 0 },
        { 0, -1, 0 },
        //back
        BackNormal,
        BackNormal,
        BackNormal,
        //left
        LeftNormal,
        LeftNormal,
        LeftNormal,
        //right
        RightNormal,
        RightNormal,
        RightNormal
    };

    Result->HasNormals = true;
    Result->Normals = PushArray(AssetArena, ArrayCount(TetrahedronNormals), v3);
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Normals[i] = TetrahedronNormals[i];
    }

    //CalculateExtents(Result);

    PlatformAPI->AllocateMesh(Result);

    return Result;
}

mesh* CreatePyramidMesh(memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);

    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    Result->MeshType = MeshType_Triangles;
    SetString(&Result->Path, "/Pyramid");

    v3 BackLeft = V3(-0.5f, -0.5f, -0.5f);
    v3 BackRight = V3(0.5f, -0.5f, -0.5f);
    v3 FrontLeft = V3(-0.5f, -0.5f, 0.5f);
    v3 FrontRight = V3(0.5f, -0.5f, 0.5f);
    v3 Top = V3(0, 0.5f, 0);
    v3 PyramidVertices[] = {
        //bottom
        BackLeft,
        BackRight,
        FrontLeft,
        BackRight,
        FrontLeft,
        FrontRight,
        //back
        BackLeft,
        BackRight,
        Top,
        //left
        BackLeft,
        FrontLeft,
        Top,
        //right
        BackRight,
        FrontRight,
        Top,
        //front
        FrontLeft,
        FrontRight,
        Top
    };

    Result->VertexCount = ArrayCount(PyramidVertices);
    Result->Vertices = PushArray(AssetArena, Result->VertexCount, v3);
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Vertices[i] = PyramidVertices[i];
    }

    v3 TopToBackLeft = Top - BackLeft;
    v3 TopToBackRight = Top - BackRight;
    v3 TopToFrontLeft = Top - FrontLeft;
    v3 TopToFrontRight = Top - FrontRight;

    Result->HasNormals = true;

    v3 BackNormal = Cross(TopToBackRight, TopToBackLeft);
    v3 RightNormal = Cross(TopToFrontRight, TopToBackRight);
    v3 LeftNormal = Cross(TopToBackLeft, TopToFrontLeft);
    v3 FrontNormal = Cross(TopToFrontLeft, TopToFrontRight);
    v3 PyramidNormals[] = {
        //bottom
        v3_DOWN,
        v3_DOWN,
        v3_DOWN,
        v3_DOWN,
        v3_DOWN,
        v3_DOWN,
        //back
        BackNormal,
        BackNormal,
        BackNormal,
        //left
        LeftNormal,
        LeftNormal,
        LeftNormal,
        //right
        RightNormal,
        RightNormal,
        RightNormal,
        //front
        FrontNormal,
        FrontNormal,
        FrontNormal
    };

    Result->Normals = PushArray(AssetArena, ArrayCount(PyramidNormals), v3);
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Normals[i] = PyramidNormals[i];
    }

    //CalculateExtents(Result);

    PlatformAPI->AllocateMesh(Result);

    return Result;
}

#define GRID_SIZE 100 //equal to the count of lines in either direction
#define GRID_SIZE_HALF GRID_SIZE / 2
mesh* CreateXZGrid(memory_arena* MemoryArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(MemoryArena, mesh);
    Result->MeshType = MeshType_Lines;

    uint32 VertexCount = GRID_SIZE * 4 + 4;

    real32 MinPosition = -(GRID_SIZE_HALF); //go from -0.5 to +0.5
    v3* Vertices = PushArray(MemoryArena, VertexCount, v3); //start+end, doubled because horizontal and vertical
    uint32 VertexIndex = 0;
    while (VertexIndex < VertexCount)
    {
        real32 Position = MinPosition + VertexIndex / 4;
        Vertices[VertexIndex] = V3(Position, 0, -GRID_SIZE_HALF);
        ++VertexIndex;
        Vertices[VertexIndex] = V3(Position, 0, GRID_SIZE_HALF);
        ++VertexIndex;

        Vertices[VertexIndex] = V3(-GRID_SIZE_HALF, 0, Position);
        ++VertexIndex;
        Vertices[VertexIndex] = V3(GRID_SIZE_HALF, 0, Position);
        ++VertexIndex;
    }

    Result->Vertices = Vertices;
    Result->VertexCount = VertexCount;
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));

    Result->Colors = PushArray(MemoryArena, VertexCount, v4);
    for (uint32 ColorIndex = 0;
         ColorIndex < VertexCount;
         ++ColorIndex)
    {
        Result->Colors[ColorIndex] = v4_WHITE;
    }
    Result->HasColors = true;

    PlatformAPI->AllocateMesh(Result);

    return Result;
}

mesh* CreateDynamicLinesMesh(memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);
    
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    SetString(&Result->Path, "/LinesMesh");
    
    Result->MeshType = MeshType_Triangles;
    Result->VertexCount = 0;
    
    Result->Residency = MeshResidency_Stream;
    
    Result->HasNormals = false;
    Result->HasUVs = false;
    
    Result->HasColors = true;
    
    Result->IsIndexedMesh = true;
    
    #if 0
    Result->Vertices = (v3*)PushArray(AssetArena, 1500*3, float);
    Result->VertexCount = 1500;
    
    Result->Colors = (v4*)PushArray(AssetArena, 1500*4, float);
    
    Result->Elements = (uint32*)PushArray(AssetArena, 3000, float);
    Result->ElementCount = 3000;
    #endif
    PlatformAPI->AllocateMesh(Result);
    
    return Result;
}

#endif

#define RESOLUTION 100
mesh* CircleOutlineMesh(memory_arena* AssetArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(AssetArena, mesh);
    PushString(&Result->Path, AssetArena, 128);
    
    PlatformAPI->CreateBuffer(&Result->Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, MeshResidency_Static);
    
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    C_SetString(&Result->Path, "/CircleOutline");
    
     v3 NewCircleVerts[RESOLUTION];
    real32 Step = (PI * 2) / RESOLUTION;
    v3 Perpendicular = v3_RIGHT;
    for (int Index = 0;
         Index < RESOLUTION;
         Index++)
    {
        v4 Quat = AngleAxisToQuaternion(Step * (Index), v3_FORWARD);
        v3 Offset = RotateVectorByQuaternion(Perpendicular, Quat);
        v3 NewPoint = Offset;
        NewCircleVerts[Index] = NewPoint;
    };
    
    Result->Vertices.Data = (v3*)PushArray(AssetArena, RESOLUTION * 3, float);
    Result->Vertices.Count = RESOLUTION;
    
    for (int i = 0; i < RESOLUTION; i++)
    {
        ((v3*)Result->Vertices.Data)[i] = NewCircleVerts[i];
    }
    
    Result->MeshType = MeshType_LineLoop;
    
    PlatformAPI->FillBuffer(&Result->Vertices);
    
    return Result;
}

void LoadStandardAssets(render_assets* Assets, memory_arena* AssetArena, platform_api* PlatformAPI)
{
    standard_assets Result = {};

    Result.Cube = CreateCubeMesh(AssetArena, PlatformAPI);
    AddMeshAsset(Assets, Result.Cube);
    Result.ColliderCube = CreateColliderCubeMesh(AssetArena, PlatformAPI);
    AddMeshAsset(Assets, Result.ColliderCube);
    Result.Quad = CreateQuadMesh(MeshResidency_Static, AssetArena, PlatformAPI);
    Result.QuadVertices = CreateQuadVerticesMesh(MeshResidency_Static, AssetArena, PlatformAPI);
    AddMeshAsset(Assets, Result.Quad);
    Result.CircleOutline = CircleOutlineMesh(AssetArena, PlatformAPI);
    AddMeshAsset(Assets, Result.CircleOutline);
    #if 0
    Result.Tetrahedron = CreateTetrahedronMesh(AssetArena, PlatformAPI);
    AddMeshAsset(Assets, Result.Tetrahedron);
    Result.Pyramid = CreatePyramidMesh(AssetArena, PlatformAPI);
    AddMeshAsset(Assets, Result.Pyramid);
    
    Result.Grid = CreateXZGrid(AssetArena, PlatformAPI);
#endif
    
    Assets->StandardAssets = Result;
}

//
//
// TEST MESH TYPES
//
//

#if 0
mesh* CreateTriangleStrip(memory_arena* MemoryArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(MemoryArena, mesh);

    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    Result->MeshType = MeshType_TriangleStrip;

    v3 TriangleStripVertices[] = {
        V3(0, 0, 0),
        V3(0, 1, 0),
        V3(1, 0, 0),
        V3(1.5f, 1.0f, 0)
    };
    Result->Vertices = PushArray(MemoryArena, 4, v3);
    Result->VertexCount = 4;
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Vertices[i] = TriangleStripVertices[i];
    }
    PlatformAPI->AllocateMesh(Result);

    return Result;
}

mesh* CreateTriangleStripAdj(memory_arena* MemoryArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(MemoryArena, mesh);
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = true;
    Result->MeshType = MeshType_TriangleStrip_Adj;

    v3 TriangleStripVertices[] = {
        V3(0, 0, 0),
        V3(0, 1, 0),
        V3(1, 0, 0),
        V3(1.5f, 1.0f, 0)
    };
    Result->Vertices = PushArray(MemoryArena, ArrayCount(TriangleStripVertices), v3);
    Result->VertexCount = ArrayCount(TriangleStripVertices);
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Vertices[i] = TriangleStripVertices[i];
    }

    uint32 Elements[] = {
        0, 0, 1, 3, 2, 2,
        3, 3
    };
    Result->Elements = PushArray(MemoryArena, ArrayCount(Elements), uint32);
    Result->ElementCount = ArrayCount(Elements);
    for (uint32 i = 0; i < Result->ElementCount; i++)
    {
        Result->Elements[i] = Elements[i];
    }
    PlatformAPI->AllocateMesh(Result);

    return Result;
}

mesh* CreateTriangleFan(memory_arena* MemoryArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(MemoryArena, mesh);
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = false;
    Result->MeshType = MeshType_TriangleFan;

    v3 TriangleFanVertices[] = {
        V3(0, 0, 0),
        V3(1, 1, 0),
        V3(-1, 1, 0),
        V3(-1, -1, 0),
        V3(1, -1, 0),
        V3(1, 1, 0)
    };
    Result->Vertices = PushArray(MemoryArena, ArrayCount(TriangleFanVertices), v3);
    Result->VertexCount = ArrayCount(TriangleFanVertices);
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Vertices[i] = TriangleFanVertices[i];
    }
    PlatformAPI->AllocateMesh(Result);

    return Result;
}

mesh* CreateQuadAdjacency(memory_arena* MemoryArena, platform_api* PlatformAPI)
{
    mesh* Result = PushStruct(MemoryArena, mesh);
    TransformTRSLocal(Result, V3(0, 0, 0), v4_ZERO_ROTATION, V3(1, 1, 1));
    Result->IsIndexedMesh = true;
    Result->MeshType = MeshType_TriangleList_Adj;

    v3 Vertices[] = {
        V3(-0.5f, 0.5f, 0),
        V3(-0.5f, -0.5f, 0),
        V3(0.5f, -0.5f, 0),
        V3(0.5f, 0.5f, 0)
    };
    Result->Vertices = PushArray(MemoryArena, 4, v3);
    Result->VertexCount = 4;
    for (uint32 i = 0; i < Result->VertexCount; i++)
    {
        Result->Vertices[i] = Vertices[i];
    }

    uint32 Elements[] = {
        0, 0, 1, 1, 2, 3,
        0, 1, 2, 2, 3, 3
    };
    /*uint32 Elements[] = {
        0,1,2,
        0,2,3
    };*/
    Result->Elements = PushArray(MemoryArena, ArrayCount(Elements), uint32);
    Result->ElementCount = ArrayCount(Elements);
    for (uint32 i = 0; i < Result->ElementCount; i++)
    {
        Result->Elements[i] = Elements[i];
    }
    PlatformAPI->AllocateMesh(Result);

    return Result;
}



void PushToLinesMesh(memory_arena* DynamicLinesVerts, v3 A, v3 B)
{
}
#endif