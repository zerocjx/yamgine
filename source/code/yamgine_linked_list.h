struct linked_list_member
{
    void* Payload;
    linked_list_member* NextNode;
    linked_list_member* PrevNode;
};

struct linked_list
{
    linked_list_member* MemberMemory;

    uint32 MemoryEntriesCount;
    linked_list_member* FirstMember;

    linked_list_member* FirstFreeMember;

    //NOTE(james): sometimes we want a linked list of stuff that already lives in memory, IE a list
    //of all collision entities. Entities already live in memory elsewhere.
    //
    //However sometimes we want a list that has pointers to valid memory thats already been allocated
    //IE our entity list. Max num of entities is allocated at start with list initialization
    bool32 HasMemoryOwnership;
    void* MemberBodyMemory; //ptr to memory of entries when HasMemoryOwnship=true
    uint32 MemberSize;
};

//memory should be a pointer to memory for the copy to live in, so
//sizeof(linked_list_member) * List->MemoryEntriesCount
internal void
CopyLinkedList(linked_list* SourceList, linked_list* DestList,
               void* SourceMemory, void* DestMemory)
{
    Copy(sizeof(linked_list_member) * SourceList->MemoryEntriesCount, SourceMemory, DestMemory);
    *DestList = *SourceList;
}

inline uint32
CountListMembers(linked_list_member* Node)
{
    uint32 Count = 1;
    while (Node->NextNode != NULL)
    {
        Count++;
        Node = Node->NextNode;
    }
    return Count;
}

internal void
CheckForLoops(linked_list_member* FirstNode)
{
    if (FirstNode == NULL)
    {
        return;
    }

    // floyd's cycle finding, one pointer moves one at a time, other two at a time
    // if they are ever equal then we have a cycle
    linked_list_member* Slow = FirstNode;
    linked_list_member* Fast = FirstNode;
    while (Slow->NextNode != NULL)
    {
        Slow = Slow->NextNode;
        if (Fast->NextNode != NULL)
        {
            if (Fast->NextNode->NextNode != NULL)
            {
                Fast = Fast->NextNode->NextNode;
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }

        if (Slow == Fast)
        {
            Assert(0); //found loop!
        }
    }
}

void ClearLinkedList(linked_list* List)
{
    uint32 ListSize = List->MemoryEntriesCount;
    List->MemberMemory[0] = {};
    List->MemberMemory[0].NextNode = &List->MemberMemory[1];
    List->MemberMemory[0].PrevNode = NULL; //&List->Memory[ListSize - 1];
    for (uint32 AssetIndex = 1;
         AssetIndex < ListSize - 1;
         ++AssetIndex)
    {
        linked_list_member* Member = &List->MemberMemory[AssetIndex];
        *Member = {};
        Member->NextNode = &List->MemberMemory[AssetIndex + 1];
        Member->PrevNode = &List->MemberMemory[AssetIndex - 1];
    }
    List->MemberMemory[ListSize - 1] = {};
    List->MemberMemory[ListSize - 1].NextNode = NULL; //&List->MemberMemory[0];
    List->MemberMemory[ListSize - 1].PrevNode = &List->MemberMemory[ListSize - 2];

    List->FirstFreeMember = &List->MemberMemory[0];

    List->FirstMember = NULL;

    CheckForLoops(List->FirstFreeMember);
}

inline void
InitLinkedList(linked_list* List, memory_arena* Arena, uint32 ListSize,
               bool32 OwnMemory = false, uint32 MemberSize = 0)
{
    YamProf(InitLinkedList);
    List->MemberMemory = (linked_list_member*)PushArray(Arena, ListSize, linked_list_member);
    List->MemoryEntriesCount = ListSize;
    ClearLinkedList(List);

    if (OwnMemory)
    {
        List->HasMemoryOwnership = true;
        List->MemberBodyMemory = PushSize_(Arena, ListSize * MemberSize);
        List->MemberSize = MemberSize;

        for (uint32 MemberIndex = 0;
             MemberIndex < ListSize;
             ++MemberIndex)
        {
            uint8* PayloadBody = ((uint8*)List->MemberBodyMemory + (MemberIndex * MemberSize));
            List->MemberMemory[MemberIndex].Payload = PayloadBody;
        }
    }
}

inline linked_list_member*
AddToList(linked_list* List, void* Payload)
{
    Assert(!List->HasMemoryOwnership);

    CheckForLoops(List->FirstFreeMember);
    linked_list_member* NewMember = List->FirstFreeMember;
    List->FirstFreeMember = List->FirstFreeMember->NextNode;
    List->FirstFreeMember->PrevNode = NULL;
    NewMember->PrevNode = NULL;
    NewMember->NextNode = NULL;

    CheckForLoops(List->FirstFreeMember);

    NewMember->Payload = Payload;
    if (List->FirstMember == NULL)
    {
        List->FirstMember = NewMember;
    }
    else
    {
        List->FirstMember->PrevNode = NewMember;
        NewMember->NextNode = List->FirstMember;
        List->FirstMember = NewMember;
    }

    CheckForLoops(List->FirstMember);
    CheckForLoops(List->FirstFreeMember);

    return NewMember;
}

inline linked_list_member*
GetFromList(linked_list* List)
{
    Assert(List->HasMemoryOwnership);

    linked_list_member* PoppedMember = List->FirstFreeMember;
    List->FirstFreeMember = List->FirstFreeMember->NextNode;
    List->FirstFreeMember->PrevNode = NULL;
    PoppedMember->PrevNode = NULL;
    PoppedMember->NextNode = NULL;

    CheckForLoops(List->FirstFreeMember);

    if (List->FirstMember == NULL)
    {
        List->FirstMember = PoppedMember;
    }
    else
    {
        List->FirstMember->PrevNode = PoppedMember;
        PoppedMember->NextNode = List->FirstMember;
        List->FirstMember = PoppedMember;
    }

    CheckForLoops(List->FirstMember);
    CheckForLoops(List->FirstFreeMember);

    return PoppedMember;
}

inline void RemoveFromList(linked_list* List, linked_list_member* Member)
{
    if (List->HasMemoryOwnership)
    {
        //TODO(james): zero member here?
        //ZeroSize(List->MemberSize, Member->Payload);
    }
    else
    {
        Member->Payload = 0;
    }

    if (Member == List->FirstMember)
    {
        List->FirstMember = Member->NextNode;
        Member->NextNode->PrevNode = NULL;
    }
    else
    {
        Member->PrevNode->NextNode = Member->NextNode;
        Member->NextNode->PrevNode = Member->PrevNode;
    }

    List->FirstFreeMember->PrevNode = Member;
    Member->NextNode = List->FirstFreeMember;
    List->FirstFreeMember = Member;
    List->FirstFreeMember->PrevNode = NULL;

    CheckForLoops(List->FirstMember);
    CheckForLoops(List->FirstFreeMember);
}

#define PROGRESS_LINKED_LIST(a, b, c) Progress(a, &b, (void**)&c)

bool32 Progress(linked_list* Master, linked_list_member** Member, void** Payload)
{
    bool32 Result = false;
    
    if (*Member == NULL)
    {
        *Member = Master->FirstMember;
        if (*Member != NULL)
        {
            *Payload = (*Member)->Payload;
            Result = true;
        }
        else
        {
            Assert(0);//linked list is empty?!
        }
    }
else
    {
        *Member = (*Member)->NextNode;
        if (*Member != NULL)
        {
            *Payload = (*Member)->Payload;
            Result = true;
        }
    }
    

    return Result;
}
