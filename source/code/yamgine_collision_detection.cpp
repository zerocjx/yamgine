#include "yamgine_collision.h"

void UpdateWorldspaceVertices(entity* Entity)
{
    graphics_buffer* Positions = &Entity->ColliderMesh->Vertices;
    for (uint32 VertexIndex = 0;
         VertexIndex < Positions->Count;
         ++VertexIndex)
    {
        v3 LocalPos = GET_V3(Positions, VertexIndex);
        Entity->WorldspaceVertices[VertexIndex] = MatrixByPosition(&Entity->ColliderMeshInstanceSpace, LocalPos);
    }
}

void PushAndUpdateWorldspaceVertices(entity* Entity, memory_arena* Arena)
{

    Entity->WorldspaceVertices = PushArray(Arena, Entity->ColliderMesh->Vertices.Count, v3);
    UpdateWorldspaceVertices(Entity);
}

inline bool32
SphereSphereIntersects(entity* A, entity* B, contact_info* Contact)
{
    bool32 Result = false;
    Assert(0);

#if 0
    Assert(A->ColliderType == ColliderType_sphere);
    Assert(B->ColliderType == ColliderType_sphere);
    
    //static / static
    if (LengthSq(A->LinearVelocity) == 0 && LengthSq(B->LinearVelocity) == 0)
    {
        float DistanceBetween = Length(A->Position - B->Position);
        if (DistanceBetween <= (A->SphereColliderRadius) + 
            (B->SphereColliderRadius))
        {
            Result = true;
            // TODO(james): contact info

        }
    }
    // static / dynamic
    else if ((LengthSq(A->LinearVelocity) == 0 && LengthSq(B->LinearVelocity) > 0) ||
             (LengthSq(A->LinearVelocity) > 0 && LengthSq(B->LinearVelocity) > 0))
    {
        if (LengthSq(A->LinearVelocity) == 0)
        {
            Result = SphereStaticSphereDynamicIntersects(A, B, Contact);
        }
        else
        {
            Result = SphereStaticSphereDynamicIntersects(B, A, Contact);
        }
    }
    //dynamic / dynamic
    else
    {
        
    }
#endif
    return Result;
}

inline bool32
CheckLayerCollision(collision_group* Group, collision_layer A, collision_layer B)
{
    bool32 Result = false;
    if (Group->CollisionRules[A * CollisionLayer_Count + B])
    {
        Result = true;
    }
    return Result;
}

internal void
DrawBroadphaseDebug(render_list* RenderList,
                    entity* Entity,
                    v4 Color)
{
    DrawAABB(RenderList, &Entity->ColliderAABB, Color);
}

inline bool32
AABBPairIntersects(aabb* A, aabb* B)
{
    if (AbsoluteValue(A->Position.x - B->Position.x) > (A->HalfWidths.x + B->HalfWidths.x))
    {
        return false;
    }
    if (AbsoluteValue(A->Position.y - B->Position.y) > (A->HalfWidths.y + B->HalfWidths.y))
    {
        return false;
    }
    if (AbsoluteValue(A->Position.z - B->Position.z) > (A->HalfWidths.z + B->HalfWidths.z))
    {
        return false;
    }

    return true;
}

inline bool32
IsPointInsideAABB(aabb* A, v3 Point)
{
    bool32 Result = false;

    if (Point.x > (A->Position.x - A->HalfWidths.x) && Point.x < (A->Position.x + A->HalfWidths.x) && Point.y > (A->Position.y - A->HalfWidths.y) && Point.y < (A->Position.y + A->HalfWidths.y) && Point.z > (A->Position.z - A->HalfWidths.z) && Point.z < (A->Position.z + A->HalfWidths.z))
    {
        Result = true;
    }

    return Result;
}

inline bool32
AABBPairIntersects(render_list* RenderList, aabb* A, aabb* B)
{
    if (AABBPairIntersects(A, B))
    {
        DrawAABB(RenderList, A, v4_RED);
        DrawAABB(RenderList, B, v4_RED);
        return true;
    }
    else
    {
        DrawAABB(RenderList, A, v4_BLACK);
        DrawAABB(RenderList, B, v4_BLACK);
        return false;
    }
}

internal bool32
BroadphaseCollision(collision_group* Group,
                    entity* A, entity* B)
{
    bool32 Result = false;

    //NOTE(james): if a trigger is static, it shouldn't register a collision with a static collider within it
    //IE if we have a death pit collider, it shouldn't trigger against the walls surrounding it
    bool32 TriggerCollisionA = (A->IsTrigger && A->IsStatic) && !B->IsTrigger && B->IsStatic;
    bool32 TriggerCollisionB = (B->IsTrigger && B->IsStatic) && !A->IsTrigger && A->IsStatic;
    bool32 TriggerCollision = TriggerCollisionA || TriggerCollisionB;

    if (CheckLayerCollision(Group, A->CollisionLayer, B->CollisionLayer) && A->Enabled && B->Enabled && !TriggerCollision)
    {
        Result = AABBPairIntersects(&A->ColliderAABB, &B->ColliderAABB);
    }

    if (Group->BroadphaseViz)
    {
        if (Result)
        {
            DrawBroadphaseDebug(Group->RenderList, A, v4_GREEN);
        }
        else
        {
            //DrawBroadphaseDebug(Group->RenderList, B, v4_BLACK);
        }
    }

    return Result;
}

simplex_triangle SimplexTriangle(simplex_point A, simplex_point B, simplex_point C)
{
    simplex_triangle Result;
    Result.A = A;
    Result.B = B;
    Result.C = C;

    return Result;
}

inline void
CheckTriangleIntegrity(triangle Tri)
{

    Assert(Tri.A != Tri.B);
    Assert(Tri.A != Tri.C);
    Assert(Tri.B != Tri.C);

    /*
    real32 Dot0 = Inner((Tri.B - Tri.A), (Tri.C - Tri.A));
    real32 Dot1 = Inner((Tri.A - Tri.B), (Tri.C - Tri.B));
    real32 Dot2 = Inner((Tri.A - Tri.C), (Tri.B - Tri.C));
    Assert(Dot0 != 1 && Dot0 != -1);
    Assert(Dot1 != 1 && Dot1 != -1);
    Assert(Dot2 != 1 && Dot2 != -1);
    */
}

triangle SimplexTriangleToTriangle(simplex_triangle Tri)
{
    triangle Result;
    Result.A = Tri.A.Dilation;
    Result.B = Tri.B.Dilation;
    Result.C = Tri.C.Dilation;
    return Result;
}

void CheckTriangleIntegrity(simplex_triangle Tri)
{
    CheckTriangleIntegrity(SimplexTriangleToTriangle(Tri));
}

bool32 NoMatchingPoints(v3 A, v3 B, v3 C)
{
    return (A == B || B == C || A == C);
}

/*
To do collision with other shapes, like a sphere, or capsule, which would (hopefully) not be made of
100+ vertexmeshes, we would just need to create a ClosestPointToDirection for them
*/

//collision doc http://www.cs.sjsu.edu/faculty/pollett/masters/Semesters/Spring12/josh/GJK.html
//TODO(james): check out http://allenchou.net/2014/02/game-physics-implementing-support-function-for-polyhedrons-using-half-edges/ for a more efficient search

internal v3
ClosestPointToDirectionMesh(entity* Entity, v3 Direction)
{
    //YamProf(ClosestPointToDirMesh_New);
    v3 Result = V3(INFINITY, INFINITY, INFINITY);
    real32 ClosestInner = -INFINITY;

    for (uint32 VertexIndex = 0;
         VertexIndex < Entity->ColliderMesh->Vertices.Count;
         ++VertexIndex)
    {
        v3 Pos = Entity->WorldspaceVertices[VertexIndex];
        real32 InnerProduct = Inner(Direction, Pos);
        if (InnerProduct > ClosestInner)
        {
            ClosestInner = InnerProduct;
            Result = Pos;
        }
    }

    Assert(Result != V3(INFINITY, INFINITY, INFINITY));

    return Result;
}

void GetCapsuleNodes(entity* Entity, /*out*/ v3* TopNode, /*out*/ v3* BottomNode)
{
    real32 HalfHeight = (Entity->CapsuleColliderHeight / 2);
    real32 NodeOffset = HalfHeight - Entity->CapsuleColliderRadius;

    transform* Config = &Entity->Configs[Entity->TargetConfig];
    *TopNode = GetWorldPositionFromLocalOffset(Config, V3(0, NodeOffset, 0));
    *BottomNode = GetWorldPositionFromLocalOffset(Config, V3(0, -NodeOffset, 0));
}

inline v3
ClosestPointToDirectionCylinder(v3 Position, real32 Height, real32 Radius, v3 Up, v3 Direction,
                                bool32* HitEndCap = 0)
{
    v3 Result;

    v3 n = Cross(Direction, Up);
    //v3 RayBaseToCylinderBase = Ray.Position - Position; // RC
    real32 LengthN = Length(n);
    if (LengthN == 0) // cylinder and ray are parallel
    {
        Result = Position + (Direction * (Height * 0.5f));
        if (HitEndCap != 0)
        {
            *HitEndCap = true;
        }
    }
    else
    {
        n = Normalize(n);

        //v3 O = Cross(Direction, Up);
        //real32 t = -Inner(O, n) / LengthN;
        v3 O = Normalize(Cross(n, Up));

        real32 Dist = AbsoluteValue(SquareRoot(Radius * Radius) / Inner(Direction, O));

        //final answer for an infinite cylinder
        //v3 InfiniteCylinderSideIntersectionPosition = Entity->Position + (Direction * s);
        //
        // Handle End Caps
        //

        v3 TopCapPosition = Position + (Up * (Height / 2));
        v3 BottomCapPosition = Position + (-Up * (Height / 2));

        plane Top = PlaneFromPositionAndNormal(TopCapPosition, -Up);
        plane Bottom = PlaneFromPositionAndNormal(BottomCapPosition, Up);

        real32 DC;
        real32 DW;
        //bottom cap
        {
            DC = (Bottom.A * Direction.x) + (Bottom.B * Direction.y) + (Bottom.C * Direction.z);
            DW = (Bottom.A * Position.x) + (Bottom.B * Position.y) + (Bottom.C * Position.z) + Bottom.D;
            if (!(DC == 0.0f && DW >= 0.0f)) // make sure we aren't parallel
            {
                real32 T = -DW / DC;
                if (DC < 0.0f)
                {
                    if (T < Dist)
                    {
                        Dist = T; //disabled for capsule
                        if (HitEndCap != 0)
                        {
                            *HitEndCap = true;
                        }
                    }
                }
            }
        }
        //top cap
        {
            DC = (Top.A * Direction.x) + (Top.B * Direction.y) + (Top.C * Direction.z);
            DW = (Top.A * Position.x) + (Top.B * Position.y) + (Top.C * Position.z) + Top.D;
            if (!(DC == 0.0f && DW >= 0.0f)) // make sure we aren't parallel
            {
                real32 T = -DW / DC;
                if (DC < 0.0f)
                {
                    if (T < Dist)
                    {
                        Dist = T; //disabled for capsule
                        if (HitEndCap != 0)
                        {
                            *HitEndCap = true;
                        }
                    }
                }
            }
        }

        Result = Position + (Direction * Dist);
    }

    return Result;
}

inline v3
ClosestPointToDirectionCylinder(entity* Entity, v3 Direction)
{
    v3 Result;
    Result = ClosestPointToDirectionCylinder(Entity->Configs[Entity->TargetConfig].Position,
                                             Entity->CylinderColliderHeight,
                                             Entity->CylinderColliderRadius,
                                             Entity->Up,
                                             Direction);
    return Result;
}

/*Test Rendering

    v3 RayDir = Normalize(V3(0.1f,Sin(Memory->ElapsedTimeSinceAppStart),0.1f));
    v3 Duh = ClosestPointToDirectionCapsule(GameState->Player, RayDir);
    DrawRay(RenderList, GameState->Player->Position, Duh, v4_RED);
    DrawRay(RenderList, GameState->Player->Position, RayDir, v4_YELLOW);
    
*/
inline v3
ClosestPointToDirectionCapsule(entity* Entity, v3 Direction)
{
    real32 Radius = Entity->CapsuleColliderRadius;
    real32 CylinderHeight = Entity->CapsuleColliderHeight - (Radius * 2);
    bool32 HitEndCap = false;
    v3 EntityPosition = Entity->Configs[Entity->TargetConfig].Position;
    v3 CylinderResult = ClosestPointToDirectionCylinder(EntityPosition,
                                                        CylinderHeight,
                                                        Radius, Entity->Up, Direction, &HitEndCap);
    v3 TopNode = {};
    v3 BottomNode = {};
    GetCapsuleNodes(Entity, &TopNode, &BottomNode);
    v3 SphereResult = {};
    if (Inner(Direction, TopNode - EntityPosition) > 0)
    {
        // Raycast against the sphere, but take the position on the far side of the sphere
        v3 L = EntityPosition - TopNode;
        real32 a = Inner(Direction, Direction);
        real32 b = 2 * Inner(Direction, L);
        real32 c = Inner(L, L) - (Radius * Radius);
        real32 t0, t1, t;
        if (SolveQuadratic(a, b, c, &t0, &t1))
        {
            if (t0 > 0 || t1 > 0)
            {
                if (t0 < 0)
                {
                    t = t1;
                }
                else if (t1 < 0)
                {
                    t = t0;
                }
                else if (t0 > t1)
                {
                    t = t0;
                }
                else
                {
                    t = t1;
                }

                SphereResult = EntityPosition + (Direction * t);
            }
        }
    }
    else
    {
        v3 L = EntityPosition - BottomNode;
        real32 a = Inner(Direction, Direction);
        real32 b = 2 * Inner(Direction, L);
        real32 c = Inner(L, L) - (Radius * Radius);
        real32 t0, t1, t;
        if (SolveQuadratic(a, b, c, &t0, &t1))
        {
            if (t0 > 0 || t1 > 0)
            {
                if (t0 < 0)
                {
                    t = t1;
                }
                else if (t1 < 0)
                {
                    t = t0;
                }
                else if (t0 > t1)
                {
                    t = t0;
                }
                else
                {
                    t = t1;
                }

                SphereResult = EntityPosition + (Direction * t);
            }
        }
    }

    v3 Result = {};
    if (!HitEndCap)
    {
        Result = CylinderResult;
    }
    else
    {
        Result = SphereResult;
    }

    return Result;
}

internal v3
ClosestPointToDirection(entity* Entity, v3 Direction)
{
    v3 Result = {};
    switch (Entity->ColliderType)
    {
        case ColliderType_Mesh:
        {
            Result = ClosestPointToDirectionMesh(Entity, Direction);
        }
        break;
        case ColliderType_Sphere:
        {
            Result = Entity->Configs[Entity->TargetConfig].Position + (Direction * Entity->SphereColliderRadius);
        }
        break;

        case ColliderType_Capsule:
        {
            Result = ClosestPointToDirectionCapsule(Entity, Direction);
        }
        break;

        case ColliderType_Cylinder:
        {
            Result = ClosestPointToDirectionCylinder(Entity, Direction);
        }
        break;

            InvalidDefaultCase;
    }

    Assert(Result != v3_ZERO);
    return Result;
}

internal simplex_point
FindSupport(collision_pair* CollisionPair, //read only
            v3 Direction)
{
    //Assert(Direction != v3_ZERO);
    YamProf(Collision_FindSupport);
    //calc

    //TODO(james): should this be normalized?
    //Direction = Normalize(Direction);

    simplex_point Result = {};

    Result.PointA = ClosestPointToDirection(CollisionPair->A,
                                            Direction);
    Result.PointB = ClosestPointToDirection(CollisionPair->B,
                                            -Direction);
    Result.Dilation = Result.PointA - Result.PointB;

    return Result;
}

void GJK_EvolveSimplexLine(simplex* Simplex, v3* Direction)
{
    Assert(*Direction != v3_ZERO);
    Assert(Simplex->Count == 2);

    v3 A0 = -Simplex->A.Dilation;
    v3 AB = Simplex->B.Dilation - Simplex->A.Dilation;

    if (Inner(AB, A0) >= 0)
    {
        //tripleCrossProduct(ab, a0, ab); //calculate perpendicular to existing simplex
        v3 NewDirection = TripleCrossProduct(AB, A0, AB);
        if (LengthSq(NewDirection) == 0)
        {
            //this means a0 and ab are aligned, so use the 2D perpendicular trick
            *Direction = V3(-AB.y, AB.z, -AB.x); //just give it some other direction
            Normalize(*Direction);
        }
        else
        {
            *Direction = NewDirection;
        }
        Simplex->C = Simplex->B;
        Simplex->B = Simplex->A;
    }
    else
    {
        *Direction = A0;
        Simplex->B = Simplex->A;
        Simplex->Count = 1;
    }
}

//simplex, dir
void GJK_EvolveSimplexTriangle(simplex* Simplex,
                               v3* Direction)
{
    Assert(Simplex->Count == 3);

    v3 A0 = -Simplex->A.Dilation;
    v3 AB = Simplex->B.Dilation - Simplex->A.Dilation;
    v3 AC = Simplex->C.Dilation - Simplex->A.Dilation;
    v3 ABC = Cross(AB, AC);

    v3 ABC_AC = Cross(ABC, AC);
    v3 AB_ABC = Cross(AB, ABC);
    if (Inner(ABC_AC, A0) >= 0)
    {
        if (Inner(AC, A0) >= 0)
        {
            Simplex->B = Simplex->A;
            Simplex->Count = 2;

            *Direction = TripleCrossProduct(AC, A0, AC);
        }
        else
        {
            // star case
            if (Inner(AB, A0) >= 0)
            {
                Simplex->C = Simplex->B;
                Simplex->B = Simplex->A;
                Simplex->Count = 2;

                *Direction = TripleCrossProduct(AB, A0, AB);
            }
            else
            {
                Simplex->B = Simplex->A;
                Simplex->Count = 1;

                *Direction = A0;
            }
            // end star case
        }
    }
    else
    {

        if (Inner(AB_ABC, A0) >= 0)
        {
            // star case
            if (Inner(AB, A0) >= 0)
            {
                Simplex->C = Simplex->B;
                Simplex->B = Simplex->A;
                Simplex->Count = 2;

                *Direction = TripleCrossProduct(AB, A0, AB);
            }
            else
            {
                Simplex->B = Simplex->A;
                Simplex->Count = 1;

                *Direction = A0;
            }
            // end star case
        }
        else
        {
            if (Inner(ABC, A0) >= 0)
            {
                Simplex->D = Simplex->C;
                Simplex->C = Simplex->B;
                Simplex->B = Simplex->A;

                *Direction = ABC;
            }
            else
            {
                Simplex->D = Simplex->C;
                Simplex->C = Simplex->B;
                Simplex->B = Simplex->A;

                // swap winding
                simplex_point Storage = Simplex->D;
                Simplex->D = Simplex->C;
                Simplex->C = Storage;

                *Direction = -ABC;
            }
        }
    }
}

void CheckTetrahedron(simplex* Simplex, v3* Direction)
{
    v3 A = Simplex->A.Dilation;
    v3 B = Simplex->B.Dilation;
    v3 C = Simplex->C.Dilation;

    v3 AB = B - A;
    v3 AC = C - A;

    v3 ABC = Cross(AB, AC); //n123

    v3 A0 = -Simplex->A.Dilation;
    v3 AB_ABC = Cross(AB, ABC);
    v3 ABC_AC = Cross(ABC, AC);

    if (Inner(AB_ABC, A0) >= 0)
    {
        Simplex->C = Simplex->B;
        Simplex->B = Simplex->A;
        Simplex->Count = 2;

        *Direction = TripleCrossProduct(AB, A0, AB);
    }
    else if (Inner(ABC_AC, A0) >= 0)
    {
        Simplex->B = Simplex->A;
        Simplex->Count = 2;

        *Direction = TripleCrossProduct(AC, A0, AC);
    }
    else
    {
        Simplex->D = Simplex->C;
        Simplex->C = Simplex->B;
        Simplex->B = Simplex->A;
        Simplex->Count = 3;

        *Direction = ABC;
    }
}

bool32 GJK_EvolveSimplexTetrahedron(simplex* Simplex, v3* Direction)
{
    bool32 Result = false;

    Assert(*Direction != v3_ZERO);
    Assert(Simplex->Count == 4);

    v3 A = Simplex->A.Dilation;
    v3 B = Simplex->B.Dilation;
    v3 C = Simplex->C.Dilation;
    v3 D = Simplex->D.Dilation;

    v3 AB = B - A;
    v3 AC = C - A;
    v3 AD = D - A;

    v3 ABC = Cross(AB, AC); //n123
    v3 ADB = Cross(AD, AB); //n142
    v3 ACD = Cross(AC, AD);

    // CASE 1
    if (Inner(ABC, -A) >= 0)
    {
        // In front of triangle ABC
        CheckTetrahedron(Simplex, Direction);
        Result = false;
    }
    // CASE 2
    else if (Inner(ACD, -A) >= 0)
    {
        // In front of triangle ACD
        Simplex->B = Simplex->C;
        Simplex->C = Simplex->D;
        CheckTetrahedron(Simplex, Direction);
        Result = false;
    }
    // CASE 3
    else if (Inner(ADB, -A) >= 0)
    {
        Simplex->C = Simplex->B;
        Simplex->B = Simplex->D;
        CheckTetrahedron(Simplex, Direction);
        Result = false;
    }
    else
    {
        Result = true;
    }

    return Result;
}

//TODO(james): to do EPA, we need a full tetrahedron for 3D, or full triangle for 2D,
//so if we are only checking overlap (triggers) then we can return early
//if possible, otherwise we must continue until a full shape is made
inline bool32
GJK_Step(simplex* Simplex, v3* Direction)
{
    //YamProf(GJK_Step);
    bool32 Result = false;
    //Assert(SimplexSize > 1);

    *Direction = Normalize(*Direction);

    if (Simplex->Count == 1)
    {
        //*Direction = V3(-AB.y, AB.x, 0); //just give it some other direction
        //Normalize(*Direction);
    }
    else if (Simplex->Count == 2)
    {
        GJK_EvolveSimplexLine(Simplex, Direction);
    }
    else if (Simplex->Count == 3)
    {
        GJK_EvolveSimplexTriangle(Simplex, Direction);
#if 0
        if (stb_arr_len(Simplex) == 3)
        {
            CheckTriangleIntegrity(SimplexTriangle(Simplex[0],Simplex[1],Simplex[2]));
        }
#endif
    }
    else if (Simplex->Count == 4)
    {

        Result = GJK_EvolveSimplexTetrahedron(Simplex, Direction);
#if 0        
        if (stb_arr_len(Simplex) == 4)
        {
            CheckTriangleIntegrity(SimplexTriangle(Simplex[0],Simplex[1],Simplex[2]));
            CheckTriangleIntegrity(SimplexTriangle(Simplex[1],Simplex[2],Simplex[3]));
            CheckTriangleIntegrity(SimplexTriangle(Simplex[0],Simplex[2],Simplex[3]));
            CheckTriangleIntegrity(SimplexTriangle(Simplex[0],Simplex[1],Simplex[3]));
        }
        else if (stb_arr_len(Simplex) == 3)
        {
            CheckTriangleIntegrity(SimplexTriangle(Simplex[0],Simplex[1],Simplex[2]));
        }
#endif
    }
    else if (Simplex->Count > 4)
    {
        InvalidCodePath;
    }
    //let simplex size 1 fall through

    return Result;
}

//
// EPA
//

triangle Triangle(v3 A, v3 B, v3 C)
{
    triangle Result;
    Result.A = A;
    Result.B = B;
    Result.C = C;

    return Result;
}

v3 TriangleNormal(v3 A, v3 B, v3 C)
{
    v3 Result;
    //CheckTriangleIntegrity(Tri);
    Result = Normalize(Cross(
        C - A,
        B - A));
    return Result;
}

v3 TriangleNormal(triangle Tri)
{
    v3 Result;
    //CheckTriangleIntegrity(Tri);
    Result = TriangleNormal(Tri.A, Tri.B, Tri.C);
    return Result;
}

v3 TriangleNormal(simplex_triangle Tri)
{
    v3 Result;

    Result = TriangleNormal(SimplexTriangleToTriangle(Tri));

    return Result;
}

//http://mathinsight.org/distance_point_plane
inline real32
PointDistanceFromFace(v3 Point, triangle Tri)
{
    v3 Normal = TriangleNormal(Tri);
    Assert(Normal != v3_ZERO);
    // plane equation = Ax + By + Cz + d = 0, ABC is the vector normal
    // xyz is a point on the plane
    real32 D = AbsoluteValue(Inner(Normal, Point - Tri.A));

    //NOTE(james): if we wanted to project a point to a plane, it'd  be P + (D * Normal)
    return D;
}

inline real32
PointDistanceFromFace(v3 Point, simplex_triangle Tri)
{
    real32 Result;
    Result = PointDistanceFromFace(Point, SimplexTriangleToTriangle(Tri));
    return Result;
}

inline void
PointDistanceFromFaceTest()
{
    triangle Triangle = {};
    Triangle.A = V3(1, 0, 0);
    Triangle.B = V3(1, 1, 0);
    Triangle.C = V3(1, 0, 1);

    real32 Dist = PointDistanceFromFace(v3_ZERO, Triangle);
    Assert(Dist == 1);
}

//returns index of closest face
internal int
GetNearestFace(polytope* Polytope, v3 Origin,
               real32* DistanceResult, v3* NormalResult)
{
    uint32 ClosestFaceIndex = MAX_UINT32;
    real32 ClosestFaceDistance = 10000;
    v3 ClosestNormal = v3_ZERO;
    for (int32 FaceIndex = 0;
         FaceIndex < stb_arr_len(Polytope->Triangles);
         FaceIndex++)
    {
        Assert(FaceIndex < 1000);

        real32 FaceDistance = PointDistanceFromFace(Origin, Polytope->Triangles[FaceIndex]);
        if (FaceDistance < ClosestFaceDistance)
        {
            v3 NewNormal = TriangleNormal(Polytope->Triangles[FaceIndex]);
            ClosestFaceDistance = FaceDistance;
            ClosestFaceIndex = FaceIndex;
            ClosestNormal = NewNormal;
            //Assert(ClosestFaceDistance > 0);
        }
    }

    *DistanceResult = ClosestFaceDistance;
    *NormalResult = ClosestNormal;

    Assert(ClosestFaceIndex >= 0);
    Assert(*NormalResult != v3_ZERO);

    return ClosestFaceIndex;
}

inline void
AddPolytopeTriangle(polytope* Polytope, simplex_point A, simplex_point B, simplex_point C)
{

    simplex_triangle Tri;

    Tri.A = A;
    Tri.B = B;
    Tri.C = C;

    CheckTriangleIntegrity(Tri);

    stb_arr_push(Polytope->Triangles, Tri);
}

// NOTE(james): If we find an edge in the opposite direction, then they cancel each other out
inline void
AddPolytopeEdge(polytope* Polytope, edge Edge)
{
    bool FoundOppositeEdge = false;
    int OppositeEdgeIndex = -1;
    for (int EdgeIndex = 0;
         EdgeIndex < stb_arr_len(Polytope->Edges);
         EdgeIndex++)
    {
        if (Polytope->Edges[EdgeIndex].A.Dilation == Edge.B.Dilation && Polytope->Edges[EdgeIndex].B.Dilation == Edge.A.Dilation)
        {
            FoundOppositeEdge = true;
            OppositeEdgeIndex = EdgeIndex;
            break;
        }
    }

    if (!FoundOppositeEdge)
    {
        *stb_arr_add(Polytope->Edges) = Edge;
    }
    else
    {
        stb_arr_delete(Polytope->Edges, OppositeEdgeIndex);
    }
}

inline void
RemovePolytopeTriangle(polytope* Polytope, int TriangleIndex)
{
    edge Edge1 = { Polytope->Triangles[TriangleIndex].A, Polytope->Triangles[TriangleIndex].B };
    edge Edge2 = { Polytope->Triangles[TriangleIndex].B, Polytope->Triangles[TriangleIndex].C };
    edge Edge3 = { Polytope->Triangles[TriangleIndex].C, Polytope->Triangles[TriangleIndex].A };

    AddPolytopeEdge(Polytope, Edge1);
    AddPolytopeEdge(Polytope, Edge2);
    AddPolytopeEdge(Polytope, Edge3);

    stb_arr_delete(Polytope->Triangles, TriangleIndex);
}

v3 ToBarycentric(v3 P, v3 A, v3 B, v3 C)
{
    v3 vertex0 = B - A, vertex1 = C - A, vertex2 = P - A;
    float d00 = Inner(vertex0, vertex0);
    float d01 = Inner(vertex0, vertex1);
    float d11 = Inner(vertex1, vertex1);
    float d20 = Inner(vertex2, vertex0);
    float d21 = Inner(vertex2, vertex1);
    float denom = d00 * d11 - d01 * d01;
    float v = (d11 * d20 - d01 * d21) / denom;
    float w = (d00 * d21 - d01 * d20) / denom;
    float u = 1.0f - v - w;
    v3 Result = V3(u, v, w);

    return Result;
}

v3 ToBarycentric(v3 P, triangle Tri)
{
    v3 Result;
    Result = ToBarycentric(P, Tri.A, Tri.B, Tri.C);
    return Result;
}

v3 ToCartesian(v3 BarycentricCoord, v3 A, v3 B, v3 C)
{
    v3 Result;

    Result = A * BarycentricCoord.x + B * BarycentricCoord.y + C * BarycentricCoord.z;

    return Result;
}

v3 ToCartesian(v3 BarycentricCoord, triangle Tri)
{
    v3 Result;
    Result = ToCartesian(BarycentricCoord, Tri.A, Tri.B, Tri.C);
    return Result;
}

void BarycentricCartesianConversionTest()
{
    v3 A = V3(0, 0, 0);
    v3 B = V3(1, 0, 0);
    v3 C = V3(0, 1, 0);

    v3 Point = V3(0.5f, 0, 0);

    v3 Barycentric = ToBarycentric(Point, A, B, C);
    Assert(Barycentric == V3(0.5f, 0.5f, 0));
}

void ProjectPointToCurvedSurface(entity* Entity, v3* ContactPoint, v3 Normal, float* PenetrationDepth)
{
    if (Entity->ColliderType == ColliderType_Capsule)
    {
        v3 OldContactPoint = *ContactPoint;
        // TODO(james): should be Normal or (ContactPoint - Entity->Position)?
        v3 LookDirection = Normal; //Normalize(*ContactPoint - Entity->Position);
        *ContactPoint = ClosestPointToDirectionCapsule(Entity, LookDirection);
        real32 PenetrationDepthDelta = Length(OldContactPoint - *ContactPoint);
        *PenetrationDepth += PenetrationDepthDelta;
    }
    else if (Entity->ColliderType == ColliderType_Sphere)
    {
        Assert(0);
    }
    else if (Entity->ColliderType == ColliderType_Cylinder)
    {
        Assert(0);
    }
}

//http://allenchou.net/2013/12/game-physics-contact-generation-epa/
internal void
EPA(collision_group* CollisionGroup, collision_pair* Pair, all_contacts* AllContacts)
{
    YamProf(EPA);
    Assert(Pair->Simplex.Count == 4);
    Assert(Pair->Simplex.A.Dilation != Pair->Simplex.B.Dilation);
    Assert(Pair->Simplex.A.Dilation != Pair->Simplex.C.Dilation);
    Assert(Pair->Simplex.A.Dilation != Pair->Simplex.D.Dilation);
    Assert(Pair->Simplex.B.Dilation != Pair->Simplex.C.Dilation);
    Assert(Pair->Simplex.B.Dilation != Pair->Simplex.D.Dilation);
    Assert(Pair->Simplex.C.Dilation != Pair->Simplex.D.Dilation);

    //fix tetrahedron winding if necessary, so 0 1 2 is CCW
    v3 x30 = Pair->Simplex.A.Dilation - Pair->Simplex.D.Dilation;
    v3 x31 = Pair->Simplex.B.Dilation - Pair->Simplex.D.Dilation;
    v3 x32 = Pair->Simplex.C.Dilation - Pair->Simplex.D.Dilation;
    real32 det = Inner(x30, Cross(x31, x32));
    if (det > 0) // TODO(james): we probably shouldn't be hitting this if GJK is working correctly
    {
        //Assert(0);
        simplex_point Storage = Pair->Simplex.A;
        Pair->Simplex.A = Pair->Simplex.B;
        Pair->Simplex.B = Storage;
    }

    polytope Polytope = {};
    Polytope.Triangles = NULL;
    Polytope.Edges = NULL;
    AddPolytopeTriangle(&Polytope,
                        Pair->Simplex.A,
                        Pair->Simplex.B,
                        Pair->Simplex.C);
    AddPolytopeTriangle(&Polytope,
                        Pair->Simplex.B,
                        Pair->Simplex.A,
                        Pair->Simplex.D);
    AddPolytopeTriangle(&Polytope,
                        Pair->Simplex.A,
                        Pair->Simplex.C,
                        Pair->Simplex.D);
    AddPolytopeTriangle(&Polytope,
                        Pair->Simplex.C,
                        Pair->Simplex.B,
                        Pair->Simplex.D);

    real32 NearestFaceDistance = 10000;
    simplex_triangle NearestFace = {};
    v3 NearestFaceNormal = {};
    v3 Origin = {};

    int32 IterationCount = 0;
    bool32 Iterating = true;
    while (Iterating && IterationCount < 10)
    {
        real32 TestFaceDistance;
        v3 TestFaceNormal;
        // step 3: find closest face to the origin
        int TestFaceIndex = GetNearestFace(&Polytope, Origin,
                                           /*out*/ &TestFaceDistance, /*out*/ &TestFaceNormal);

        // step 4: if closer than last, otherwise break loop
        if (AbsoluteValue(TestFaceDistance - NearestFaceDistance) > FLT_EPSILON * 2) //epsilon?
        {
            NearestFaceDistance = TestFaceDistance;
            NearestFace = Polytope.Triangles[TestFaceIndex];
            NearestFaceNormal = TestFaceNormal;
            // Step 5: remove nearest face, find support in its normal direction
            RemovePolytopeTriangle(&Polytope, TestFaceIndex);

            simplex_point SupportPoint = FindSupport(Pair, NearestFaceNormal);

            // if we already have this point in the simplex, then we can't go any farther this way
            // NOTE(james): this was not in the Allen Chou algorithm, my addition
            bool32 AlreadyHavePoint = false;
            for (int TriIndex = 0;
                 TriIndex < stb_arr_len(Polytope.Triangles);
                 ++TriIndex)
            {
                if (SupportPoint.Dilation == Polytope.Triangles[TriIndex].A.Dilation || SupportPoint.Dilation == Polytope.Triangles[TriIndex].B.Dilation || SupportPoint.Dilation == Polytope.Triangles[TriIndex].C.Dilation)
                {
                    AlreadyHavePoint = true;
                    Iterating = false;
                    break;
                }
            }

            // NOTE(james): another James addition on top of Allen Chou's.
            bool32 IsCoplanar = false;
            if (PointDistanceFromFace(SupportPoint.Dilation, NearestFace) < FLT_EPSILON * 2)
            {
                IsCoplanar = true;
                Iterating = false;
            }

            if (!AlreadyHavePoint && !IsCoplanar)
            {

                // Step 6: remove faces thatt can be seen by new polytope,
                // repair polytope, using new support vertice
                // by seen, I mean that the new face introduces a concavity,
                // so we will those by "tenting out" at the new vertex

                // remove faces
                int TriangleIndex = 0;

                while (TriangleIndex < stb_arr_len(Polytope.Triangles))
                {
                    v3 Normal = TriangleNormal(Polytope.Triangles[TriangleIndex]);
                    v3 FaceToNewPoint = SupportPoint.Dilation - Polytope.Triangles[TriangleIndex].A.Dilation;
                    if (Inner(Normal, FaceToNewPoint) > 0.0f)
                    {
                        RemovePolytopeTriangle(&Polytope, TriangleIndex);
                    }
                    else
                    {
                        TriangleIndex++;
                    }
                }

                //repair holes
                int x = 0;
                for (int EdgeIndex = 0;
                     EdgeIndex < stb_arr_len(Polytope.Edges);
                     EdgeIndex++)
                {
                    //TODO(james): winding??
                    AddPolytopeTriangle(&Polytope,
                                        SupportPoint,
                                        Polytope.Edges[EdgeIndex].A, Polytope.Edges[EdgeIndex].B);
                }
                stb_arr_deleten(Polytope.Edges, 0, stb_arr_len(Polytope.Edges)); //clear list
            }

            // Back to 3
            ++IterationCount;
        }
        else
        {
            Iterating = false;
        }
    }
    // step 8: return penetration depth and normal

    //contact_info ContactInfo = {};
    v3 MinkowskiNormal = Normalize(NearestFaceNormal);

    real32 PenetrationDepth = NearestFaceDistance;

    //NOTE(james): for contact points, project origin to nearest face, convert to barycentric coordinates, use barycentric coordinates on each meshes supports
    //barycentric coordinates are like a normalized point in a geometric space, so for a triangle, point A is (1,0,0), B is (0,1,0), C is (0,0,1), center of triangle is (0.33, 0.33, 0.33)
    //penetration depth may be zero if the pieces are just abutting
    v3 MinkowksiContactPoint = Normalize(NearestFaceNormal);
    if (PenetrationDepth > 0)
    {
        MinkowksiContactPoint = MinkowksiContactPoint * PenetrationDepth;
    }

    v3 BarycentricCoordinates = ToBarycentric(MinkowksiContactPoint,
                                              NearestFace.A.Dilation,
                                              NearestFace.B.Dilation,
                                              NearestFace.C.Dilation);
    v3 CartesianContactPointA = ToCartesian(BarycentricCoordinates,
                                            NearestFace.A.PointA,
                                            NearestFace.B.PointA,
                                            NearestFace.C.PointA);

    v3 CartesianContactPointB = ToCartesian(BarycentricCoordinates,
                                            NearestFace.A.PointB,
                                            NearestFace.B.PointB,
                                            NearestFace.C.PointB);

    //NOTE(james): I think that if we are colliding with a sphere, capsule, or cylinder, then we are not
    //fully considering the spherical volume of the object. EPA reduces the collision to a plane.

    v3 PointANormal = TriangleNormal(Triangle(NearestFace.A.PointA,
                                              NearestFace.B.PointA,
                                              NearestFace.C.PointA));
    /*
    ProjectPointToCurvedSurface(Pair->A, &CartesianContactPointA, 
                                MinkowskiNormal, &PenetrationDepth);
    ProjectPointToCurvedSurface(Pair->B, &CartesianContactPointB, 
                                -MinkowskiNormal, &PenetrationDepth);
    */

    /*
    if (Length(CartesianContactPointA - CartesianContactPointB) > PenetrationDepth + 0.001f)
    {
        Assert(0);//large difference in contact points
    }
    */

    contact_info* Contact = &AllContacts->Contacts[AllContacts->Count++];
    Assert(AllContacts->Count < MAX_COLLISION_PAIRS);
    if (Pair->A->IsTrigger || Pair->B->IsTrigger)
    {
        Contact->IsTriggerContact = true;
    }

    Contact->PenetrationDepth = PenetrationDepth;

    Contact->PointWorldA = CartesianContactPointA;
    Contact->PointWorldB = CartesianContactPointB;

    Contact->NormalA = MinkowskiNormal;
    Contact->NormalB = -MinkowskiNormal;

    Contact->A = Pair->A;
    Contact->B = Pair->B;

    // NOTE(james): not really sure why this happens sometimes, only happens when they
    // are basically prefectly aligned
    if (Contact->PenetrationDepth == 0)
    {
        Contact->PenetrationDepth = FLT_EPSILON;
        MyConsole.AddLog("Faked Depth");
    }

#if 0
    if (CollisionGroup->NarrowphaseViz)
    {

        // draw supports
        DrawCube(CollisionGroup->RenderList, NearestFace.A.PointA, v3_ONE * 0.1f, v4_GREEN);
        DrawCube(CollisionGroup->RenderList, NearestFace.B.PointA, v3_ONE * 0.1f, v4_GREEN);
        DrawCube(CollisionGroup->RenderList, NearestFace.C.PointA, v3_ONE * 0.1f, v4_GREEN);

        DrawCube(CollisionGroup->RenderList, NearestFace.A.PointB, v3_ONE * 0.1f, v4_MAGENTA);
        DrawCube(CollisionGroup->RenderList, NearestFace.B.PointB, v3_ONE * 0.1f, v4_MAGENTA);
        DrawCube(CollisionGroup->RenderList, NearestFace.C.PointB, v3_ONE * 0.1f, v4_MAGENTA);

        DrawCube(CollisionGroup->RenderList, ContactA->PointWorldMe,
                 v3_ONE * 0.1f, v4_BLUE);
        DrawLine(CollisionGroup->RenderList, ContactA->PointWorldMe,
                 ContactA->PointWorldMe + ContactA->Normal, v4_BLUE);

        DrawCube(CollisionGroup->RenderList, ContactA->PointWorldOther,
                 v3_ONE * 0.1f, v4_RED);
        DrawLine(CollisionGroup->RenderList, ContactA->PointWorldOther,
                 ContactA->PointWorldOther + ContactB->Normal, v4_RED);

        MyConsole.AddLog("%f", ContactA->PenetrationDepth);
    }
#endif
}

//TODO(james): capsule isn't colliding correctly on the edges of the sphere against cubes.
//apparently this is degenerate? https://www.gamedev.net/topic/605999-gjk-accuracy-at-near-contact/
internal bool32
GJK(collision_group* CollisionGroup, collision_pair* Pair, v3 StartingDirection, all_contacts* AllContacts)
{
    YamProf(GJK);
    bool32 Result = false;

    StartingDirection = V3(1, 1, 1);
    StartingDirection = Normalize(StartingDirection);
    v3 Direction = StartingDirection; //MeshInstanceB->LocalPosition - MeshInstanceA->LocalPosition;

    Pair->Simplex = {};
    simplex_point Support = FindSupport(Pair, Direction);

    Pair->Simplex.B = Support;

    Pair->Simplex.Count = 1;

    Direction = -Support.Dilation;

    uint32 IterationCount = 0;
    while (IterationCount++ < 20)
    {
        simplex_point NextPoint = FindSupport(Pair, Direction);

        if (Inner(NextPoint.Dilation, Direction) < 0)
        {
            //no collision
            break;
        }
        else
        {
            if (Length(NextPoint.Dilation) < Length(Direction))
            {
                //Assert(0);
                //no collision, length is
            }

            Pair->Simplex.A = NextPoint;
            Pair->Simplex.Count++;

#if 1
            bool32 MatchingPoints = false;

            if (Pair->Simplex.Count == 2)
            {
                MatchingPoints = (Pair->Simplex.A.Dilation == Pair->Simplex.B.Dilation);
            }
            if (Pair->Simplex.Count == 3)
            {
                MatchingPoints = NoMatchingPoints(Pair->Simplex.A.Dilation, Pair->Simplex.B.Dilation, Pair->Simplex.C.Dilation);
            }
            if (Pair->Simplex.Count == 4)
            {
                MatchingPoints = NoMatchingPoints(Pair->Simplex.A.Dilation, Pair->Simplex.B.Dilation, Pair->Simplex.C.Dilation) || NoMatchingPoints(Pair->Simplex.A.Dilation, Pair->Simplex.B.Dilation, Pair->Simplex.D.Dilation) || NoMatchingPoints(Pair->Simplex.A.Dilation, Pair->Simplex.C.Dilation, Pair->Simplex.D.Dilation) || NoMatchingPoints(Pair->Simplex.B.Dilation, Pair->Simplex.C.Dilation, Pair->Simplex.D.Dilation);
            }

            //Assert(!MatchingPoints);
            if (MatchingPoints)
            {
                break;
            }
#endif

            if (GJK_Step(&Pair->Simplex, &Direction))
            {
#if 0
                    Assert(Pair->Simplex.A.Dilation.x > 0 || 
                           Pair->Simplex.B.Dilation.x > 0 || 
                           Pair->Simplex.C.Dilation.x > 0 || 
                           Pair->Simplex.D.Dilation.x > 0);
                    Assert(Pair->Simplex.A.Dilation.x < 0 || 
                           Pair->Simplex.B.Dilation.x < 0 || 
                           Pair->Simplex.C.Dilation.x < 0 || 
                           Pair->Simplex.D.Dilation.x < 0);
                    Assert(Pair->Simplex.A.Dilation.y > 0 || 
                           Pair->Simplex.B.Dilation.y > 0 || 
                           Pair->Simplex.C.Dilation.y > 0 || 
                           Pair->Simplex.D.Dilation.y > 0);
                    Assert(Pair->Simplex.A.Dilation.y < 0 || 
                           Pair->Simplex.B.Dilation.y < 0 || 
                           Pair->Simplex.C.Dilation.y < 0 || 
                           Pair->Simplex.D.Dilation.y < 0);
                    Assert(Pair->Simplex.A.Dilation.z > 0 || 
                           Pair->Simplex.B.Dilation.z > 0 || 
                           Pair->Simplex.C.Dilation.z > 0 || 
                           Pair->Simplex.D.Dilation.z > 0);
                    Assert(Pair->Simplex.A.Dilation.z < 0 || 
                           Pair->Simplex.B.Dilation.z < 0 || 
                           Pair->Simplex.C.Dilation.z < 0 || 
                           Pair->Simplex.D.Dilation.z < 0);

#endif
                v3 TriangleNormal0 = TriangleNormal(Pair->Simplex.A.Dilation,
                                                    Pair->Simplex.B.Dilation,
                                                    Pair->Simplex.C.Dilation);
                v3 TriangleNormal1 = TriangleNormal(Pair->Simplex.B.Dilation,
                                                    Pair->Simplex.A.Dilation,
                                                    Pair->Simplex.D.Dilation);
                v3 TriangleNormal2 = TriangleNormal(Pair->Simplex.A.Dilation,
                                                    Pair->Simplex.C.Dilation,
                                                    Pair->Simplex.D.Dilation);
                v3 TriangleNormal3 = TriangleNormal(Pair->Simplex.C.Dilation,
                                                    Pair->Simplex.B.Dilation,
                                                    Pair->Simplex.D.Dilation);
                Assert(TriangleNormal0 != TriangleNormal1);
                Assert(TriangleNormal0 != TriangleNormal2);
                Assert(TriangleNormal0 != TriangleNormal3);
                Assert(TriangleNormal1 != TriangleNormal2);
                Assert(TriangleNormal1 != TriangleNormal3);
                Assert(TriangleNormal2 != TriangleNormal3);
#if 0 // draw minkowski simplex
                    DrawCube(CollisionGroup->RenderList, Pair->Simplex.A.Dilation, v3_ONE, v4_WHITE * 0);
                    DrawCube(CollisionGroup->RenderList, Pair->Simplex.B.Dilation, v3_ONE, v4_WHITE * 0.4f);
                    DrawCube(CollisionGroup->RenderList, Pair->Simplex.C.Dilation, v3_ONE, v4_WHITE * 0.8f);
                    DrawCube(CollisionGroup->RenderList, Pair->Simplex.D.Dilation, v3_ONE, v4_WHITE);
                    DrawCube(CollisionGroup->RenderList, v3_ZERO, v3_ONE, v4_BLACK);
                    
                    DrawLine(CollisionGroup->RenderList, Pair->Simplex.A.Dilation, Pair->Simplex.B.Dilation, v4_WHITE);
                    DrawLine(CollisionGroup->RenderList, Pair->Simplex.A.Dilation, Pair->Simplex.C.Dilation, v4_WHITE);
                    DrawLine(CollisionGroup->RenderList, Pair->Simplex.A.Dilation, Pair->Simplex.D.Dilation, v4_WHITE);
                    DrawLine(CollisionGroup->RenderList, Pair->Simplex.B.Dilation, Pair->Simplex.C.Dilation, v4_WHITE);
                    DrawLine(CollisionGroup->RenderList, Pair->Simplex.B.Dilation, Pair->Simplex.D.Dilation, v4_WHITE);
                    DrawLine(CollisionGroup->RenderList, Pair->Simplex.C.Dilation, Pair->Simplex.D.Dilation, v4_WHITE);
#endif

                //collision!
                Result = true;

                //Assert(Pair->A->Configurations[Pair->ConfigurationIndex].Transform.LocalPosition.y < 0.5f);

                /*
                    // draw GJK initial supports
                    if (CollisionGroup->NarrowphaseViz)
                    {
                        for (int i = 0; i < stb_arr_len(Pair->Simplex); i++)
                        {
                            DrawCube(CollisionGroup->RenderList, Pair->Simplex[i].PointA, v3_ONE * 0.1f, v4_GREEN);
                            if (i > 0)
                            {
                                DrawLine(CollisionGroup->RenderList, Pair->Simplex[i].PointA, Pair->Simplex[i-1].PointA, v4_GREEN);
                            }
                            
                            //DrawCube(RenderList, Pair.Simplex[i].PointB, v3_ONE * 0.1f, v4_PINK);
                            if (i > 0)
                            {
                                DrawLine(CollisionGroup->RenderList, Pair->Simplex[i].PointB, Pair->Simplex[i-1].PointB, v4_RED);
                            }
                            
                        }
                    }
                    */

                EPA(CollisionGroup, Pair, AllContacts);
                break;
            }
        }
    }

    return Result;
}

bool32 CollisionDetection(collision_group* Group, render_list* RenderList,
                          entity* Entity, uint32 ConfigurationIndex, all_contacts* AllContacts)
{
    //YamProf(CollisionDetection);

    collision_pair Pairs[MAX_COLLISION_PAIRS];
    uint32 CollisionPairsCount = 0;

    YamProfBegin(BroadphaseCollision);
    linked_list_member* InnerLoopMember = Group->Entries.FirstMember;
    bool BroadphaseCollided = false;
    while (InnerLoopMember != NULL)
    {
        //YamProf(BroadphaseCollision);
        entity* InnerLoopEntity = (entity*)InnerLoopMember->Payload;

        if (Entity != InnerLoopEntity)
        {
            if (BroadphaseCollision(Group, Entity, InnerLoopEntity))
            {
                collision_pair Pair = {};
                Pair.A = Entity;
                Pair.B = InnerLoopEntity;
                BroadphaseCollided = true;
                Pairs[CollisionPairsCount++] = Pair;
                Assert(CollisionPairsCount < MAX_COLLISION_PAIRS);
            }
        }
        InnerLoopMember = InnerLoopMember->NextNode;
    }

    YamProfEnd();
    if (!BroadphaseCollided)
    {
        return false;
    }

    //
    // Narrow
    //
    YamProfBegin(NarrowphaseCollision);
    bool32 Collided = false;
    for (uint32 PairIndex = 0;
         PairIndex < CollisionPairsCount;
         ++PairIndex)
    {

        collision_pair* Pair = &Pairs[PairIndex];

        contact_info* Contact = {};

        //if (Pair.A->CollidedThisFrame && Pair.B->CollidedThisFrame)
        {

        }
        //else
        {
            if (Pair->A->ColliderType == ColliderType_Mesh && Pair->B->ColliderType == ColliderType_Mesh)
            {
                v3 StartingDirection = Pair->A->Configs[ConfigurationIndex].Position - Pair->B->Configs[ConfigurationIndex].Position;

                bool32 SamePosition = false;
                if (Pair->A->Position == Pair->B->Position)
                {
                    Assert(0);
                    SamePosition = true;
                    /*
                    Pair->A->Contact.Other = Pair->B;
                    Pair->B->Contact.Other = Pair->A;
                    
                    Pair->A->Contact.Normal = v3_UP;
                    Pair->B->Contact.Normal = v3_UP;
                    */
                }

                if (SamePosition || GJK(Group, Pair, StartingDirection, AllContacts))
                {
                    Pair->A->CollidedThisFrame = true;
                    Pair->B->CollidedThisFrame = true;

                    Collided = true;

                    //contact_info* NewContact = PushStruct(&Group->ContactsThisFrame, contact_info);
                    //*NewContact = *Contact;
                    //collision happened, add to resolution page, do any trigger stuff, whatever
                    if (Group->NarrowphaseViz)
                    {
                        DrawCube(RenderList, Pair->A, v4_RED, RenderMode_Wireframe);
                        DrawCube(RenderList, Pair->B, v4_RED, RenderMode_Wireframe);
                    }
                }
                else if (Group->NarrowphaseViz)
                {
                    DrawCube(RenderList, Pair->A, v4_BLACK, RenderMode_Wireframe);
                    DrawCube(RenderList, Pair->B, v4_BLACK, RenderMode_Wireframe);
                }
            }
            else if (Pair->A->ColliderType == ColliderType_Sphere && Pair->B->ColliderType == ColliderType_Sphere)
            {
                if (SphereSphereIntersects(Pair->A, Pair->B, Contact))
                {
                    contact_info* PushedContact = PushStruct(&Group->ContactsThisFrame, contact_info);
                    *PushedContact = *Contact;
                    Pair->A->CollidedThisFrame = true;
                    Pair->B->CollidedThisFrame = true;
                    DrawSphere(RenderList, Pair->A->LocalPosition, Pair->A->SphereColliderRadius, v4_RED);
                    DrawSphere(RenderList, Pair->B->LocalPosition, Pair->A->SphereColliderRadius, v4_RED);
                }
                else
                {
                    DrawSphere(RenderList, Pair->A->LocalPosition, Pair->A->SphereColliderRadius, v4_BLACK);
                    DrawSphere(RenderList, Pair->B->LocalPosition, Pair->A->SphereColliderRadius, v4_BLACK);
                }
            }
            else if (Pair->A->ColliderType == ColliderType_Mesh && Pair->B->ColliderType == ColliderType_Sphere)
            {
                v3 StartingDirection = Pair->A->LocalPosition - Pair->B->LocalPosition;
                if (GJK(Group, Pair, StartingDirection, AllContacts))
                //if (SphereMeshIntersects(Pair->B, Pair->A))
                {
                    contact_info* PushedContact = PushStruct(&Group->ContactsThisFrame, contact_info);
                    *PushedContact = *Contact;
                    Pair->A->CollidedThisFrame = true;
                    Pair->B->CollidedThisFrame = true;
                    DrawCube(RenderList, Pair->A, v4_RED, RenderMode_Wireframe);
                    DrawSphere(RenderList, Pair->B->LocalPosition, Pair->B->SphereColliderRadius, v4_RED, RenderMode_Wireframe);
                }
                else
                {
                    DrawCube(RenderList, Pair->A, v4_BLACK, RenderMode_Wireframe);
                    DrawSphere(RenderList, Pair->B->LocalPosition, Pair->B->SphereColliderRadius, v4_BLACK, RenderMode_Wireframe);
                }
            }
            else if (Pair->A->ColliderType == ColliderType_Sphere && Pair->B->ColliderType == ColliderType_Mesh)
            {
                v3 StartingDirection = Pair->A->LocalPosition - Pair->B->LocalPosition;
                if (GJK(Group, Pair, StartingDirection, AllContacts))
                //if (SphereMeshIntersects(Pair->A, Pair->B))
                {
                    contact_info* PushedContact = PushStruct(&Group->ContactsThisFrame, contact_info);
                    *PushedContact = *Contact;
                    Pair->A->CollidedThisFrame = true;
                    Pair->B->CollidedThisFrame = true;
                    DrawSphere(RenderList, Pair->A->LocalPosition, Pair->A->SphereColliderRadius, v4_RED, RenderMode_Wireframe);
                    DrawCube(RenderList, Pair->B, v4_RED, RenderMode_Wireframe);
                }
                else
                {
                    DrawSphere(RenderList, Pair->A->LocalPosition, Pair->A->SphereColliderRadius, v4_BLACK, RenderMode_Wireframe);
                    DrawCube(RenderList, Pair->B, v4_BLACK, RenderMode_Wireframe);
                }
            }
            else
            {
                v3 StartingDirection = Pair->A->Configs[Pair->A->TargetConfig].Position - Pair->B->Position;
                if (GJK(Group, Pair, StartingDirection, AllContacts))
                //if (SphereMeshIntersects(Pair->A, Pair->B))
                {
                    Collided = true;
                    //contact_info* PushedContact = PushStruct(&Group->ContactsThisFrame, contact_info);
                    //*PushedContact = *Contact;
                    Pair->A->CollidedThisFrame = true;
                    Pair->B->CollidedThisFrame = true;
                }
            }
        }
    }

    YamProfEnd();
    return Collided;
}
