#if !defined(YAMGINE_TRANSFORM_H)

#include "yamgine_math.h"
#include "yamgine_string.h"
#include "yamgine_profiling.h"

//NOTE(james): these are totally arbitrary, just need min to be > 0
#define MIN_TRANSFORM_SCALE 0.001f //NOTE(james): for editor slider, not actually enforced
#define MAX_TRANSFORM_SCALE 10000.0f

internal void
ResolveLocalToWorld(transform* Transform)
{
    Assert(Transform != NULL);

    if (Transform->IsDirty)
    {
        matrix4 TranslateMatrix = Translate(Transform->LocalPosition);

        //v3 EulerRotation = V3(Transform->LocalRotation.x, Transform->LocalRotation.y, Transform->LocalRotation.z);
        matrix4 RotationMatrix = QuaternionToMatrixVersion2(Transform->LocalRotation);

        matrix4 ScaleMatrix = CreateScaleMatrix(Transform->LocalScale);

        matrix4 Local = TranslateMatrix * RotationMatrix * ScaleMatrix;

        if (Transform->Parent != NULL)
        {
            if (Transform->Parent->IsDirty)
            {
                ResolveLocalToWorld(Transform->Parent);
            }
            Transform->LocalToWorld = Transform->Parent->LocalToWorld * Local;

            Transform->Rotation = CombineQuaternions(Transform->Parent->Rotation, Transform->LocalRotation);

            Transform->Position = MatrixByPosition(&Transform->Parent->LocalToWorld, Transform->LocalPosition);
        }
        else
        {
            Transform->LocalToWorld = Local;
            Transform->Position = Transform->LocalPosition;
            Transform->Rotation = Transform->LocalRotation;
        }

        Transform->WorldToLocal = InverseMatrix(Transform->LocalToWorld);

        v4 RealRotation = MatrixToQuaternion(Transform->LocalToWorld);
        Transform->Forward = Normalize(RotateVectorByQuaternion(v3_FORWARD, RealRotation));
        Transform->Right = Normalize(RotateVectorByQuaternion(v3_RIGHT, RealRotation));
        Transform->Up = Normalize(RotateVectorByQuaternion(v3_UP, RealRotation));

#if 0 //TODO(james): this is what the solution should be but its not matching up to above solution
        v3 A = Normalize(RotateVectorByQuaternion(v3_FORWARD, Transform->Rotation));
        v3 B = Normalize(RotateVectorByQuaternion(v3_RIGHT, Transform->Rotation));
        v3 C = Normalize(RotateVectorByQuaternion(v3_UP, Transform->Rotation));
#endif

        Assert(AbsoluteValue(Inner(Transform->Right, Transform->Up)) < 0.0001f);
        Assert(AbsoluteValue(Inner(Transform->Right, Transform->Forward)) < 0.0001f);
        Assert(AbsoluteValue(Inner(Transform->Up, Transform->Forward)) < 0.0001f);

        Assert(Length(Transform->Position) < 10000);
        Assert(Length(Transform->LocalPosition) < 10000);

        Assert(Transform->LocalScale.x > 0);
        Assert(Transform->LocalScale.y > 0);
        Assert(Transform->LocalScale.z > 0);

        Assert(Transform->LocalScale.x < MAX_TRANSFORM_SCALE);
        Assert(Transform->LocalScale.y < MAX_TRANSFORM_SCALE);
        Assert(Transform->LocalScale.z < MAX_TRANSFORM_SCALE);

        //Assert(ApproximatelyEqual(Length(Transform->LocalRotation), 1));
        //Assert(ApproximatelyEqual(Length(Transform->Rotation), 1));

        Transform->IsDirty = false;

        if (Transform->Child != NULL)
        {
            transform* Child = Transform->Child;
            while (Child != NULL)
            {
                ResolveLocalToWorld(Child);
                Child = Child->NextSibling;
            }
        }
    }
}

inline transform
TransformTRSLocal(v3 Position, v4 Rotation, v3 Scale)
{
    transform Result = {};

    Result.LocalPosition = Position;
    Result.LocalRotation = Rotation;
    Result.LocalScale = Scale;

    Result.Parent = NULL;
    Result.Child = NULL;

    //Result.LocalToWorld = IdentityMatrix();
    //Result.Local = IdentityMatrix();

    Result.IsDirty = true;
    ResolveLocalToWorld(&Result);

    return Result;
}

inline void
TransformTRSLocal(transform* Transform, v3 Position, v4 Rotation, v3 Scale)
{
    Transform->LocalPosition = Position;
    Transform->LocalRotation = Rotation;
    Transform->LocalScale = Scale;

    Transform->Parent = NULL;
    Transform->Child = NULL;

    Transform->IsDirty = true;
    ResolveLocalToWorld(Transform);
}

inline transform
ZeroTransform()
{
    return TransformTRSLocal(v3_ZERO, v4_ZERO_ROTATION, v3_ONE);
}

inline void
InitializeTransform(transform* Transform)
{
    ZeroArray(128, Transform->__PathMemory);
    Transform->Path = { Transform->__PathMemory, 128 };

    ZeroArray(128, Transform->__SerializedPathMemory);
    Transform->SerializedPath = { Transform->__SerializedPathMemory, 128 };

    Transform->LocalPosition = { 0, 0, 0 };
    Transform->LocalScale = { 1, 1, 1 };
    Transform->LocalRotation = { 0, 0, 0, 1 };

    Transform->Position = { 0, 0, 0 };
    Transform->Rotation = { 0, 0, 0, 1 };

    Transform->Forward = { 0, 0, 0 };
    Transform->Right = { 0, 0, 0 };
    Transform->Up = { 0, 0, 0 };

    Transform->LocalToWorld = IdentityMatrix();

    Transform->IsDirty = false;

    Transform->Parent = NULL;
    Transform->Child = NULL; //NOTE(james):needs to be first in list
    Transform->NextSibling = NULL; //singly linked list
}

#if 0
v3 LocalToWorld(transform* Transform, v3 LocalMove)
{
    v3 EulerRotation = V3(Transform->Rotation.x, Transform->Rotation.y, Transform->Rotation.z);
    matrix4 RotationMatrix = QuaternionToMatrix2(EulerToQuaternion(EulerRotation));
 v4 LocalToWorld = RotationMatrix * ToV4(LocalMove,1);
    v3 Result = V3(LocalToWorld.x,LocalToWorld.y,LocalToWorld.z);
    
    return Result;
}
#endif

//NOTE(james): use this helper because it sets all children dirty as well!
inline void
SetDirty(transform* Transform)
{
    transform* CurrentChild = Transform->Child;
    while (CurrentChild != NULL)
    {
        CurrentChild->IsDirty = true;
        if (CurrentChild->Child != NULL)
        {
            SetDirty(CurrentChild);
        }
        CurrentChild = CurrentChild->NextSibling;
    }
    Transform->IsDirty = true;
}

inline void
AddChild(transform* Transform, transform* ChildToAdd)
{
    ChildToAdd->Parent = Transform;
    if (Transform->Child == NULL)
    {
        Transform->Child = ChildToAdd;
    }
    else
    {
        transform* CurrentChild = Transform->Child;
        while (CurrentChild->NextSibling != NULL)
        {
            CurrentChild = CurrentChild->NextSibling;
        }
        CurrentChild->NextSibling = ChildToAdd;
    }
    SetDirty(ChildToAdd);
    ResolveLocalToWorld(ChildToAdd);
}

inline void
RemoveChild(transform* Transform, transform* ChildToRemove)
{
    Assert(Transform->Child);

    // removed first child, but it had siblings, so just Child up one in list
    if (Transform->Child == ChildToRemove)
    {
        Transform->Child = ChildToRemove->NextSibling;
    }
    else if (Transform->Child->NextSibling == NULL) // only had one child
    {
        Transform->Child = NULL;
    }
    else
    {
        transform* FirstChild = Transform->Child;
        transform* PreviousChild = NULL;
        transform* CurrentChild = Transform->Child;
        while (CurrentChild->NextSibling != NULL)
        {
            // patch up linked list if the child being removed isn't first in line
            if (CurrentChild == ChildToRemove && PreviousChild != NULL)
            {
                PreviousChild->NextSibling = CurrentChild->NextSibling;
                break;
            }
            else
            {
                PreviousChild = CurrentChild;
                CurrentChild = CurrentChild->NextSibling;
            }
        }
        //end of list
        if (ChildToRemove == CurrentChild)
        {
            PreviousChild->NextSibling = NULL;
        }
    }
    ChildToRemove->Parent = NULL;
    ChildToRemove->NextSibling = NULL;

    SetDirty(ChildToRemove);
    ResolveLocalToWorld(ChildToRemove);
}

inline void
SetTransformParent(transform* Transform, transform* Parent)
{
    if (Transform->Parent != NULL)
    {
        RemoveChild(Transform->Parent, Transform);
    }

    Transform->Parent = Parent;
    AddChild(Parent, Transform);
    SetDirty(Transform);
    ResolveLocalToWorld(Transform);
}

inline void
RemoveTransformParent(transform* Transform)
{
    Assert(Transform->Parent);
    RemoveChild(Transform->Parent, Transform);
    Transform->Parent = NULL;
    SetDirty(Transform);
    ResolveLocalToWorld(Transform);
}

inline void
SetLocalPosition(transform* Transform, v3 Position, bool32 UpdateMatrix = true)
{
    Transform->LocalPosition = Position;
    SetDirty(Transform);

    if (UpdateMatrix)
    {
        ResolveLocalToWorld(Transform);
    }
}

/*
//TODO(james):TEST THIS!!
inline void 
SetRotationLookAt(transform* Transform, v3 LookDirection)
{
    v4 RightRotate = EulerToQuaternion(V3(0, PI/2, 0));
    v4 UpRotate = EulerToQuaternion(V3(PI/2, 0, 0));
    
    v3 RightDirection = RotateVectorByQuaternion(LookDirection, RightRotate);
    v3 UpDirection = RotateVectorByQuaternion(LookDirection, UpRotate);
    
    matrix4 LookAtMatrix = ObjectLookAt(RightDirection, UpDirection, LookDirection, Transform->LocalPosition);// TODO(james): should probably be world position?
    v4 LookAtRotation = MatrixToQuaternion(&LookAtMatrix);
    LookAtRotation = Normalize(LookAtRotation);
    SetDirty(Transform);
}
*/

inline void
SetLocalRotation(transform* Transform, v4 Quaternion, bool32 UpdateMatrix = true)
{
    Transform->LocalRotation = Quaternion;
    SetDirty(Transform);

    if (UpdateMatrix)
    {
        ResolveLocalToWorld(Transform);
    }
}

//TODO(james):TEST THIS!!
inline void
SetForward(transform* Transform, v3 Forward)
{
    Assert(Forward != v3_ZERO);
    Forward = Normalize(Forward);
    real32 ForwardLength = Length(Forward);
    Assert(ApproximatelyEqual(ForwardLength, 1.0f));

    ResolveLocalToWorld(Transform);
    v3 Right = Normalize(V3(Forward.z, 0, -Forward.x));
    v3 Up = Normalize(Cross(Forward, Right));
    matrix4 LookAtMatrix = ObjectLookAt(Right, Up, Forward, Transform->Position);
    v4 Rotation = MatrixToQuaternion(LookAtMatrix);
    //TODO(james): Rotation has to be inversed, I think the ObjectLookAt is incorrect
    Rotation = InverseQuaternion(Rotation);

    SetLocalRotation(Transform, Rotation);
}

inline void
RotateLocal(transform* Transform, v4 RotationDelta, bool32 UpdateMatrix = true)
{
    Transform->LocalRotation = CombineQuaternions(Transform->LocalRotation, RotationDelta);
    SetDirty(Transform);

    if (UpdateMatrix)
    {
        ResolveLocalToWorld(Transform);
    }
}

inline void
SetLocalScale(transform* Transform, v3 Scale, bool32 UpdateMatrix = true)
{
    Transform->LocalScale = Scale;
    SetDirty(Transform);

    if (UpdateMatrix)
    {
        ResolveLocalToWorld(Transform);
    }
}

inline void
SetScale(transform* Transform, v3 Scale, bool32 UpdateMatrix = true)
{
    if (Transform->Parent)
    {
        v3 ParentScale = Transform->Parent->LocalScale;
        ParentScale.x = SafeRatio0(1, ParentScale.x);
        ParentScale.y = SafeRatio0(1, ParentScale.y);
        ParentScale.z = SafeRatio0(1, ParentScale.z);

        v3 LocalScale = Hadamard(Scale, ParentScale);
        SetLocalScale(Transform, LocalScale, UpdateMatrix);
    }
    else
    {
        SetLocalScale(Transform, Scale, UpdateMatrix);
    }
}

inline void
SetPosition(transform* Transform, v3 Position, bool32 UpdateMatrix = true)
{
    if (Transform->Parent != NULL)
    {
        v3 LocalPosition = MatrixByPosition(&Transform->Parent->WorldToLocal, Position);
        //LocalPosition += Transform->LocalPosition;
        SetLocalPosition(Transform, LocalPosition, UpdateMatrix);
    }
    else
    {
        SetLocalPosition(Transform, Position, UpdateMatrix);
    }
}

inline void
SetWorldRotation(transform* Transform, v4 Quaternion, bool32 UpdateMatrix = true)
{
    if (Transform->Parent != NULL)
    {
        v4 InverseRotation = InvertQuaternion(Transform->Parent->Rotation);
        v4 NewRotation = CombineQuaternions(Quaternion, InverseRotation);
        SetLocalRotation(Transform, NewRotation, UpdateMatrix);
    }
    else
    {
        SetLocalRotation(Transform, Quaternion, UpdateMatrix);
    }
}

inline void
MoveLocal(transform* Transform, v3 MoveAmount, bool32 UpdateMatrix = true)
{
    Transform->LocalPosition += MoveAmount;

    SetDirty(Transform);

    if (UpdateMatrix)
    {
        ResolveLocalToWorld(Transform);
    }
}

inline void
MoveWorld(transform* Transform, v3 MoveAmount, bool32 UpdateMatrix = true)
{
    SetPosition(Transform, Transform->Position + MoveAmount, UpdateMatrix);
}

inline v3
GetWorldPositionFromLocalOffset(transform* Transform, v3 LocalOffset)
{
    v3 Result;

    Result = MatrixByPosition(&Transform->LocalToWorld, LocalOffset);

    return Result;
}

inline char*
GetName(transform* Transform)
{
    uint32 LastSlashIndex = C_FindLastOf(Transform->Path.Text, '/');
    char* Result = &Transform->Path.Text[LastSlashIndex + 1];
    return Result;
}

inline void
SetRootName(transform* Transform, char* Name)
{
    STRING(Path, 128);
    AppendStringEx(&Path, "/%s", Name);
    SetString(&Transform->Path, Path);
}

#define YAMGINE_TRANSFORM_H
#endif