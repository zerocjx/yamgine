//#include <imgui.h>
#include "yamgine_gui.h"

bool32 DrawButton(render_list* RenderList, rectangle2 DrawingRect, game_input* Input)
{
    bool32 Result = false;

    v2 MousePos = Input->Controllers[0].MousePosition - RenderList->Viewport.Min;
    bool32 MouseClick = Input->Controllers[0].LeftMouseClick.DownThisFrame;
    bool32 MouseHold = Input->Controllers[0].LeftMouseClick.IsDown;

    if (IsInRectangle(DrawingRect, MousePos))
    {
        if (MouseClick) //down
        {
            Result = true;
        }

        if (MouseClick || MouseHold)
        {
            DrawColorRect(RenderList, DrawingRect, v4_WHITE * 0.3f);
        }
        else //hover
        {
            DrawColorRect(RenderList, DrawingRect, v4_WHITE * 0.25f);
        }
    }
    else
    {
        DrawColorRect(RenderList, DrawingRect, v4_WHITE * 0.2f);
    }

    return Result;
}

//returns if it was clicked this frame
bool32 DrawToggle(render_list* RenderList, bool32* Toggle, rectangle2 DrawingRect, game_input* Input)
{
    bool32 Result = false;

    v2 MousePos = Input->Controllers[0].MousePosition - RenderList->Viewport.Min;
    bool32 MouseClick = Input->Controllers[0].LeftMouseClick.DownThisFrame;
    bool32 MouseHold = Input->Controllers[0].LeftMouseClick.IsDown;

    if (IsInRectangle(DrawingRect, MousePos))
    {
        if (MouseClick) //down
        {
            Result = true;
            *Toggle = !(*Toggle);
        }

        if (MouseClick || MouseHold)
        {
            DrawColorRect(RenderList, DrawingRect, v4_WHITE * 0.3f);
        }
        else //hover
        {
            DrawColorRect(RenderList, DrawingRect,  v4_WHITE * 0.25f);
        }
    }
    else
    {
        DrawColorRect(RenderList, DrawingRect, v4_WHITE * 0.2f);
    }

    if (*Toggle)
    {
        DrawColorRect(RenderList, SubRadiusFrom(DrawingRect, V2(4, 4)), v4_WHITE);
    }

    return Result;
}

void DrawSlider(render_list* RenderList, real32* Val, real32 Min, real32 Max,
                rectangle2 DrawingRect, game_input* Input)
{
    v2 MousePos = Input->Controllers[0].MousePosition - RenderList->Viewport.Min;
    bool32 MouseHold = Input->Controllers[0].LeftMouseClick.IsDown;

    real32 Progress = InverseLerp(Min, Max, *Val);

    if (IsInRectangle(DrawingRect, MousePos))
    {
        DrawColorRect(RenderList,DrawingRect, v4_WHITE * 0.25f);
        if (MouseHold)
        {
            Progress = InverseLerp(DrawingRect.Min.x,
                                   DrawingRect.Max.x,
                                   MousePos.x);
            *Val = Lerp(Min, Max, Progress);
        }
    }
    else
    {
        DrawColorRect(RenderList, DrawingRect, v4_WHITE * 0.2f);
    }

    v2 DrawingRectDim = GetDim(DrawingRect);

    real32 PipWidth = 6;
    v2 PipPosition = DrawingRect.Min;
    PipPosition.x += Lerp(0, DrawingRectDim.x - PipWidth, Progress);
    PipPosition.y += 2;
    DrawColorRect(RenderList, RectMinDim(PipPosition, V2(PipWidth, DrawingRectDim.y - 4)), v4_WHITE);
}

//
// TEXT
//
void LoadFont(font* Result, memory_arena* TextureArena, render_assets* Assets, temporary_memory* Temp, platform_api* Platform)
{
    Result->TextureAtlas = LoadTexture("glyphs.bmp", TextureArena, Assets, Temp, Platform);

    Result->Glyphs = PushArray(TextureArena, 94, glyph_table_entry);
    Result->GlyphCount = 94;

    char* GlyphTable = LoadTextFile("glyphs.table", Temp, Platform);
    tokenizer Tokenizer = {};
    Tokenizer.At = GlyphTable;

    for (int GlyphIndex = 0; GlyphIndex < 94; ++GlyphIndex)
    {
        AssertToken(&Tokenizer, Token_Identifier); // struct type
        AssertToken(&Tokenizer, Token_Identifier); // struct identifier

        AssertToken(&Tokenizer, Token_Equals); // equals sign

        glyph_table_entry* Entry = &Result->Glyphs[GlyphIndex];
        DeserializeStruct(&Tokenizer, Entry,
                          MembersOf_glyph_table_entry, ArrayCount(MembersOf_glyph_table_entry));

        AssertToken(&Tokenizer, Token_Semicolon); // semi colon
    }
}

void DrawGlyph(render_list* List, glyph_table_entry* Glyph,
               real32 x, real32 y, texture* TextureAtlas, real32 Scale)
{
    rectangle2 DrawingRect;
    DrawingRect.Min.x = x + Glyph->BoundingBox.Min.x;
    DrawingRect.Min.y = y + Glyph->BoundingBox.Min.y;

    DrawingRect.Max.x = x + (Glyph->BoundingBox.Max.x * Scale);
    DrawingRect.Max.y = y + (Glyph->BoundingBox.Max.y * Scale);

    if (DrawingRect.Max.y < DrawingRect.Min.y)
    {
        real32 Store = DrawingRect.Max.y;
        DrawingRect.Max.y = DrawingRect.Min.y;
        DrawingRect.Min.y = Store;
    }

    rectangle2 AtlasBox = Glyph->AtlasBox;
    //AtlasBox.Min *= 0.99;
    //AtlasBox.Max *= 0.99;
    DrawGlyphRect(List, TextureAtlas, DrawingRect, AtlasBox, v4_WHITE);
}

void DrawText(render_list* List, char* Text, real32 x, real32 y, font* Font, real32 Scale)
{
    real32 xPos = x;
    real32 yPos = y;
    while (*Text != '\0')
    {
        uint32 GlyphIndex = (*Text) - 32;

        if (*Text == '\n')
        {
            yPos -= 32.0f * Scale;
            xPos = x;
        }
        else if (GlyphIndex >= 0 && GlyphIndex < 94)
        {
            glyph_table_entry* Glyph = &Font->Glyphs[GlyphIndex];
            DrawGlyph(List, Glyph, xPos, yPos, Font->TextureAtlas, Scale);

            xPos += Glyph->Advance * Scale;
        }

        ++Text;
    }
}
