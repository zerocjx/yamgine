                             struct texture_load
                             {
                                 texture* Texture;
                                 uint8* FileData;
                                 platform_api* PlatformAPI;
                             };

                             internal PLATFORM_WORK_QUEUE_CALLBACK(FillBmpAsync)
                             {
                                 texture_load* Load = (texture_load*)Data;
                                 
                                 uint8* BmpData = Load->FileData;
                                 texture* Texture = Load->Texture;
                                 platform_api* PlatformAPI = Load->PlatformAPI;
                                 
                                 struct bitmap_file_header
                                 {
                                     uint16 Signature;
                                     uint32 FileSize;
                                     uint16 Reserved1;
                                     uint16 Reserved2;
                                     uint32 FileOffset;
                                 };
                                 bitmap_file_header* BitmapFileHeader = (bitmap_file_header*)BmpData;
                                 BmpData+= 14; //sizeof(bitmap_file_header) without any padding
                                 
                                 struct dib_header
                                 {
                                     uint32 HeaderSize;
                                     
                                     uint32 ImageWidth;
                                     uint32 ImageHeight;
                                     
                                     uint16 Planes;
                                     
                                     uint16 BitsPerPixel;
                                     
                                     uint32 Compression;
                                     uint32 ImageSize;
                                     
                                     uint32 XPixelsPerMeter;
                                     uint32 YPixelsPerMeter;
                                     uint32 ColorsInColorTable;
                                     uint32 ImportantColorCount;
                                     
                                     uint32 RedChannelBitmask;
                                     uint32 BlueChannelBitmask;
                                     uint32 GreenChannelBitmask;
                                     uint32 AlphaChannelBitmask;
                                     //more
                                 };
                                 
                                 dib_header* DibHeader = (dib_header*)BmpData;
                                 BmpData += DibHeader->HeaderSize;
                                 
                                 Assert(DibHeader->ImageHeight > 0);
                                 
                                 if (DibHeader->BitsPerPixel == 24)
                                 {
                                     Assert(DibHeader->Compression == 0 ); //NOTE(james): no compression
                                     Texture->ChannelType = TextureChannelType_RGB;
                                 }
                                 else if (DibHeader->BitsPerPixel == 32)
                                 {
                                     // 3 == BI_BITFIELDS
                                     Assert(DibHeader->Compression == 3 ); //NOTE(james): no compression
                                     Texture->ChannelType = TextureChannelType_RGBA;
                                 }
                                 
                                 Texture->Width = DibHeader->ImageWidth;
                                 Texture->Height = DibHeader->ImageHeight;
                                 Texture->CompressionType = TextureCompressionType_Uncompressed;
                                 
                                 uint32 BytesPerPixel = (DibHeader->BitsPerPixel / 8);
                                 uint32 RowWidth = DibHeader->ImageWidth * BytesPerPixel;
                                 Assert(RowWidth % 4 == 0); //rows are padded to be 4-byte aligned, above calculation doesn't account for that
                                 //Assert(DibHeader->BitsPerPixel == 24); // NOTE(james): we dont handle anything but RGB yet
                                 
                                 for (uint32 HeightIndex = 0;
                                      HeightIndex < Texture->Height;
                                      ++HeightIndex)
                                 {
                                     for (uint32 WidthIndex = 0;
                                          WidthIndex < RowWidth;
                                          WidthIndex += BytesPerPixel)
                                     {
                                         uint32 ComputedIndex = (HeightIndex * RowWidth) + WidthIndex;
                                         uint8* PixelPtr = &BmpData[ComputedIndex];
                                         uint32* PixelData = (uint32*)PixelPtr;
                                         
                                         // NOTE(james): get a byte index into the destination texture. We must also flip the Y so
                                         // it starts in the bottom left, not the top left
                                         uint32 TextureIndex = (HeightIndex * RowWidth) + WidthIndex;
                                         
                                         uint8* DestinationPtr = (uint8*)&Texture->Data[TextureIndex];
                                         Assert(DestinationPtr < ((uint8*)Texture->Data) + Texture->SizeOnDisk);
                                         Assert(DestinationPtr >= ((uint8*)Texture->Data));
                                         
                                         uint32* DestinationData = (uint32*)DestinationPtr;
                                         
                                         *DestinationData = *PixelData;
                                     }
                                 }
                                 
                                 PlatformAPI->AllocateTexture(Texture);
                             }

                             uint32 BmpGetSize(uint8* DataOrig)
                             {
                                 uint8* Data = DataOrig;
                                 struct bitmap_file_header
                                 {
                                     uint16 Signature;
                                     uint32 FileSize;
                                     uint16 Reserved1;
                                     uint16 Reserved2;
                                     uint32 FileOffset;
                                 };
                                 bitmap_file_header* BitmapFileHeader = (bitmap_file_header*)Data;
                                 Data += 14; //sizeof(bitmap_file_header) without any padding
                                 
                                 struct dib_header
                                 {
                                     uint32 HeaderSize;
                                     
                                     uint32 ImageWidth;
                                     uint32 ImageHeight;
                                     
                                     uint16 Planes;
                                     
                                     uint16 BitsPerPixel;
                                     
                                     uint32 Compression;
                                     uint32 ImageSize;
                                     
                                     uint32 XPixelsPerMeter;
                                     uint32 YPixelsPerMeter;
                                     uint32 ColorsInColorTable;
                                     uint32 ImportantColorCount;
                                     
                                     uint32 RedChannelBitmask;
                                     uint32 BlueChannelBitmask;
                                     uint32 GreenChannelBitmask;
                                     uint32 AlphaChannelBitmask;
                                     //more
                                 };
                                 
                                 dib_header* DibHeader = (dib_header*)Data;
                                 
                                 return DibHeader->ImageSize;
                             }
                             
inline void
                                 DecodeBmp(texture* Texture, uint8* FileData, memory_arena* AssetsArena, temporary_memory* TempMem,
                                           platform_api* PlatformAPI)
{
    texture_load* Load = PushStruct(TempMem->Arena,texture_load);
    
    Load->Texture = Texture;
    Load->FileData = FileData;
    Load->PlatformAPI = PlatformAPI;
    PlatformAPI->AddEntry(WorkQueue, FillBmpAsync, Load);
    //PlatformAPI->CompleteAllWork(WorkQueue);
}

/*
opengl loads these flipped vertically, so you have to load upside images
*/
inline texture*
DecodeDxt1(uint8* Data, memory_arena* AssetsArena)
{
    texture* Texture = PushStruct(AssetsArena, texture);

    // MSDN DDS_PIXELFORMAT
    struct pixel_format
    {
        uint32 Size; // size of this struct, so 32
        uint32 Flags;
        uint32 FourCC; // type of compression as a 4 character string, so "DXT1", "DXT3", etc.
        uint32 RGBBitCount;
        uint32 RBitMask;
        uint32 GBitMask;
        uint32 BBitMask;
        uint32 ABitMask;
    };

    // MSDN DDS_HEADER
    struct file_header
    {
        uint32 Name; //technically not part of header
        uint32 HeaderSize;
        uint32 Flags;
        uint32 Height; //in pixels
        uint32 Width; //in pixels
        uint32 SizeTexture; //not exactly sure what size this is
        uint32 Depth;
        uint32 MipMapCount;
        uint32 Reserved[11];

        pixel_format PixelFormat;

        uint32 Caps1;
        uint32 Caps2;
        uint32 Caps3;
        uint32 Caps4;
        uint32 Reserved2;
    };

    file_header* FileHeader = (file_header*)Data;
    Data += sizeof(file_header);

    char DxtType[4];
    DxtType[0] = (char)FileHeader->PixelFormat.FourCC;
    DxtType[1] = (char)(FileHeader->PixelFormat.FourCC >> 8);
    DxtType[2] = (char)(FileHeader->PixelFormat.FourCC >> 16);
    DxtType[3] = (char)(FileHeader->PixelFormat.FourCC >> 24);
    Assert(StringsAreEqualAbsolute(DxtType, "DXT1")); //TODO(james): currently only handling DXT1 in OpenGL load

    Texture->Width = FileHeader->Width;
    Texture->Height = FileHeader->Height;
    Texture->CompressionType = TextureCompressionType_Compressed;
    Texture->Data = Data;
    Texture->SizeOnDisk = FileHeader->SizeTexture;

    return Texture;
}

inline texture*
DecodeTga(uint8* Data, int32 Size, memory_arena* AssetsArena)
{
    texture* Texture = PushStruct(AssetsArena, texture);

    struct file_header
    {
        uint8 IDLength;
        uint8 ColorMapType;
        /*
           0 - no image data
           1 - uncompressed color-mapped image
           2 - uncompressed true-color image
           3 - uncompressed grayscale
           9 - RLE color-mapped
           10 - RLE true-color
           11 - RLE grayscale
        */
        uint8 ImageType;
        uint8 ColorMapSpec[5];

        uint16 XOrigin;
        uint16 YOrigin;
        uint16 Width;
        uint16 Height;
        uint8 PixelDepth;
        uint8 ImageDescriptor;
    };

    file_header* Header = (file_header*)Data;

    Assert(Header->ImageType == 2);
    Assert(Header->XOrigin == 0);
    Assert(Header->YOrigin == 0);

    Assert(Header->PixelDepth == 24); // TODO(james): we dont handle anything but RGB yet, can we get alpha at least?

    uint32 BytesPerPixel = (Header->PixelDepth / 8);

    uint32 ImageSize = Header->Width * Header->Height * BytesPerPixel;

    Texture->Width = Header->Width;
    Texture->Height = Header->Height;
    Texture->CompressionType = TextureCompressionType_Uncompressed;
    Texture->Data = PushArray(AssetsArena, ImageSize, uint8);

    struct file_footer
    {
        uint32 ExtensionOffset;
        uint32 DeveloperOffset;
        char Signature[18];
        char Align[2]; // extra bytes here for word alignment
    };

    file_footer* Footer = (file_footer*)(Data + Size - sizeof(file_footer) + 2 /*to handle align*/);
    /*
    Assert(StringsAreEqual(Footer->Signature, "TRUEVISION-XFILE."));
    Assert(Footer->ExtensionOffset == 0);
    Assert(Footer->DeveloperOffset == 0);
    */
    // as opposed to BMP, TGA rows don't have to be aligned to 4 byte boundaries
    uint32 RowWidth = Header->Width * BytesPerPixel;

    //
    // Load Data In
    //

    Data += 18; //actual size of header

    for (uint32 HeightIndex = 0;
         HeightIndex < Header->Height;
         ++HeightIndex)
    {
        for (uint32 WidthIndex = 0;
             WidthIndex <= RowWidth;
             WidthIndex += BytesPerPixel)
        {
            //
            // find ptr to desired source data
            //
            uint32 SourceIndex = (HeightIndex * RowWidth) + WidthIndex;
            uint8* SourcePtr = &Data[SourceIndex];
            uint32* SourceData = (uint32*)SourcePtr;

            //
            // find ptr to destination data
            //
            uint32 DestinationIndex = (RowWidth * HeightIndex) + WidthIndex;
            uint8* DestinationPtr = (uint8*)&Texture->Data[DestinationIndex];
            uint32* DestinationData = (uint32*)DestinationPtr; //last 8 bits are overwritten on the next write...

            *DestinationData = *SourceData;
        }
    }

    return Texture;
}

texture* LoadTexture(char* Filename, memory_arena* AssetsArena, render_assets* Assets, temporary_memory* Temp, platform_api* PlatformAPI)
{
    YamProf(LoadTexture);
    texture* Texture = NULL;
    
    uint64 FileSize = 0;
    uint8* FileData = (uint8*)LoadBinaryFile(Filename, Temp, PlatformAPI, &FileSize);
    
    char* FileExtension = &Filename[FindLastOf(Filename, '.')];

    if (StringsAreEqualAbsolute(FileExtension, ".bmp"))
    {
        Texture = PushStruct(AssetsArena, texture);
        uint32 Size = BmpGetSize(FileData);
        Texture->Data = PushArray(Temp->Arena, Size, uint8);
        Texture->SizeOnDisk = Size;
        DecodeBmp(Texture, FileData, AssetsArena, Temp, PlatformAPI);
    }
    else if (StringsAreEqualAbsolute(FileExtension, ".dds"))
    {
        Texture = DecodeDxt1(FileData, AssetsArena);
    }
    else if (StringsAreEqualAbsolute(FileExtension, ".tga"))
    {
        Texture = DecodeTga(FileData, FileSize, AssetsArena);
    }
    else
    {
        InvalidCodePath; // file wasn't a supported texture type
    }
    
    CopyString(Filename, Texture->DebugName, 32);
    AddTexture(Assets, Texture);

    return Texture;
}
