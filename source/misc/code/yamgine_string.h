#if !defined(YAMGINE_STRING_H)

//TODO(james): is there a better way to allocate space on the stack?
#define STRING(Identifier, RequestedSize) \
    \
string Identifier                         \
        = {};                             \
    \
char _Text_##Identifier[RequestedSize]    \
        = {};                             \
    \
Identifier.Text                           \
        = &_Text_##Identifier[0];         \
    \
Identifier.Size                           \
        = RequestedSize

/*
#define STRING_INIT(Identifier, RequestedSize, Init) \
string Identifier = {}; \
char _Text_##Identifier[RequestedSize] = Init; \
Identifier.Text = &_Text_##Identifier[0];  \
Identifier.Size = RequestedSize
*/
inline bool32
IsEndOfLine(char C)
{
    bool32 Result = ((C == '\n') || (C == '\r'));

    return (Result);
}

inline bool32
IsWhitespace(char C)
{
    bool32 Result = ((C == ' ') || (C == '\t') || (C == '\v') || (C == '\f') || IsEndOfLine(C));

    return (Result);
}

uint32 StringLength(char* String)
{
    uint32 Result = 0;

    while (String[Result] != '\0')
    {
        Result++;
    }

    return Result;
}

uint32 StringLength(string String)
{
    return StringLength(String.Text);
}

// strings are equal in length and content
inline bool32
StringsAreEqualAbsolute(char* A, char* B)
{
    bool32 Result = false;

    if (StringLength(A) == StringLength(B))
    {
        if (A && B)
        {
            while (*A && *B && (*A == *B))
            {
                ++A;
                ++B;
            }

            Result = ((*A == 0) && (*B == 0));
        }
    }

    return (Result);
}

inline bool32
StringsAreEqualAbsolute(string A, char* B)
{
    return StringsAreEqualAbsolute(A.Text, B);
}

//one string may be longer than the other, but their union is equal
//"bill" and "billy" == true
//"billitha" and "billy" == false
inline bool32
StringsAreEqualUnion(char* A, char* B)
{
    bool32 Result = false;
    if (A && B)
    {
        Result = true;

        while (true)
        {
            if (*A != *B)
            {
                Result = false;
                break;
            }
            ++A;
            ++B;

            if (*A == '\0' || *B == '\0')
            {
                break;
            }
        }
    }

    return Result;
}

inline bool32
StringsAreEqualUnion(string A, char* B)
{
    return StringsAreEqualUnion(A.Text, B);
}

// 4 "billy" "billitha" == false
// 4 "billy" "bill" == true
inline bool32
StringsAreEqualAbsolute(uintptr ALength, char* A, char* B)
{
    bool32 Result = true;
    if (StringLength(B) != ALength)
    {
        Result = false;
    }
    else
    {
        for (uintptr Index = 0;
             Index < ALength;
             ++Index)
        {
            if ((B[Index] == 0) || (A[Index] == 0) || (A[Index] != B[Index]))
            {
                return (false);
            }
        }
    }

    return (Result);
}

// 4 "billy" "billitha" == true
inline bool32
StringsAreEqualUnion(uintptr ALength, char* A, char* B)
{
    bool32 Result = true;
    for (uintptr Index = 0;
         Index < ALength;
         ++Index)
    {
        if ((B[Index] == 0) || (A[Index] == 0) || (A[Index] != B[Index]))
        {
            return (false);
        }
    }

    return (Result);
}

inline bool32 StringContains(char* Haystack, char* Needle)
{
    bool32 Result = false;

    uint32 HaystackLength = StringLength(Haystack);
    uint32 NeedleLength = StringLength(Needle);

    if (NeedleLength <= HaystackLength)
    {
        for (uint32 HaystackIndex = 0;
             HaystackIndex <= HaystackLength - NeedleLength;
             ++HaystackIndex)
        {
            if (StringsAreEqualUnion(Needle, &Haystack[HaystackIndex]))
            {
                Result = true;
            }
        }
    }

    return Result;
}

uint32 ToString(string* String, uint32 Input)
{
    uint32 Index = 0;
    char Characters[] = "0123456789";
    uint32 Divisor = 1000000000;
    while (Divisor > 1 && Index < String->Size)
    {
        if (Input >= Divisor)
        {
            uint32 Term = Input / Divisor;
            String->Text[Index++] = Characters[Term];
            Input = Input - (Term * Divisor);

            Assert(Index < String->Size);
        }
        Divisor = Divisor / 10;
    }
    //down to something between 0 and 9 inclusive
    String->Text[Index++] = Characters[Input];
    String->Text[Index] = '\0';

    Assert(Index < String->Size);
    return Index;
}

uint32 ToString(string* String, int32 Input)
{
    uint32 Length;
    if (Input < 0)
    {
        String->Text[0] = '-';

        //create a string that points to one past the minus sign
        string UnsignedPart = {};
        UnsignedPart.Text = &String->Text[1];
        UnsignedPart.Size = String->Size - 1;

        Length = ToString(&UnsignedPart, (uint32)AbsoluteValue(Input));
        Length++;
    }
    else
    {
        Length = ToString(String, (uint32)Input);
    }

    return Length;
}

#define SubString(FullString, Start, Length, DestinationString, DestinationStringLength) \
SubString_(FullString, StringLength(FullString), Start, Length, DestinationString, DestinationStringLength)
void SubString_(
    char* FullString, uint32 FullStringLength,
    uint32 Start, uint32 Length,
    char* DestinationString, uint32 DestinationStringLength)
{
    uint32 DestinationIndex = 0;
    for (uint32 FullStringIndex = Start;
         FullStringIndex < Min(Start + Length, Start + DestinationStringLength);
         FullStringIndex++, DestinationIndex++)
    {
        DestinationString[DestinationIndex] = FullString[FullStringIndex];
    }
    DestinationString[DestinationIndex] = '\0'; //is this necessary?
}

void CopyString(char* Input, char* Destination, uint32 DestinationBufferSize)
{
    Assert(StringLength(Input) > 0);

    SubString(Input, 0, StringLength(Input), Destination, DestinationBufferSize);
}

void AppendString_(char* ExistingBuffer, uint32 ExistingBufferSize, char* ToAppend, uint32 ToAppendLength)
{
    Assert(ExistingBufferSize > 0);
    Assert(ToAppendLength > 0);

    uint32 EndOfExistingBuffer = StringLength(ExistingBuffer);
    Assert(ExistingBufferSize >= (ToAppendLength + EndOfExistingBuffer));

    uint32 ExistingBufferIndex = EndOfExistingBuffer;
    uint32 ToAppendIndex = 0;
    while (ToAppendIndex < ToAppendLength)
    {
        ExistingBuffer[ExistingBufferIndex++] = ToAppend[ToAppendIndex++];
    }
    ExistingBuffer[ExistingBufferIndex] = '\0';
}

void AppendSubstring(string* Destination, char* ToAppend, uint32 ToAppendLength)
{
    uint32 EndOfDestinationString = StringLength(Destination->Text) - 1; //-1 to overwrite nullterminator
    Assert(Destination->Size > (ToAppendLength + EndOfDestinationString));

    uint32 DestinationBufferIndex = EndOfDestinationString;
    uint32 ToAppendIndex = 0;
    do
    {
        Destination->Text[++DestinationBufferIndex] = ToAppend[ToAppendIndex++];
    } while (ToAppendIndex < ToAppendLength);
    Destination->Text[++DestinationBufferIndex] = '\0';
}

void AppendString(string* Destination, string String)
{
    AppendString_(Destination->Text, Destination->Size, String.Text, StringLength(String.Text));
}

void AppendString(string* Destination, char* String)
{
    AppendString_(Destination->Text, Destination->Size, String, StringLength(String));
}

void AppendStringEx(string* Destination, char* Format, ...)
{
    va_list Args;
    va_start(Args, Format);

    //NOTE(james): getting garbage here? make sure you are passing char*, not a string!
    char* Argument = va_arg(Args, char*);
    while (*Format != '\0')
    {
        if (*Format == '%')
        {
            Format++;
            if (*Format == 's')
            {
                AppendString_(Destination->Text, Destination->Size, Argument, StringLength(Argument));
                Argument = va_arg(Args, char*);
            }
        }
        else
        {
            AppendString_(Destination->Text, Destination->Size, Format, 1);
        }

        Format++;
    }
    va_end(Args);
}

uint32 FindFirstOf(char* Haystack, char Needle)
{
    uint32 Result = MAX_UINT32;

    uint32 HaystackIndex = 0;
    while (Haystack[HaystackIndex] != '\0')
    {
        if (Haystack[HaystackIndex] == Needle)
        {
            Result = HaystackIndex;
            break;
        }
        else
        {
            ++HaystackIndex;
        }
    }

    return Result;
}

uint32 FindLastOf(char* Haystack, char Needle)
{
    uint32 Result = MAX_UINT32;

    uint32 Index = StringLength(Haystack);

    while (Index >= 0)
    {
        if (Haystack[Index] == Needle)
        {
            Result = Index;
            break;
        }
        else
        {
            --Index;
        }
    }

    return Result;
}

//TODO(james): memset?
void ClearString(string* String)
{
    for (uint32 Index = 0;
         Index < String->Size;
         ++Index)
    {
        String->Text[Index] = '\0';
    }
}

//c:/folder/filename.exe -> filename
void ExtractFileName(string* Destination, char* FilePath)
{
    uint32 LastSlashLocation = FindLastOf(FilePath, '/') + 1;
    uint32 LastPeriodLocation = FindLastOf(FilePath, '.');

    ClearString(Destination);
    AppendSubstring(Destination, FilePath + LastSlashLocation, LastPeriodLocation - LastSlashLocation);
}

//c:/folder/foldername -> foldername
void ExtractFolderName(string* Destination, char* FilePath)
{
    uint32 LastSlashLocation = FindLastOf(FilePath, '/') + 1;

    ClearString(Destination);
    AppendSubstring(Destination, FilePath + LastSlashLocation, StringLength(FilePath) - LastSlashLocation);
}

//"asdf/fdas" -> "asdf"
void RemoveLastSlashContent(string* String)
{
    uint32 Loc = FindLastOf(String->Text, '/');
    char* Ptr = String->Text + Loc;
    while (*Ptr != '\0')
    {
        *Ptr = '\0';
        ++Ptr;
    }
}

void ClearString(char* String, uint32 Size)
{
    for (uint32 Index = 0;
         Index < Size;
         ++Index)
    {
        String[Index] = '\0';
    }
}

void SetString(string* Destination, string Source)
{
    ClearString(Destination);
    AppendString(Destination, Source);
}

void SetString(string* Destination, char* Source)
{
    ClearString(Destination);
    AppendString(Destination, Source);
}

void Indent(string* String, int IndentLevel)
{
    for (int i = 0; i < IndentLevel; i++)
    {
        AppendString(String, "    ");
    }
}

#define YAMGINE_STRING_H
#endif