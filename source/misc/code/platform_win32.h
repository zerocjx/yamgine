#if !defined(PLATFORM_WIN32_H)

#include "renderer_opengl.h" // includes a lot of our other code as well, plus windows.h
#include <xinput.h>

#include "DbgHelp.h"

#ifdef FMOD_SUPPORT
#include "Objbase.h" //NOTE(james): needed by fmod
#endif

struct platform_work_queue_entry
{
    platform_work_queue_callback *Callback;
    void *Data;
};

#define WORKER_THREAD_COUNT 6
struct platform_work_queue
{
    uint32 volatile CompletionGoal;
    uint32 volatile CompletionCount;
    
    uint32 volatile NextEntryToWrite;
    uint32 volatile NextEntryToRead;
    HANDLE SemaphoreHandle;
    
    platform_work_queue_entry Entries[256];
    
    HDC WindowDC;
};

struct win32_thread_startup
{
    platform_work_queue *Queue;
    HGLRC GLContext;
};

struct win32_replay_buffer
{
    HANDLE FileHandle;
    HANDLE MemoryMap;
    char __FileNameMemory[MAX_PATH];
    string FileName{ __FileNameMemory, MAX_PATH };
    void* MemoryBlock;
};

struct win32_state
{
    size_t TotalSize = 0;
    void* GameMemoryBlock = NULL;
    win32_replay_buffer ReplayBuffers[4];

    HANDLE RecordingHandle = NULL;
    int InputRecordingIndex = 0;

    HANDLE PlaybackHandle = NULL;
    int InputPlayingIndex = 0;

    bool32 GlobalRunning = false;

    bool32 ShowCursor = true;
    v2 HideCursorPos;

    bool32 Vsync = true;
    
      shader* WatchedFiles[16];
    FILETIME LastWriteTimes[16];
    uint32 WatchedFilesCount;
};

struct game_code
{
    HMODULE LoadedDLL;

    game_update_and_render* UpdateAndRender;

    FILETIME LastWriteTime; //time dll was created
    //bool32 isValid;
};

struct gl_code
{
    HMODULE LoadedDLL;

    opengl_init* OpenGLInit;
    opengl_exit* OpenGLExit;
    opengl_imgui_loadfonts* OpenGLImGuiLoadFonts;
    opengl_render_commands* OpenGLRenderCommands;

    opengl_create_buffer* OpenGLCreateBuffer;
    opengl_fill_buffer* OpenGLFillBuffer;
    
    opengl_allocate_texture* OpenGLAllocateTexture;

    opengl_create_fbo* OpenGLCreateFbo;
    opengl_resize_fbo* OpenGLResizeFbo;
    
    opengl_create_shader* OpenGLCreateShader;

    FILETIME LastWriteTime; //time dll was created
    //bool32 isValid;
};

#define PLATFORM_WIN32_H
#endif