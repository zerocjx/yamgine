#if !defined(YAMGINE_PROFILING_H)

#include "yamgine_memory_arena.h"
global_variable yam_profiling* YamProfiling;

real64 TimeElapsedMillis(int64 A, int64 B)
{
    real64 Result = 0;
    
    //times 1000 to get to milliseconds
    Result = (((real64)(B - A)) / (real64)(YamProfiling->PerformanceFrequency)) * 1000;
    
    return Result;
}

void AddChild(yam_profiling_scope* Parent, yam_profiling_scope* Child)
{
    if (Parent->Child)
    {
        yam_profiling_scope* LastSibling = Parent->Child;
        while (LastSibling->NextSibling != NULL)
        {
            LastSibling = LastSibling->NextSibling;
        }
        LastSibling->NextSibling = Child;
    }
    else
    {
        Parent->Child = Child;
    }

    Child->Parent = Parent;
}

uint32 StringSum(char* Str)
{
    uint32 Result = 0;
    while (*Str != '\0')
    {
        Result += *Str;
        ++Str;
    }
    return Result;
}

internal yam_profiling_scope*
FindScope(char* ScopeName)
{
    yam_profiling_scope* HashResult = NULL;

    bool32 Success = false;

    uint32 ScopeStringSum = StringSum(ScopeName);
    uint32 Sum = ScopeStringSum;
    if (YamProfiling->CurrentScope)
    {
        Sum += ((uint64)YamProfiling->CurrentScope->ID);
    }
    uint32 Index = Sum + (Sum >> 8);
    Index %= HASH_SCOPES_COUNT;
    if (Index == 264)
    {
        //DebugBreak();
    }
    while (!Success)
    {
        HashResult = &YamProfiling->Scopes[Index];

        if (HashResult->ID == 0)
        {
            Success = true;
        }
        else if (HashResult->ID == Sum)
        {
            Assert(StringsAreEqualAbsolute(ScopeName, HashResult->Name));
            Success = true;
        }
        else
        {
            Index = Index + (((Sum << 4) + (Sum >> 4)) | 1);
            Index %= HASH_SCOPES_COUNT;
        }
    }

    HashResult->ID = Sum;

    return HashResult;
}

inline void
PushScopeStack(yam_profiling_scope* Scope)
{
    YamProfiling->ScopeStack[++YamProfiling->ScopeStackDepth] = Scope;

    Scope->Depth = YamProfiling->ScopeStackDepth - 1;

    if (YamProfiling->CurrentScope)
    {
        if (Scope->Parent == NULL)
        {
            AddChild(YamProfiling->CurrentScope, Scope);
        }
    }
    else
    {
        Assert(Scope->Depth == 0);
        YamProfiling->Global = Scope;
    }
    YamProfiling->CurrentScope = Scope;
}

void ProfInternalStart()
{
    int64 CurrentTime = YamProfiling->GetTimestamp();
    YamProfiling->InternalScope.StartTimestamp = CurrentTime;
}

void ProfInternalEnd()
{
    YamProfiling->InternalScope.EndTimestamp = YamProfiling->GetTimestamp();

    real64 TimeElapsed = TimeElapsedMillis(YamProfiling->InternalScope.StartTimestamp, 
                                           YamProfiling->InternalScope.EndTimestamp);
    
    YamProfiling->InternalScope.TotalTime += TimeElapsed;
}

inline void
YamProfBegin_(char* ScopeName)
{
#if YAMGINE_DEBUG
    ProfInternalStart();

    yam_profiling_scope* Scope = FindScope(ScopeName);

    Assert(StringLength(ScopeName) < sizeof(Scope->Name));
    CopyString(ScopeName, Scope->Name, 32);

    bool32 IsSameScope = false;
    if (YamProfiling->CurrentScope == Scope) // recursion detected!
    {
        IsSameScope = true;
    }

    PushScopeStack(Scope);

    Scope->EntryCount++;

    int64 CurrentTime = YamProfiling->GetTimestamp();
    if (IsSameScope)
    {
        Scope->TotalTime +=TimeElapsedMillis(Scope->StartTimestamp, CurrentTime);
        Scope->StartTimestamp = CurrentTime;
    }
    else
    {
        Scope->StartTimestamp = CurrentTime;
    }
    ProfInternalEnd();
#endif
}

//#define YamProfBegin(Name) YamProfBegin_(#Name)

void InitProfiler()
{
    int64 Frequency = YamProfiling->PerformanceFrequency;
    platform_get_timestamp* GetTimestamp = YamProfiling->GetTimestamp;
    ZeroStruct(*YamProfiling);
    YamProfiling->GetTimestamp = GetTimestamp;
    YamProfiling->PerformanceFrequency = Frequency;

    string Str = {};
    Str.Text = YamProfiling->InternalScope.Name;
    Str.Size = 32;
    SetString(&Str, "Profiling Overhead");

    YamProfBegin(__global);
}

void YamProfEnd()
{
#if YAMGINE_DEBUG
    //ProfInternalStart();
    YamProfiling->CurrentScope->EndTimestamp = YamProfiling->GetTimestamp();

    YamProfiling->CurrentScope->TotalTime += TimeElapsedMillis(YamProfiling->CurrentScope->StartTimestamp, 
                                                               YamProfiling->CurrentScope->EndTimestamp);
    
    yam_profiling_scope* OldScope = YamProfiling->ScopeStack[YamProfiling->ScopeStackDepth];
    YamProfiling->CurrentScope = YamProfiling->ScopeStack[--YamProfiling->ScopeStackDepth];

    if (OldScope == YamProfiling->CurrentScope)
    {
        YamProfiling->CurrentScope->StartTimestamp = YamProfiling->CurrentScope->EndTimestamp;
    }

//ProfInternalEnd();
#endif
}

void CopyScope(memory_arena* Arena, yam_profiling_scope* CopyParent, yam_profiling_scope* Scope)
{
    yam_profiling_scope* NewScope = PushStruct(Arena, yam_profiling_scope);
    *NewScope = *Scope;
    NewScope->Parent = NULL;
    NewScope->NextSibling = NULL;
    NewScope->Child = NULL;
    AddChild(CopyParent, NewScope);

    NewScope->TotalTimeSelf = NewScope->TotalTime;
    NewScope->Parent->TotalTimeSelf -= NewScope->TotalTime;

    if (Scope->Child)
    {
        CopyScope(Arena, NewScope, Scope->Child);

        yam_profiling_scope* NextSibling = Scope->Child->NextSibling;
        while (NextSibling != NULL)
        {
            CopyScope(Arena, NewScope, NextSibling);
            NextSibling = NextSibling->NextSibling;
        }
    }
}

void UpdateProfiling()
{
#if YAMGINE_DEBUG
    //ProfInternalStart();
    // clear entry counts for next frame
    for (uint32 Index = 0;
         Index < HASH_SCOPES_COUNT;
         ++Index)
    {
        yam_profiling_scope* Scope = &YamProfiling->Scopes[Index];
        ZeroStruct((*Scope));
    }
    ZeroStruct(YamProfiling->InternalScope);

    string Str = {};
    Str.Text = YamProfiling->InternalScope.Name;
    Str.Size = 32;
    SetString(&Str, "Profiling Overhead");

    Assert(YamProfiling->ScopeStackDepth == 0);
    YamProfBegin(__global);
//ProfInternalEnd();
#endif
}

yam_profiling_report* CreateReport(memory_arena* Arena)
{
    yam_profiling_report* Report = PushStruct(Arena, yam_profiling_report);
#if YAMGINE_DEBUG
    ProfInternalStart();
    Report->Global = *YamProfiling->Global;
    Report->Global.Child = NULL;

    Report->Global.TotalTimeSelf = Report->Global.TotalTime;

    // make copies in arena
    yam_profiling_scope* Child = YamProfiling->Global->Child;
    if (Child)
    {
        while (Child != NULL)
        {
            CopyScope(Arena, &Report->Global, Child);
            Child = Child->NextSibling;
        }
    }
    Report->InternalScope = YamProfiling->InternalScope;
    
    if (YamProfiling->FrameTime->TotalTime > 16.666666f)
    {
        Report->SkippedFrame = true;
    }
    else
    {
        Report->SkippedFrame = false;
    }

    ProfInternalEnd();
#endif

    return Report;
}

/*
struct yam_profiling_auto
{
    inline ~yam_profiling_auto()
    {
        YamProfEnd();
    }
};

#define YamProf(Name) yam_profiling_auto _Prof_##Name;YamProfBegin(Name)
*/
#endif