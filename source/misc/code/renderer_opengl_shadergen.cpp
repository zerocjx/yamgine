#include "yamgine_tokenizer.h"

#define MAX_SHADER_SIZE 6000

void GenerateShader(char* Filename, char* Options, char* Header, char* OutputName, temporary_memory* Temp, platform_api* PlatformAPI)
{
    char* FileContents = (char*)LoadTextFile(Filename, Temp, PlatformAPI);

    tokenizer Tokenizer = {};
    Tokenizer.At = FileContents;
    Tokenizer.EnableMinusToken = true;

    STRING(Vert, MAX_SHADER_SIZE);
    AppendStringEx(&Vert, "// VERT SHADER %s\n\n#version 430\n%s%s", Filename, Options, Header);

    STRING(Frag, MAX_SHADER_SIZE);
    AppendStringEx(&Frag, "// FRAG SHADER %s\n\n#version 430\n%s%s", Filename, Options, Header);

    /*
        char Uniforms[MAX_SHADER_SIZE] = {};
        uint32 UniformCount;
        char* UniformLocations[32];
*/
    bool WritingVert = true;
    bool BeganWritingVertMain = false;
    bool ParsedVertToFragStruct = false;

    string* CurrentShader = &Vert;

    bool Parsing = true;

    int BraceDepth = 0;
    token Token = GetToken(&Tokenizer);
    bool32 NextCharIsSpace = false;
    while (Parsing)
    {
        if (*Tokenizer.At == ' ')
        {
            NextCharIsSpace = true;
        }
        else
        {
            NextCharIsSpace = false;
        }
        token NextToken = GetToken(&Tokenizer);

        switch (Token.Type)
        {
            case Token_EndOfStream:
            {
                Parsing = false;
            }
            break;

            case Token_Unknown:
            {
            }
            break;

            case Token_OpenBracket:
            {
                BraceDepth++;
                AppendString(CurrentShader, "[");
            }
            break;
            
            case Token_CloseBracket:
            {
                BraceDepth--;
                AppendString(CurrentShader, "]");
            }
            break;
            
            case Token_OpenBrace:
            {
                BraceDepth++;
                AppendString(CurrentShader, "{\n");
                Indent(CurrentShader, BraceDepth);
            }
            break;

            case Token_CloseBrace:
            {
                BraceDepth--;
                Indent(CurrentShader, BraceDepth);
                AppendString(CurrentShader, "}\n");

                if (BraceDepth == 0 && BeganWritingVertMain && WritingVert)
                {
                    BeganWritingVertMain = false;
                    WritingVert = false;
                    CurrentShader = &Frag;
                }
            }
            break;

            case Token_Semicolon:
            {
                AppendString(CurrentShader, ";\n");
            }
            break;

            case Token_Equals:
            {
                AppendString(CurrentShader, "=");
                if (NextToken.Type == Token_Identifier)
                {
                    AppendString(CurrentShader, " ");
                }
            }
            break;

            case Token_LessThan:
            {
                AppendString(CurrentShader, "<");
                if (NextToken.Type == Token_Identifier)
                {
                    AppendString(CurrentShader, " ");
                }
            }
            break;

            case Token_GreaterThan:
            {
                AppendString(CurrentShader, ">");
                if (NextToken.Type == Token_Identifier)
                {
                    AppendString(CurrentShader, " ");
                }
            }
            break;

            case Token_Comma:
            {
                AppendString(CurrentShader, ",");
            }
            break;

            case Token_Period:
            {
                AppendString(CurrentShader, ".");
            }
            break;

            case Token_Plus:
            {
				if (NextToken.Type == Token_Equals || NextToken.Type == Token_Plus)
				{
                    AppendString(CurrentShader, "+");
				}
				else
				{
                    AppendString(CurrentShader, "+ ");
				}
            }
            break;

            case Token_Minus:
            {
                if (NextCharIsSpace)
				{
					AppendString(CurrentShader, "- ");
				}
				else
				{
					AppendString(CurrentShader, "-");
				}
            }
            break;

            case Token_Asterisk:
            {
                if (NextToken.Type == Token_Equals || NextToken.Type == Token_Asterisk)
                {
                    AppendString(CurrentShader, "*");
                }
                else
                {
                    AppendString(CurrentShader, "* ");
                }
            }
            break;

            case Token_Divide:
            {
                if (NextToken.Type == Token_Equals || NextToken.Type == Token_Divide)
                {
                    AppendString(CurrentShader, "/");
                }
                else
                {
                    AppendString(CurrentShader, "/ ");
                }
            }
            break;

            case Token_OpenParen:
            {
                AppendString(CurrentShader, "(");
            }
            break;

            case Token_CloseParen:
            {
                AppendString(CurrentShader, ")");
            }
            break;

            case Token_Number:
            {
                AppendSubstring(CurrentShader, Token.Text, (uint32)Token.TextLength);
            }
            break;

            case Token_Pound:
            {
                if (TokenEquals(NextToken, "ifdef"))
                {
                    AppendString(CurrentShader, "#ifdef ");

                    token PoundId = GetToken(&Tokenizer);
                    AppendSubstring(CurrentShader, PoundId.Text, (uint32)PoundId.TextLength);
                    AppendString(CurrentShader, "\n");
                }
                else if (TokenEquals(NextToken, "define"))
                {
                    AppendString(CurrentShader, "#define ");

                    token PoundId = GetToken(&Tokenizer);
                    AppendSubstring(CurrentShader, PoundId.Text, (uint32)PoundId.TextLength);
                    AppendString(CurrentShader, "\n");
                }
                else if (TokenEquals(NextToken, "endif"))
                {
                    AppendString(CurrentShader, "#endif\n");
                }
                else if (TokenEquals(NextToken, "else"))
                {
                    AppendString(CurrentShader, "#else\n");
                }
                else if (TokenEquals(NextToken, "elif"))
                {
                    AppendString(CurrentShader, "#elif ");

                    token PoundId = GetToken(&Tokenizer);
                    AppendSubstring(CurrentShader, PoundId.Text, (uint32)PoundId.TextLength);
                    AppendString(CurrentShader, "\n");
                }
                else if (TokenEquals(NextToken, "if"))
                {
                    AppendString(CurrentShader, "#if");

                    token RestOfLine = GetRestOfLine(&Tokenizer);
                    AppendSubstring(CurrentShader, RestOfLine.Text, (uint32)RestOfLine.TextLength);
                    AppendString(CurrentShader, "\n");
                }
                else
                {
                    InvalidCodePath;
                }
                NextToken = GetToken(&Tokenizer);
            }
            break;

            case Token_Identifier:
            {
                if (TokenEquals(Token, "VERT_TO_FRAG"))
                {
                    if (!ParsedVertToFragStruct) //convert VERT_TO_FRAG struct
                    {
                        Assert(NextToken.Type == Token_OpenBrace); //open brace
                        token Type = GetToken(&Tokenizer);
                        while (Type.Type != Token_CloseBrace)
                        {
                            if (Type.Type == Token_Identifier)
                            {
                                token Identifier = GetToken(&Tokenizer);
                                Assert(Identifier.Type == Token_Identifier);

                                AppendString(&Vert, "out ");
                                AppendSubstring(&Vert, Type.Text, (uint32)Type.TextLength);
                                AppendString(&Vert, " _");
                                AppendSubstring(&Vert, Identifier.Text, (uint32)Identifier.TextLength);
                                AppendString(&Vert, ";\n");

                                AppendString(&Frag, "in ");
                                AppendSubstring(&Frag, Type.Text, (uint32)Type.TextLength);
                                AppendString(&Frag, " _");
                                AppendSubstring(&Frag, Identifier.Text, (uint32)Identifier.TextLength);
                                AppendString(&Frag, ";\n");

                                AssertToken(&Tokenizer, Token_Semicolon);

                                Type = GetToken(&Tokenizer);
                            }
                            else if (Type.Type == Token_Pound)
                            {
                                token PoundType = GetToken(&Tokenizer);

                                if (TokenEquals(PoundType, "ifdef"))
                                {
                                    AppendString(&Vert, "#ifdef ");
                                    AppendString(&Frag, "#ifdef ");

                                    token PoundId = GetToken(&Tokenizer);
                                    AppendSubstring(&Vert, PoundId.Text, (uint32)PoundId.TextLength);
                                    AppendSubstring(&Frag, PoundId.Text, (uint32)PoundId.TextLength);

                                    AppendString(&Vert, "\n");
                                    Indent(&Vert, BraceDepth);
                                    AppendString(&Frag, "\n");
                                    Indent(&Frag, BraceDepth);
                                }
                                else if (TokenEquals(PoundType, "if"))
                                {
                                    AppendString(&Vert, "#if");
                                    AppendString(&Frag, "#if");

                                    token RestOfLine = GetRestOfLine(&Tokenizer);
                                    AppendSubstring(&Vert, RestOfLine.Text, (uint32)RestOfLine.TextLength);
                                    AppendSubstring(&Frag, RestOfLine.Text, (uint32)RestOfLine.TextLength);

                                    AppendString(&Vert, "\n");
                                    Indent(&Vert, BraceDepth);
                                    AppendString(&Frag, "\n");
                                    Indent(&Frag, BraceDepth);
                                }
                                else if (TokenEquals(PoundType, "endif"))
                                {
                                    AppendString(&Vert, "#endif\n");
                                    Indent(&Vert, BraceDepth);
                                    AppendString(&Frag, "#endif\n");
                                    Indent(&Frag, BraceDepth);
                                }
                                else
                                {
                                    InvalidCodePath;
                                }
                                Type = GetToken(&Tokenizer);
                            }
                        }
                        AssertToken(&Tokenizer, Token_Semicolon);
                        ParsedVertToFragStruct = true;
                        NextToken = GetToken(&Tokenizer);
                    }
                    else //convert VERT_TO_FRAG member
                    {
                        if (CurrentShader->Text[StringLength(CurrentShader->Text) - 1] == '\n')
                        {
                            Indent(CurrentShader, BraceDepth);
                        }

                        AppendString(CurrentShader, "_");
                        Assert(NextToken.Type == Token_Period);
                        NextToken = GetToken(&Tokenizer);
                    }
                }
                else if (TokenEquals(Token, "VERT"))
                {
                    AppendString(&Vert, "main");
                    BeganWritingVertMain = true;
                }
                else if (TokenEquals(Token, "FRAG"))
                {
                    AppendString(&Frag, "main");
                }
                else
                {
                    if (CurrentShader->Text[StringLength(CurrentShader->Text) - 1] == '\n')
                    {
                        Indent(CurrentShader, BraceDepth);
                    }

                    AppendSubstring(CurrentShader, Token.Text, (uint32)Token.TextLength);
                    if (NextToken.Type == Token_Identifier)
                        AppendString(CurrentShader, " ");
                }
            }
            break;
        }
        Token = NextToken;
    }
    
    //TODO(james): get rid of this assumed output path?
    STRING(VertPath, 1024);
    AppendStringEx(&VertPath, "%s.vert", OutputName);

    PlatformAPI->WriteBinaryFile(VertPath.Text,
                                 (uint8*)Vert.Text,
                                 StringLength(Vert.Text));

    STRING(FragPath, 1024);
    AppendStringEx(&FragPath, "%s.frag", OutputName);

    PlatformAPI->WriteBinaryFile(FragPath.Text,
                                 (uint8*)Frag.Text,
                                 StringLength(Frag.Text));
}
