@echo off
setlocal EnableExtensions EnableDelayedExpansion

 ..\..\tools\ctime\ctime -begin ..\..\tools\ctime\overall.ctm
 
 
REM ..\..\tools\ctime\ctime -begin ..\..\tools\ctime\game.ctm

REM this batch file should look something like "call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64"
REM here we are checking if cl is a valid command, if not, call vcvarsall through the user's batch file

WHERE cl >nul 2>nul 
IF %ERRORLEVEL% NEQ 0 call "..\misc\vcvarsall-caller-x64.bat"

REM fmod libs followed by an L have logging enabled. We should probably switch to version without when shipping

REM using fbxsdk adds a full second to compilation time
REM switching to explicit linking did not reduce linking time with fmod

set FMOD_SUPPORT=0
set WWISE_SUPPORT=0

IF "%FMOD_SUPPORT%" == "1" (
IF "%WWISE_SUPPORT%" == "1" echo Both FMOD_SUPPORT and WWISE_SUPPORT are enabled!
)

set YAMGINE_DEBUG=1

REM -analyze - Static Analysis
REM WX - Warnings as Errors
REM wd4201 - nonstandard extension used: nameless struct/union
REM wd4100 - unreferenced formal parameter
REM wd4189 - local variable is initialized but not referenced
REM wd4505 - compiler could not resolve type
REM wd4127 - controlling expression of a statement evaulates to a constant
REM wd4244 - conversion from type to type has possible loss of data
REM wd4800 - forcing value to a bool, need this for converting bool32 to bool to pass to ImGui
REM wd4324 - warns struct was padded due to declspec(align()) which is neccsary for shader/c++ uniform block alignment
set WarningFlags=  -W4 -wd4201 -wd4100 -wd4189 -wd4505 -wd4127 -wd4244 -wd4800 -wd4324

set DebugFlags = -0d -MTd -D_ITERATOR_DEBUG_LEVEL=2
set ReleaseFlags = -02 -MT
set CommonCompilerFlags=%DebugFlags% -WX  -nologo -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -FC -Z7 -EHsc -cgthreads4
set CommonCompilerFlags=-DYAMGINE_WIN32=1 -DYAMGINE_DEBUG=%YAMGINE_DEBUG% -DYAMGINE_CLIENT=0 -DYAMGINE_SERVER=0 -DFMOD_SUPPORT=%FMOD_SUPPORT% -DWWISE_SUPPORT=%WWISE_SUPPORT% -DYAMGINE_EDITOR=1 %WarningFlags% %CommonCompilerFlags%
set CommonCompilerFlags= /I"..\source\imgui" %CommonCompilerFlags%

IF %FMOD_SUPPORT%==1 set AUDIO_LIB_INCLUDE=/I"..\source\FMOD Studio API Windows\api\lowlevel\inc" /I"..\source\FMOD Studio API Windows\api\studio\inc"

IF %WWISE_SUPPORT%==1 set AUDIO_LIB_INCLUDE=/I"..\source\Audiokinetic\Wwise 2016.2.2.6022\SDK\include" /I"..\source\Audiokinetic\Wwise 2016.2.2.6022\SDK\samples\SoundEngine\Win32" /I"..\source\Audiokinetic\Wwise 2016.2.2.6022\SDK\samples\SoundEngine\Common"


set CommonCompilerFlags=%AUDIO_LIB_INCLUDE% %CommonCompilerFlags%

set CommonLinkerFlags= -incremental:no -opt:ref  user32.lib gdi32.lib winmm.lib Ws2_32.lib opengl32.lib Ole32.lib

IF NOT EXIST ..\..\build mkdir ..\..\build
pushd ..\..\build

REM 32-bit build
REM cl %CommonCompilerFlags% ..\source\code\platform_win32.cpp /link -subsystem:windows,5.1  %CommonLinkerFlags%

REM %1 is first parameter to the batch file
REM maybe something separate for the preprocessor?
IF "%1" == "" (
cl -O2 -MT -nologo -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -FC -Z7 -EHsc -cgthreads4 -D_CRT_SECURE_NO_WARNINGS -Fepreprocessor ..\source\code\simple_preprocessor.cpp /link %CommonLinkerFlags%
)
REM Simple preprocessor

pushd ..\source\code
..\..\build\preprocessor.exe > yamgine_generated.h
popd


set BuildRenderer=false
IF "%1" == "renderer" set BuildRenderer=true
IF "%1" == "" set BuildRenderer=true

IF %BuildRenderer%==true (

del renderer_opengl_*.pdb > NUL 2> NUL
del renderer_opengl_*.dll > NUL 2> NUL

echo WAITING FOR GL PDB > gl_lock.tmp

cl %CommonCompilerFlags% ..\source\code\renderer_opengl.cpp -Fmrenderer_opengl.map -LD /link -PDB:renderer_opengl_%random%.pdb -EXPORT:OpenGLInit -EXPORT:OpenGLExit -EXPORT:OpenGLImGuiLoadFonts -EXPORT:OpenGLRenderCommands -EXPORT:OpenGLCreateBuffer -EXPORT:OpenGLFillBuffer -EXPORT:OpenGLAllocateTexture -EXPORT:OpenGLCreateFbo -EXPORT:OpenGLResizeFbo -EXPORT:OpenGLCreateShader  %CommonLinkerFlags%
del gl_lock.tmp
)

set BuildGame=false
IF "%1" == "game" set BuildGame=true
IF "%1" == "" set BuildGame=true

IF %BuildGame%==true (

del yamgine_*.pdb > NUL 2> NUL
del yamgine_*.dll > NUL 2> NUL


echo WAITING FOR PDB > lock.tmp
cl %CommonCompilerFlags% -Feyamgine ..\source\code\game.cpp -Fmyamgine.map -LD /link -PDB:yamgine_%random%.pdb -EXPORT:GameUpdateAndRender %CommonLinkerFlags%
del lock.tmp
)

set FMOD_LIBS=fmodstudio64_vc.lib fmod64_vc.lib
IF "%YAMGINE_DEBUG%"=="1" set FMOD_LIBS=/LIBPATH:"..\source\FMOD Studio API Windows\api\studio\lib" /LIBPATH:"..\source\FMOD Studio API Windows\api\lowlevel\lib" fmodstudioL64_vc.lib fmodL64_vc.lib

IF "%YAMGINE_DEBUG%"=="1" set WWISE_LIBS=/NODEFAULTLIB:LIBCMTD /LIBPATH:"..\source\Audiokinetic\Wwise 2016.2.2.6022\SDK\x64_vc140\Debug(StaticCRT)\lib" AkMemoryMgr.lib AkRoomVerbFX.lib AkStreamMgr.lib AkSoundEngine.lib CommunicationCentral.lib dxguid.lib AkMusicEngine.lib AkVorbisDecoder.lib

IF "%FMOD_SUPPORT%"=="1" set AUDIO_LIBS=%FMOD_LIBS%
IF "%WWISE_SUPPORT%"=="1" set AUDIO_LIBS=%WWISE_LIBS%

set BuildPlatform=false
IF "%1" == "platform" set BuildPlatform=true
IF "%1" == "" set BuildPlatform=true
IF %BuildPlatform%==true (
cl %CommonCompilerFlags% -Feyamgine ..\source\code\platform_win32.cpp  -Fmyamgine.map ..\source\code\resources.res /link %CommonLinkerFlags% %AUDIO_LIBS% 
)

REM Compile Time Checker
IF "%1" == "tests" (
cl %CommonCompilerFlags% ..\source\code\tests\compile_time_tests.cpp /link %CommonLinkerFlags% 
pushd ..\source\code
..\..\build\compile_time_tests.exe
popd
)

popd

set LastError=%ERRORLEVEL%

REM ..\..\tools\ctime\ctime -end ..\..\tools\ctime\platform.ctm %LastError%
..\..\tools\ctime\ctime -end ..\..\tools\ctime\overall.ctm %LastError%

REM ECHO.
REM ECHO CPP CHECK STATIC ANALYSIS
REM ECHO ------------------------------
REM call cppcheck

