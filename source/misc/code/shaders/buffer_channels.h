
typedef enum buffer_channel
{
    BufferChannel_Position=0,
    BufferChannel_Normal=1,
    BufferChannel_Color=2,
    BufferChannel_Element=3,
    BufferChannel_UV=4,
    
    BufferType_ParticleCenter=10,
}buffer_channel;
