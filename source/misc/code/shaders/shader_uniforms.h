#ifdef GLSL
#define matrix4 mat4 
#define v2 vec2
#define v3 vec3 
#define v4 vec4 

#define ALIGN(a)
#endif


#ifdef CPP
#define layout(a,b) 
#define uniform struct
#endif

#define YAM_TEXTURE0_LOCATION 16
#define YAM_TEXTURE1_LOCATION 17

//#define YAM_SHADOW_TEXTURE0_LOCATION 20

#define CHANNEL_POSITION 0
#define CHANNEL_POSITION_WIDTH 3

#define CHANNEL_NORMAL 1
#define CHANNEL_NORMAL_WIDTH 3

#define CHANNEL_COLOR 2
#define CHANNEL_COLOR_WIDTH 4

#define CHANNEL_ELEMENT 3
#define CHANNEL_ELEMENT_WIDTH 1

#define CHANNEL_UV 4
#define CHANNEL_UV_WIDTH 2

#define CHANNEL_PARTICLE_CENTER 10
#define CHANNEL_PARTICLE_CENTER_WIDTH 3

#define CHANNEL_PARTICLE_COLOR 11
#define CHANNEL_PARTICLE_COLOR_WIDTH 4



layout(std140, binding=0) uniform global_uniforms
{
    ALIGN(16) matrix4 View;
    ALIGN(16) matrix4 Projection;
};

#ifdef CPP
#define BASIC_LINE_UNIFORMS
#endif

#ifdef BASIC_LINE_UNIFORMS
layout(std140, binding=1) uniform line_uniforms
{
    ALIGN(16) matrix4 Model;
};
#endif


#ifdef CPP
#define BASIC_LINE_UNIFORM_COLOR_UNIFORMS
#endif

#ifdef BASIC_LINE_UNIFORM_COLOR_UNIFORMS
layout(std140, binding=1) uniform line_uniform_color_uniforms
{
    ALIGN(16) matrix4 Model;
    ALIGN(16) v4 Color;
};
#endif


#ifdef CPP
#define TEXTURED_RECT_UNIFORMS
#endif

#ifdef TEXTURED_RECT_UNIFORMS
layout(std140, binding=1) uniform textured_rect_uniforms
{
    ALIGN(16) matrix4 Model;
    ALIGN(16) v4 Color;
    ALIGN(16) v4 UVRect;
};
#endif


#ifdef CPP
#define MULTISAMPLE_TEXTURE_RECT_UNIFORMS
#endif

#ifdef MULTISAMPLE_TEXTURE_RECT_UNIFORMS
layout(std140, binding=1) uniform multisample_texture_rect_uniforms
{
    ALIGN(16) matrix4 Model;
    ALIGN(16) v2 Dimensions;
};
#endif


#ifdef CPP
#define FILL_DEPTHMAP_UNIFORMS
#endif

#ifdef FILL_DEPTHMAP_UNIFORMS
layout(std140, binding=1) uniform fill_depthmap_uniforms
{
    ALIGN(16) matrix4 Model;
};
#endif


#ifdef CPP
#define MESH_RECIEVE_SHADOWS_UNIFORMS
#endif

#ifdef MESH_RECIEVE_SHADOWS_UNIFORMS
layout(std140, binding=1) uniform mesh_recieve_shadows_uniforms
{
    ALIGN(16) matrix4 Model;
    
    ALIGN(16) matrix4 LightSpaceMatrix;
    
    ALIGN(16) v4 ObjectColor;
    ALIGN(16) v3 LightDir;
    
    ALIGN(16) v3 LightColor;
};
#endif


#ifdef CPP
#define SOBEL_UNIFORMS
#endif

#ifdef SOBEL_UNIFORMS
layout(std140, binding=1) uniform sobel_uniforms
{
    ALIGN(16) matrix4 Model;
     float imageHeightFactor;
     float imageWidthFactor;
};
#endif


#ifdef CPP
#define GAUSSIAN_BLUR_UNIFORMS
#endif

#ifdef GAUSSIAN_BLUR_UNIFORMS
layout(std140, binding=1) uniform gaussian_blur_uniforms
{
    ALIGN(16) matrix4 Model;
    ALIGN(16) v2 dir;
    float resolution;
     float radius;
};
#endif


//END OF FILE
