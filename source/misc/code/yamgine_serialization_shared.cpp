#include "yamgine_generated.h"

#define STB_SPRINTF_IMPLEMENTATION
#include "stb_printf.h"

#include "yamgine_tokenizer.h"

uint32 LoadUint32(token Token)
{
    int64 LoadedValueLong;
    uint32 LoadedValueUnsigned;

    LoadedValueLong = atoll(Token.Text);
    LoadedValueUnsigned = *((uint32*)(&LoadedValueLong));

    return LoadedValueUnsigned;
}

v2 ParseV2(tokenizer* Tokenizer)
{
    v2 HexResult = {};
    AssertToken(Tokenizer, Token_OpenBrace);

    uint32 ValueMemoryDump;

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.x) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_Comma);

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.y) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_CloseBrace);

    AssertToken(Tokenizer, Token_OpenBrace);

    v2 TextResult = {};

    TextResult.x = atof(GetToken(Tokenizer).Text); //TODO(james):from what lib are we getting atof?
    AssertToken(Tokenizer, Token_Comma);

    TextResult.y = atof(GetToken(Tokenizer).Text);
    AssertToken(Tokenizer, Token_CloseBrace);

    v2 Result = HexResult;
    if (AbsoluteValue(HexResult.x - TextResult.x) > 0.1f)
        Result.x = TextResult.x;
    if (AbsoluteValue(HexResult.y - TextResult.y) > 0.1f)
        Result.y = TextResult.y;

    return Result;
}

v3 ParseV3(tokenizer* Tokenizer)
{
    v3 HexResult = {};
    AssertToken(Tokenizer, Token_OpenBrace);

    uint32 ValueMemoryDump;

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.x) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_Comma);

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.y) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_Comma);

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.z) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_CloseBrace);

    AssertToken(Tokenizer, Token_OpenBrace);

    v3 TextResult = {};

    TextResult.x = atof(GetToken(Tokenizer).Text); //TODO(james):where are we getting atof?
    AssertToken(Tokenizer, Token_Comma);

    TextResult.y = atof(GetToken(Tokenizer).Text);
    AssertToken(Tokenizer, Token_Comma);

    TextResult.z = atof(GetToken(Tokenizer).Text);
    AssertToken(Tokenizer, Token_CloseBrace);

    v3 Result = HexResult;
    if (AbsoluteValue(HexResult.x - TextResult.x) > 0.1f)
        Result.x = TextResult.x;
    if (AbsoluteValue(HexResult.y - TextResult.y) > 0.1f)
        Result.y = TextResult.y;
    if (AbsoluteValue(HexResult.z - TextResult.z) > 0.1f)
        Result.z = TextResult.z;

    return Result;
}

v4 ParseV4(tokenizer* Tokenizer)
{
    v4 HexResult = {};
    AssertToken(Tokenizer, Token_OpenBrace);

    uint32 ValueMemoryDump;

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.x) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_Comma);

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.y) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_Comma);

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.z) |= ValueMemoryDump;

    GetToken(Tokenizer); // comma

    ValueMemoryDump = LoadUint32(GetToken(Tokenizer));
    *((uint32*)&HexResult.w) |= ValueMemoryDump;

    AssertToken(Tokenizer, Token_CloseBrace);

    AssertToken(Tokenizer, Token_OpenBrace);

    v4 TextResult = {};

    TextResult.x = atof(GetToken(Tokenizer).Text); //TODO(james):where are we getting atof?
    AssertToken(Tokenizer, Token_Comma);

    TextResult.y = atof(GetToken(Tokenizer).Text);
    AssertToken(Tokenizer, Token_Comma);

    TextResult.z = atof(GetToken(Tokenizer).Text);
    AssertToken(Tokenizer, Token_Comma);

    TextResult.w = atof(GetToken(Tokenizer).Text);
    AssertToken(Tokenizer, Token_CloseBrace);

    v4 Result = HexResult;
    if (AbsoluteValue(HexResult.x - TextResult.x) > 0.1f)
        Result.x = TextResult.x;
    if (AbsoluteValue(HexResult.y - TextResult.y) > 0.1f)
        Result.y = TextResult.y;
    if (AbsoluteValue(HexResult.z - TextResult.z) > 0.1f)
        Result.z = TextResult.z;
    if (AbsoluteValue(HexResult.w - TextResult.w) > 0.1f)
        Result.w = TextResult.w;

    return Result;
}

//
// Serialization
//
char* IndentBuffer(char* TextBuffer, int IndentLevel)
{
    for (int i = 0; i < IndentLevel; i++)
    {
        *TextBuffer++ = ' ';
        *TextBuffer++ = ' ';
        *TextBuffer++ = ' ';
        *TextBuffer++ = ' ';
    }
    return TextBuffer;
}

void SerializeStruct(int32 MemberDefinitionCount, member_definition* MemberDefs,
                     void* Struct, int IndentLevel,
                     char* StructType, char* StructName, string* File)
{
    //
    // Preamble
    //

    AppendStringEx(File, "%s %s = {\n", StructType, StructName);

    //
    // Body
    //
    IndentLevel++;

    for (int32 MemberIndex = 0;
         MemberIndex < MemberDefinitionCount;
         ++MemberIndex)
    {
        Indent(File, IndentLevel);

        size_t TextBufferLeft = File->Size - StringLength(File->Text);

        member_definition* Member = MemberDefs + MemberIndex;

        void* MemberPtr = (((uint8*)Struct) + Member->Offset);

        if (Member->Flags & MetaMemberFlag_IsPointer)
        {
            MemberPtr = *(void**)MemberPtr;
        }

        bool32 Hit = false;
        if (MemberPtr)
        {
            if (MemberIndex == 0)
            {
                if (Member->BaseType != MetaType_no_base)
                {
                    switch (Member->BaseType)
                    {
                        META_SERIALIZE_STRUCT(Struct, IndentLevel, Member->BaseTypeName, "BaseType", File);
                    }
                    //SerializeStruct(ArrayCount(MembersOf_entity), MembersOf_entity, (void*)Entity, 0, "entity", Entity->Name, File);
                    //SerializeStruct(MemberPtr, IndentLevel, Member->BaseTypeName, Member->Name, File);
                }
            }

            TextBufferLeft = File->Size - StringLength(File->Text);
            switch (Member->Type)
            {
                case MetaType_uint32:
                {
                    _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%.*s %s = %u;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, *(uint32*)MemberPtr);
                    Hit = true;
                }
                break;

                case MetaType_bool32:
                {
                    _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%*s %s = %u;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, *(bool32*)MemberPtr);
                    Hit = true;
                }
                break;

                case MetaType_int32:
                {
                    _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%*s %s = %d;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, *(int32*)MemberPtr);
                    Hit = true;
                }
                break;

                case MetaType_real32:
                {
                    _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%*s %s = %f;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, *(real32*)MemberPtr);
                    Hit = true;
                }
                break;

                case MetaType_v2:
                {
                    stbsp_snprintf(File->Text + StringLength(File->Text), (uint32)TextBufferLeft, "%*s %s = {%u,%u} {%f,%f};\n",
                                   StringLength(Member->TypeName), Member->TypeName,
                                   Member->Name,
                                   ((uint32*)MemberPtr)[0],
                                   ((uint32*)MemberPtr)[1],
                                   ((real32*)MemberPtr)[0],
                                   ((real32*)MemberPtr)[1]);
                    Hit = true;
                }
                break;

                case MetaType_v3:
                {
                    stbsp_snprintf(File->Text + StringLength(File->Text), (uint32)TextBufferLeft, "%*s %s = {%u,%u,%u} {%f,%f,%f};\n",
                                   StringLength(Member->TypeName), Member->TypeName,
                                   Member->Name,
                                   ((uint32*)MemberPtr)[0],
                                   ((uint32*)MemberPtr)[1],
                                   ((uint32*)MemberPtr)[2],
                                   ((real32*)MemberPtr)[0],
                                   ((real32*)MemberPtr)[1],
                                   ((real32*)MemberPtr)[2]);
                    Hit = true;
                }
                break;

                case MetaType_v4:
                {
                    stbsp_snprintf(File->Text + StringLength(File->Text), (uint32)TextBufferLeft, "%*s %s = {%u,%u,%u,%u} {%f,%f,%f,%f};\n",
                                   StringLength(Member->TypeName), Member->TypeName,
                                   Member->Name,
                                   ((uint32*)MemberPtr)[0],
                                   ((uint32*)MemberPtr)[1],
                                   ((uint32*)MemberPtr)[2],
                                   ((uint32*)MemberPtr)[3],
                                   ((real32*)MemberPtr)[0],
                                   ((real32*)MemberPtr)[1],
                                   ((real32*)MemberPtr)[2],
                                   ((real32*)MemberPtr)[3]);
                    Hit = true;
                }
                break;

                //TODO(james): there should be a different between serializing a char and a cstring
                case MetaType_char:
                {
                    if ((*(char*)MemberPtr) == '\\' || (*(char*)MemberPtr) == '"')
                    {
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "char %s = \"\\%s\";\n",
                                    Member->Name,
                                    (char*)MemberPtr);
                    }
                    else
                    {
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "char %s = \"%s\";\n",
                                    Member->Name,
                                    (char*)MemberPtr);
                    }
                }
                break;

                    META_SERIALIZE_STRUCT(MemberPtr, IndentLevel, Member->TypeName, Member->Name, File);

                    InvalidDefaultCase;
            }
        }
    }

    //
    // Postamble
    //
    IndentLevel--;

    if (IndentLevel > 0)
    {
        Indent(File, IndentLevel);
    }

    AppendString(File, "};\n");
}

void DeltaSerializeStruct(int32 MemberDefinitionCount, member_definition* MemberDefs,
                          void* Struct, void* DefaultStruct, int IndentLevel,
                          char* StructType, char* StructName, string* File)
{
    //
    // Preamble
    //

    Indent(File, IndentLevel);
    AppendStringEx(File, "%s %s = {\n", StructType, StructName);

    //
    // Body
    //
    IndentLevel++;

    for (int32 MemberIndex = 0;
         MemberIndex < MemberDefinitionCount;
         ++MemberIndex)
    {
        size_t TextBufferLeft = File->Size - StringLength(File->Text);

        member_definition* Member = MemberDefs + MemberIndex;

        void* MemberPtr = (((uint8*)Struct) + Member->Offset);
        void* DefaultMemberPtr = (((uint8*)DefaultStruct) + Member->Offset);

        if (Member->Flags & MetaMemberFlag_IsPointer)
        {
            MemberPtr = *(void**)MemberPtr;
            DefaultMemberPtr = *(void**)DefaultMemberPtr;
        }

        if (MemberPtr)
        {
            if (MemberIndex == 0)
            {
                if (Member->BaseType != MetaType_no_base)
                {
                    switch (Member->BaseType)
                    {
                        META_DELTA_SERIALIZE_STRUCT(Struct, DefaultStruct, IndentLevel, Member->BaseTypeName, "BaseType", File);
                    }
                    //SerializeStruct(ArrayCount(MembersOf_entity), MembersOf_entity, (void*)Entity, 0, "entity", Entity->Name, File);
                    //SerializeStruct(MemberPtr, IndentLevel, Member->BaseTypeName, Member->Name, File);
                }
            }

            TextBufferLeft = File->Size - StringLength(File->Text);
            Assert(TextBufferLeft > 100);

            switch (Member->Type)
            {
                case MetaType_uint32:
                {
                    uint32 StructMemberValue = *(uint32*)MemberPtr;
                    uint32 DefaultStructMemberValue = *(uint32*)DefaultMemberPtr;
                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%.*s %s = %u;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, StructMemberValue);
                    }
                }
                break;

                case MetaType_bool32:
                {
                    bool32 StructMemberValue = *(bool32*)MemberPtr;
                    bool32 DefaultStructMemberValue = *(bool32*)DefaultMemberPtr;

                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%*s %s = %u;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, StructMemberValue);
                    }
                }
                break;

                case MetaType_int32:
                {
                    int32 StructMemberValue = *(int32*)MemberPtr;
                    int32 DefaultStructMemberValue = *(int32*)DefaultMemberPtr;

                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%*s %s = %d;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, StructMemberValue);
                    }
                }
                break;

                case MetaType_real32:
                {
                    real32 StructMemberValue = *(real32*)MemberPtr;
                    real32 DefaultStructMemberValue = *(real32*)DefaultMemberPtr;

                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "%*s %s = %f;\n", StringLength(Member->TypeName), Member->TypeName, Member->Name, StructMemberValue);
                    }
                }
                break;

                case MetaType_v2:
                {
                    v2 StructMemberValue = *(v2*)MemberPtr;
                    v2 DefaultStructMemberValue = *(v2*)DefaultMemberPtr;

                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        stbsp_snprintf(File->Text + StringLength(File->Text), (uint32)TextBufferLeft, "%*s %s = {%u,%u} {%f,%f};\n",
                                       StringLength(Member->TypeName), Member->TypeName,
                                       Member->Name,
                                       ((uint32*)MemberPtr)[0],
                                       ((uint32*)MemberPtr)[1],
                                       ((real32*)MemberPtr)[0],
                                       ((real32*)MemberPtr)[1]);
                    }
                }
                break;

                case MetaType_v3:
                {
                    v3 StructMemberValue = *(v3*)MemberPtr;
                    v3 DefaultStructMemberValue = *(v3*)DefaultMemberPtr;

                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        stbsp_snprintf(File->Text + StringLength(File->Text), (uint32)TextBufferLeft, "%*s %s = {%u,%u,%u} {%f,%f,%f};\n",
                                       StringLength(Member->TypeName), Member->TypeName,
                                       Member->Name,
                                       ((uint32*)MemberPtr)[0],
                                       ((uint32*)MemberPtr)[1],
                                       ((uint32*)MemberPtr)[2],
                                       ((real32*)MemberPtr)[0],
                                       ((real32*)MemberPtr)[1],
                                       ((real32*)MemberPtr)[2]);
                    }
                }
                break;

                case MetaType_v4:
                {
                    v4 StructMemberValue = *(v4*)MemberPtr;
                    v4 DefaultStructMemberValue = *(v4*)DefaultMemberPtr;

                    if (StructMemberValue != DefaultStructMemberValue)
                    {
                        Indent(File, IndentLevel);
                        TextBufferLeft -= IndentLevel * 4;
                        stbsp_snprintf(File->Text + StringLength(File->Text), (uint32)TextBufferLeft, "%*s %s = {%u,%u,%u,%u} {%f,%f,%f,%f};\n",
                                       StringLength(Member->TypeName), Member->TypeName,
                                       Member->Name,
                                       ((uint32*)MemberPtr)[0],
                                       ((uint32*)MemberPtr)[1],
                                       ((uint32*)MemberPtr)[2],
                                       ((uint32*)MemberPtr)[3],
                                       ((real32*)MemberPtr)[0],
                                       ((real32*)MemberPtr)[1],
                                       ((real32*)MemberPtr)[2],
                                       ((real32*)MemberPtr)[3]);
                    }
                }
                break;

                //TODO(james): there should be a difference between serializing a char and a cstring
                // but will we ever actuallly serialize a char?
                case MetaType_char:
                {
                    Indent(File, IndentLevel);
                    TextBufferLeft -= IndentLevel * 4;
                    if ((*(char*)MemberPtr) == '\\' || (*(char*)MemberPtr) == '"')
                    {
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "char %s = \"\\%s\";\n",
                                    Member->Name,
                                    (char*)MemberPtr);
                    }
                    else
                    {
                        _snprintf_s(File->Text + StringLength(File->Text), TextBufferLeft, TextBufferLeft, "char %s = \"%s\";\n",
                                    Member->Name,
                                    (char*)MemberPtr);
                    }
                }
                break;

                    META_DELTA_SERIALIZE_STRUCT(MemberPtr, DefaultMemberPtr, IndentLevel, Member->TypeName, Member->Name, File);

                    InvalidDefaultCase;
            }
        }
    }

    //
    // Postamble
    //
    IndentLevel--;

    if (IndentLevel > 0)
    {
        Indent(File, IndentLevel);
    }

    AppendString(File, "};\n");
}

bool32 IsSameAsMemberDefinition(token MemberType, token MemberName, member_definition* Definition)
{
    bool Result = false;

    bool32 IsSameType = StringsAreEqualAbsolute(StringLength(Definition->TypeName), MemberType.Text, Definition->TypeName);

    bool32 IsSameTypeIdentifier = StringsAreEqualAbsolute(StringLength(Definition->Name), MemberName.Text, Definition->Name);

    if ((IsSameType) && IsSameTypeIdentifier)
    {
        Result = true;
    }
    return Result;
}

void DeserializeStruct(tokenizer* Tokenizer,
                       void* StructPtr,
                       member_definition* MemberDefs, int32 MemberDefinitionCount)
{
    AssertToken(Tokenizer, Token_OpenBrace); // open bracket

    token MemberType = GetToken(Tokenizer);
    
    //NOTE(james): currently if we delta serialize a struct which has child structs,
    //and the child structs have no changes from default, its written as 
    // type Name = { };
    if (MemberType.Type == Token_CloseBrace)
    {
        //AssertToken(Tokenizer, Token_Semicolon);
        return;
    }
    
    
    Assert(MemberType.Type == Token_Identifier);
    token MemberName = GetToken(Tokenizer);
    Assert(MemberName.Type == Token_Identifier);
    bool32 ProgressToNextMember = false;

    bool32 HitEndOfStructEarly = false;

    while (!HitEndOfStructEarly)
    {
        for (int32 MemberDefinitionIndex = 0;
             MemberDefinitionIndex < MemberDefinitionCount;
             ++MemberDefinitionIndex)
        {
            member_definition* MemberDefinition = &MemberDefs[MemberDefinitionIndex];

            void* MemberPointer = (void*)(((uintptr)StructPtr) + MemberDefinition->Offset);

            if (ProgressToNextMember)
            {
                MemberType = GetToken(Tokenizer);
                if (MemberType.Type == Token_CloseBrace) // hit end of struct
                {
                    HitEndOfStructEarly = true;
                    break;
                }
                MemberName = GetToken(Tokenizer);
            }

            if (MemberDefinitionIndex == 0 && MemberDefinition->BaseType != MetaType_no_base && StringsAreEqualAbsolute(StringLength("BaseType"), MemberName.Text, "BaseType"))
            {
                AssertToken(Tokenizer, Token_Equals); //equals
                DeserializeStruct(Tokenizer, StructPtr, MembersOf_transform, ArrayCount(MembersOf_transform));
                AssertToken(Tokenizer, Token_Semicolon);

                MemberType = GetToken(Tokenizer);
                MemberName = GetToken(Tokenizer);
            }

            // NOTE(james): not all members of a struct are serialized, so if we are on a
            // member definition that wasn't serialized, skip to the next one
            if (IsSameAsMemberDefinition(MemberType, MemberName, MemberDefinition))
            {
                ProgressToNextMember = true;
                GetToken(Tokenizer); //equals
                if (StringsAreEqualUnion(MemberType.Text, "v2"))
                {
                    *((v2*)MemberPointer) = ParseV2(Tokenizer);
                }
                else if (StringsAreEqualUnion(MemberType.Text, "v3"))
                {
                    *((v3*)MemberPointer) = ParseV3(Tokenizer);
                }
                else if (StringsAreEqualUnion(MemberType.Text, "v4"))
                {
                    *((v4*)MemberPointer) = ParseV4(Tokenizer);
                }
                else if (MemberDefinition->Type == MetaType_transform)
                {
                    DeserializeStruct(Tokenizer, MemberPointer, MembersOf_transform, ArrayCount(MembersOf_transform));
                }
                else if (MemberDefinition->Type == MetaType_aabb)
                {
                    DeserializeStruct(Tokenizer, MemberPointer, MembersOf_aabb, ArrayCount(MembersOf_aabb));
                }
                else if (MemberDefinition->Type == MetaType_material)
                {
                    DeserializeStruct(Tokenizer, MemberPointer, MembersOf_material, ArrayCount(MembersOf_material));
                }
                else if (MemberDefinition->Type == MetaType_real32)
                {
                    token ValueToken = GetToken(Tokenizer);
                    *((real32*)MemberPointer) = atof(ValueToken.Text);
                }
                else if (MemberDefinition->Type == MetaType_bool32)
                {
                    token ValueToken = GetToken(Tokenizer);
                    *((bool32*)MemberPointer) = (bool32)atoi(ValueToken.Text);
                }
                else if (MemberDefinition->Type == MetaType_int32)
                {
                    token ValueToken = GetToken(Tokenizer);
                    *((int32*)MemberPointer) = (int32)atoi(ValueToken.Text);
                }
                else if (MemberDefinition->Type == MetaType_uint32)
                {
                    token ValueToken = GetToken(Tokenizer);
                    *((uint32*)MemberPointer) = (uint32)atoi(ValueToken.Text);
                }
                else if (MemberDefinition->Type == MetaType_char)
                {
                    token ValueToken = GetToken(Tokenizer);

                    CopyString(ValueToken.Text, ((char*)MemberPointer), (uint32)ValueToken.TextLength);
                }
                else if (MemberDefinition->Type == MetaType_string)
                {
                    DeserializeStruct(Tokenizer, MemberPointer, MembersOf_string, ArrayCount(MembersOf_string));
                }
                else if (MemberDefinition->Type == MetaType_rectangle2)
                {
                    DeserializeStruct(Tokenizer, MemberPointer, MembersOf_rectangle2, ArrayCount(MembersOf_rectangle2));
                }
                else
                {
                    InvalidCodePath;
                }
                AssertToken(Tokenizer, Token_Semicolon);
            }
            else if (MemberDefinitionIndex < MemberDefinitionCount - 1)
            {
                ProgressToNextMember = false;
            }
            else
            {
                // couldn't find a match for file's type. It was probably removed from the
                // struct definition.
                token Token;
                uint32 BracesOpen = 0;
                do
                {
                    Token = GetToken(Tokenizer);
                    if (Token.Type == Token_OpenBrace)
                    {
                        BracesOpen++;
                    }
                    if (Token.Type == Token_CloseBrace)
                    {
                        BracesOpen--;
                    }
                } while (Token.Type != Token_Semicolon || BracesOpen != 0);

                Assert(BracesOpen == 0);
                ProgressToNextMember = true;
            }
        }
    }

    if (!HitEndOfStructEarly)
        AssertToken(Tokenizer, Token_CloseBrace);
}
