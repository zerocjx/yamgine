#if !defined(PLATFORM_AGNOSTIC_H)

// TODO(casey): Have the meta-parser ignore its own #define
#define introspect(IGNORED)
#define serialize(IGNORED)

//NOTE(james): I think we expose this as a C API
//so we can access it on other platforms?
#ifdef __cplusplus
#include "imgui.h"

//extern "C" {
#endif

#if !defined(COMPILER_MSVC)
#define COMPILER_MSVC 0
#endif

#if !defined(COMPILER_LLVM)
#define COMPILER_LLVM 0
#endif

//#if !COMPILER_MSVC && !COMPILER_LLVM
#if _MSC_VER
#undef COMPILER_MSVC
#define COMPILER_MSVC 1
#else
#endif

#include <stdint.h>

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef int32 bool32;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef size_t memory_index;

typedef float real32;
typedef double real64;

typedef intptr_t intptr;
typedef uintptr_t uintptr;

#define internal static
#define local_persist static
#define global_variable static

// NOTE(james): LL for long long, or 64 bits
#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value) * 1024LL)
#define Gigabytes(Value) (Megabytes(Value) * 1024LL)
#define Terabytes(Value) (Gigabytes(Value) * 1024LL)

//#define Epsilon FLT_EPSILON //TODO(james): is this what we really want?
#define HalfEpsilon (Epsilon / 2)
#define MAX_UINT32 0xFFFFFFFF

#ifndef DEBUG_BREAK
#if defined(_MSC_VER)
#if _MSC_VER < 1300
#define DEBUG_BREAK() __asm int 3 /* Trap to debugger! */
#else
#define DEBUG_BREAK() __debugbreak()
#endif
#else
#define DEBUG_BREAK() __builtin_trap()
#endif
#endif

#if YAMGINE_DEBUG
#define Assert(Expression) \
    if (!(Expression))     \
DEBUG_BREAK()
#else
#define Assert(Expression) \
    if (!(Expression))     \
    int x = *((int*)0) //force a crash to ErrorCatcher
#endif

#if YAMGINE_DEBUG
#define DebugTrap(Expression) \
    if ((Expression))         \
    DEBUG_BREAK()
#else
#define DebugTrap(Expression)
#endif

#define InvalidCodePath Assert(!"InvalidCodePath")
#define InvalidDefaultCase \
    default:               \
    {                      \
        InvalidCodePath;   \
    }                      \
    break

#ifdef COMPILER_MSVC
#define MAX_PATH 260
#else
//TODO(james): what should this be?
#define MAX_PATH 260
#endif

#define U32FromPointer(Pointer) ((uint32)(memory_index)(Pointer))
#define PointerFromU32(type, Value) (type*)((memory_index)Value)

#ifdef COMPILER_MSVC
// NOTE(james): modifiers used by ImGUI for indexing into "isDown" keyboard state arrays
#define L_SHIFT VK_LSHIFT
#define R_SHIFT VK_RSHIFT
#define L_CONTROL VK_LCONTROL
#define R_CONTROL VK_RCONTROL
#define L_SUPER VK_LWIN
#define R_SUPER VK_RWIN
#define L_ALT VK_LMENU
#define R_ALT VK_RMENU
#endif

#ifdef COMPILER_MSVC
#define CompletePreviousReadsBeforeFutureReads _ReadBarrier()
#define CompletePreviousWritesBeforeFutureWrites _WriteBarrier()
inline uint32 AtomicCompareExchangeUInt32(uint32 volatile *Value, uint32 New, uint32 Expected)
{
    uint32 Result = _InterlockedCompareExchange((long volatile *)Value, New, Expected);
    
    return(Result);
}
inline uint64 AtomicExchangeU64(uint64 volatile *Value, uint64 New)
{
    uint32 Result = _InterlockedExchange64((__int64 volatile *)Value, New);
    
    return(Result);
}
inline uint64 AtomicAddU64(uint64 volatile *Value, uint64 Addend)
{
    // NOTE(casey): Returns the original value _prior_ to adding
    uint64 Result = _InterlockedExchangeAdd64((__int64 volatile *)Value, Addend);
    
    return(Result);
}
inline uint32 GetThreadID(void)
{
    uint8 *ThreadLocalStorage = (uint8 *)__readgsqword(0x30);
    uint32 ThreadID = *(uint32 *)(ThreadLocalStorage + 0x48);
    
    return(ThreadID);
}

#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#ifdef COMPILER_MSVC
#define ALIGN(X) __declspec(align(X))
#elif COMPILER_GCC
#define ALIGN(X) __attribute__((aligned(X)))
#endif

//
// Vector structs
//
typedef union {
    struct
    {
        real32 x, y;
    };
    real32 E[2];
} v2;

introspect(category
           : "gui") struct rectangle2
{
    serialize("param") v2 Min;
    serialize("param") v2 Max;
};

typedef union v3 {
    struct
    {
        real32 x, y, z;
    };
    struct
    {
        real32 r, g, b;
    };
    struct
    {
        v2 xy;
        real32 Ignored0_;
    };
    struct
    {
        real32 Ignored1_;
        v2 yz;
    };
    real32 E[3];
} v3;

//NOTE(james): if this represents a position, w should be 1,
//if its a direction, w should be 0
typedef union v4 {
    struct
    {
        real32 x, y, z, w;
    };
    struct
    {
        union {
            v3 rgb;
            struct
            {
                real32 r, g, b;
            };
        };

        real32 a;
    };
    struct
    {
        union {
            v3 xyz;
            struct
            {
                real32 x, y, z;
            };
        };

        real32 w;
    };
    real32 E[4];
} v4;

typedef union matrix4 {
    struct
    {
        v4 Row0;
        v4 Row1;
        v4 Row2;
        v4 Row3;
    };
    struct
    {
        real32 M00;
        real32 M10;
        real32 M20;
        real32 M30;
        real32 M01;
        real32 M11;
        real32 M21;
        real32 M31;
        real32 M02;
        real32 M12;
        real32 M22;
        real32 M32;
        real32 M03;
        real32 M13;
        real32 M23;
        real32 M33;
    };
    real32 E[16];
} matrix4;

introspect(category
           : "string") struct string
{
    serialize("param") char* Text;
    serialize("param") uint32 Size; //memory allocated for string, not distance to end of string
};

/*
  NOTE(james): Members of transform should not be included in entity
  because we need transform hierarchies in animation
  and sub-mesh modelling hierarchies, maybe we should just
  make those entities too?
*/
introspect(category
           : "math") struct transform
{
    //NOTE(james): logical path, may differ from serialized if the entity has been moved or is not supposed to be
    // serialized
    char __PathMemory[128];
    string Path = { __PathMemory, 128 };

    // NOTE(james): serialized path is the location where this entities file can be found.
    //Case 0: Check when we do a scene load. If its empty, the entity is destroyed
    //Case 1: Check when we do a scene load. if the path has been changed, we can load
    serialize("param") char __SerializedPathMemory[128];
    string SerializedPath = { __SerializedPathMemory, 128 };

    serialize("param") v3 LocalPosition = { 0, 0, 0 };
    serialize("param") v3 LocalScale = { 1, 1, 1 };
    serialize("param") v4 LocalRotation = { 0, 0, 0, 1 };

    v3 Position = { 0, 0, 0 };
    v4 Rotation = { 0, 0, 0, 1 };
    //TODO(james): lossy world scale?

    v3 Forward = { 0, 0, 0 };
    v3 Right = { 0, 0, 0 };
    v3 Up = { 0, 0, 0 };

    matrix4 LocalToWorld = {};
    matrix4 WorldToLocal = {};

    bool32 IsDirty = false;

    transform* Parent = NULL;
    transform* Child = NULL; //NOTE(james):needs to be first in list
    transform* NextSibling = NULL; //singly linked list
};

enum texture_channel_type
{
    TextureChannelType_RGB,
    TextureChannelType_RGBA,
    TextureChannelType_Depth
};

enum texture_compression_type
{
    TextureCompressionType_Compressed,
    TextureCompressionType_Uncompressed
};

struct texture
{
    texture_compression_type CompressionType;
    texture_channel_type ChannelType;

    uint32 Width;
    uint32 Height;
    uint16 BitsPerChannel;
    uint8* Data;

    uint32 SizeOnDisk;

    uint32 Handle = MAX_UINT32;

    bool32 IsMultisampled;
    uint32 Samples;
    
    char DebugName[32];
};

//param from fbo creation
enum fbo_options
{
    FboOptions_Invalid = 0,
    FboOptions_Color = 1,
    FboOptions_Alpha = 2,
    FboOptions_Depth = 4,
    FboOptions_Multisample = 8
};

struct fbo
{
    uint32 FboHandle;
    //NOTE(james): SizeOnDisk,  Data, and BitsPerChannel will be empty
    texture Texture;
    bool32 HasColor;
    
    texture DepthTexture;
    bool32 HasDepth;
    
    char DebugName[32];
};

introspect(category
           : "math") typedef struct material
{
    serialize("param") v4 Color;

    texture* Texture;

    serialize("param") bool32 IsTransparent;

    serialize("param") bool32 CastShadows;
    serialize("param") bool32 RecieveShadows;
} material;

//six most extreme points on a mesh_extents
typedef struct mesh_extents
{
    v3 Front;
    v3 Back;
    v3 Left;
    v3 Right;
    v3 Top;
    v3 Bottom;
} mesh_extents;

typedef enum mesh_type {
    MeshType_Triangles,
    MeshType_TriangleStrip,
    MeshType_TriangleFan,
    MeshType_TriangleList_Adj,
    MeshType_TriangleStrip_Adj,
    MeshType_Lines,
    MeshType_LineLoop
} mesh_type;

typedef enum mesh_residency {
    MeshResidency_Static,//modified once, used many times
    MeshResidency_Dynamic,//modified repeatedly, used repeatedly
    MeshResidency_Stream//modified once, used a few times
} mesh_residency;

introspect(category
           : "math") typedef struct aabb
{
    serialize("param") v3 Position;
    serialize("param") v3 HalfWidths;
} aabb;

#if FMOD_SUPPORT

#include "fmod_studio.h"
#include "fmod_api.h"

typedef struct fmod_events
{
    FMOD_STUDIO_EVENTDESCRIPTION* Music;
    FMOD_STUDIO_EVENTDESCRIPTION* MouseClick;
    FMOD_STUDIO_EVENTDESCRIPTION* PlayerJump;
    FMOD_STUDIO_EVENTDESCRIPTION* Step;
    FMOD_STUDIO_EVENTDESCRIPTION* WeaponPickup;
    FMOD_STUDIO_EVENTDESCRIPTION* EnemyFire;
    FMOD_STUDIO_EVENTDESCRIPTION* BulletTravel;
    FMOD_STUDIO_EVENTDESCRIPTION* EnemyDeath;
    FMOD_STUDIO_EVENTDESCRIPTION* PlayerDeath;
} fmod_events;
#endif

typedef struct FMOD
{
#if FMOD_SUPPORT
    FMOD_STUDIO_SYSTEM* StudioSystem;

    fmod_events Events;
    fmod_api API;
#endif
} FMOD;

typedef struct game_button_state
{
    bool32 DownThisFrame;
    bool32 UpThisFrame;
    bool32 IsDown;
} game_button_state;

typedef struct game_controller
{
    bool32 IsConnected;

    bool32 IsAnalog;
    real32 LeftStickAverageX;
    real32 LeftStickAverageY;
    real32 RightStickAverageX;
    real32 RightStickAverageY;

    real64 MouseDeltaX;
    real64 MouseDeltaY;

    v2 MousePosition;

    int8 MouseWheelDelta;

    char InputCharacters[16]; //input string from keyboard to pass to IMGUI
    bool KeyboardState[512];

    //TODO(james): can we rework input somehow so that adding new buttons to follow doesn't require us 
    //to remember to bump the count?
    union {
        struct
        {
            game_button_state MoveForward; //w
            game_button_state MoveBack; //s
            game_button_state MoveLeft; //a
            game_button_state MoveRight; //d

            game_button_state MoveUp;
            game_button_state MoveDown;

            game_button_state ArrowKeyUp;
            game_button_state ArrowKeyDown;
            game_button_state ArrowKeyLeft;
            game_button_state ArrowKeyRight;

            //generic actions, ABXY controller, 1234 keyboard
            game_button_state Action1; //e
            game_button_state Action2; //r
            game_button_state Action3; //f
            game_button_state Action4; //q

            game_button_state Spacebar;

            game_button_state DeleteKey;

            game_button_state PlayPause; //f5
            
            game_button_state ToggleFullscreen; //f11

            game_button_state CtrlKey; //left and right

            game_button_state ToggleGUI; //tilde ~

            game_button_state CopyKey; //c
            game_button_state PasteKey; //v
            
            game_button_state UndoKey; //z

            game_button_state TabKey;

            game_button_state Quit;
            
            game_button_state RightMouseClick;
            game_button_state LeftMouseClick;
            game_button_state MiddleMouseClick;
        };
        game_button_state Buttons[50];
    };

    //calculate game side, these should be queried for game controls,
    //as they map to either keyboard or controller
    real32 MoveHorizontalAxis;
    real32 MoveVerticalAxis;
    real32 MoveForwardAxis;
} game_controller;

typedef struct game_input
{
    real64 DeltaTime;
    v2 ApplicationDim; //dimensions of window, or of screen if in full screen
    
    v2 ScreenDim; //dimensions of window, or of screen if in full screen

    bool32 IsRecording;
    bool32 IsPlayingBack; //currently using recorded playback

    game_controller Controllers[1];

    //NOTE(james): signal to platform layer
    bool32 QuitRequested;
} game_input;

#define IMGUI_GETIO_POINTER(name) ImGuiIO& name()
typedef IMGUI_GETIO_POINTER(imgui_getio_pointer);

typedef struct imgui_font_info
{
    imgui_getio_pointer* GetIOPtr;
    unsigned char* Pixels;
    int Width;
    int Height;
} imgui_font_info;

#define PLATFORM_ALLOCATE_MEMORY(name) void* name(memory_index Size)
typedef PLATFORM_ALLOCATE_MEMORY(platform_allocate_memory);

#define PLATFORM_DEALLOCATE_MEMORY(name) void name(void* Memory)
typedef PLATFORM_DEALLOCATE_MEMORY(platform_deallocate_memory);

struct platform_work_queue;

#define PLATFORM_WORK_QUEUE_CALLBACK(name) void name(platform_work_queue *Queue, void *Data)
typedef PLATFORM_WORK_QUEUE_CALLBACK(platform_work_queue_callback);

struct graphics_buffer;

#include "shaders/buffer_channels.h"
#define PLATFORM_CREATE_BUFFER(name) void name(graphics_buffer* Buffer, uint32 BufferChannel,uint32 ChannelWidth, mesh_residency Residency)
typedef PLATFORM_CREATE_BUFFER(platform_create_buffer);

#define PLATFORM_FILL_BUFFER(name) void name(graphics_buffer* Buffer)
typedef PLATFORM_FILL_BUFFER(platform_fill_buffer);

#define PLATFORM_ALLOCATE_TEXTURE(name) void name(texture* Texture)
typedef PLATFORM_ALLOCATE_TEXTURE(platform_allocate_texture);

#define PLATFORM_CREATE_FBO(name) fbo name(uint32 Width, uint32 Height, uint32 FboOptions, char* DebugName)
typedef PLATFORM_CREATE_FBO(platform_create_fbo);

#define PLATFORM_RESIZE_FBO(name) void name(fbo* Fbo, uint32 Width, uint32 Height)
typedef PLATFORM_RESIZE_FBO(platform_resize_fbo);

#define PLATFORM_IMGUI_SETUP(name) void name(imgui_font_info* FontInfo)
typedef PLATFORM_IMGUI_SETUP(platform_imgui_setup);

struct memory_arena
{
    memory_index Size;
    uint8* Base;
    memory_index Used;
    
    int32 TempCount; //
};

struct temporary_memory
{
    memory_arena* Arena;
    memory_index Used;
};


struct shader
{
    uint32 ProgramHandle = MAX_UINT32;
    uint32 UniformsStructHandle = MAX_UINT32;//called a UBO in GL
    uint32 UniformsStructSize = MAX_UINT32;
    
    serialize("param") char __FileName[MAX_PATH] = {};
    string FileName = { __FileName, MAX_PATH };
    
    serialize("param") char __PreprocessorHeader[255] = {};
    string PreprocessorHeader = { __PreprocessorHeader, 255 };
    
    serialize("param") char __CompiledPath[128] = {};
    string CompiledPath = { __CompiledPath, 128 };
    
    void* SourceFileHandle;
};

 struct platform_api;

#define PLATFORM_CREATE_SHADER(name) bool32 name(shader* Shader, char* MetashaderFilePath, char* Header,char* ShaderPreproc, char* DestinationFolder,  uint32 UniformStructSize, temporary_memory* Temp, platform_api* API)
typedef PLATFORM_CREATE_SHADER(platform_create_shader);

//FILE IO
typedef struct
{
    void* Handle;

    char _Mem[MAX_PATH];
    string Directory = { &_Mem[0], MAX_PATH };
    
    bool32 IsOpen;
} find_handle;

typedef struct
{
    char _Mem[MAX_PATH];
    string Name = { &_Mem[0], MAX_PATH };
} file;

typedef struct
{
    char _Mem[MAX_PATH];
    string Name = { &_Mem[0], MAX_PATH };
} directory;

#define PLATFORM_GET_FIRST_FILE_IN_DIRECTORY(name) bool32 name(char* DirectoryPath, file* File, find_handle* FindHandle)
typedef PLATFORM_GET_FIRST_FILE_IN_DIRECTORY(platform_get_first_file_in_directory);

#define PLATFORM_GET_NEXT_FILE(name) bool32 name(file* File, find_handle* FindHandle)
typedef PLATFORM_GET_NEXT_FILE(platform_get_next_file);

#define PLATFORM_GET_FIRST_SUBDIRECTORY(name) bool32 name(char* DirectoryPath, directory* Directory, find_handle* FindHandle)
typedef PLATFORM_GET_FIRST_SUBDIRECTORY(platform_get_first_subdirectory);

#define PLATFORM_GET_NEXT_SUBDIRECTORY(name) bool32 name(directory* Directory, find_handle* FindHandle)
typedef PLATFORM_GET_NEXT_SUBDIRECTORY(platform_get_next_subdirectory);

#define PLATFORM_GET_FILE_SIZE(name) uint64 name(char* FilePath)
typedef PLATFORM_GET_FILE_SIZE(platform_get_file_size);

#define PLATFORM_READ_BINARY_FILE(name) void name(char* FileName, void* Memory)
typedef PLATFORM_READ_BINARY_FILE(platform_read_binary_file);

#define PLATFORM_WRITE_BINARY_FILE(name) bool32 name(char* FileName, uint8* Base, memory_index Size)
typedef PLATFORM_WRITE_BINARY_FILE(platform_write_binary_file);

#define PLATFORM_FILE_EXISTS(name) bool32 name(char* Path)
typedef PLATFORM_FILE_EXISTS(platform_file_exists);

#define PLATFORM_DELETE_DIRECTORY(name) void name(char* DirectoryName)
typedef PLATFORM_DELETE_DIRECTORY(platform_delete_directory);

#define PLATFORM_CREATE_DIRECTORY(name) void name(char* Path)
typedef PLATFORM_CREATE_DIRECTORY(platform_create_directory);

#define PLATFORM_MOVE_FILE(name) bool32 name(char* ExistingName, char* NewName)
typedef PLATFORM_MOVE_FILE(platform_move_file);

#define PLATFORM_WATCH_FOR_FILE_CHANGES(name) void name(shader* Shader)
typedef PLATFORM_WATCH_FOR_FILE_CHANGES(platform_watch_for_file_changes);

#define PLATFORM_DUMP_CHANGED_FILE_NOTIFICATIONS(name) shader* name()
typedef PLATFORM_DUMP_CHANGED_FILE_NOTIFICATIONS(platform_dump_changed_file_notifications);

//
//
//
#define PLATFORM_SET_WINDOW_POSITION(name) void name(rectangle2 Rect)
typedef PLATFORM_SET_WINDOW_POSITION(platform_set_window_position);

#define PLATFORM_GET_WINDOW_POSITION(name) rectangle2 name()
typedef PLATFORM_GET_WINDOW_POSITION(platform_get_window_position);

#ifdef COMPILER_MSVC
#include <Winsock2.h>
#include <Ws2tcpip.h> //TODO(james): using TCP/IP stack? Just needed to convert IP Addresses
#endif

struct network_socket
{
#ifdef COMPILER_MSVC
    SOCKET Socket;
#endif

    bool IsBound;
};

#define PLATFORM_CREATE_SOCKET(name) network_socket name()
typedef PLATFORM_CREATE_SOCKET(platform_create_socket);

#define PLATFORM_BIND_SOCKET(name) void name(network_socket* Socket, char* IP, uint16 Port)
typedef PLATFORM_BIND_SOCKET(platform_bind_socket);

#define PLATFORM_SENDTO(name) void name(network_socket Socket, char* DestinationIP, uint16 DestinationPort, char* Payload)
typedef PLATFORM_SENDTO(platform_sendto);

#define PLATFORM_RECVFROM(name) uint32 name(network_socket Socket, char* OutputBuffer, uint32 OutputBufferSize, char* SenderIP, uint16* SenderPort)
typedef PLATFORM_RECVFROM(platform_recvfrom);

#define PLATFORM_QUERY_TIME(name) real64 name()
typedef PLATFORM_QUERY_TIME(platform_query_time);

#define PLATFORM_GET_TIMESTAMP(name) int64 name()
typedef PLATFORM_GET_TIMESTAMP(platform_get_timestamp);

#define PLATFORM_SHOW_CURSOR(name) void name()
typedef PLATFORM_SHOW_CURSOR(platform_show_cursor);

#define PLATFORM_HIDE_CURSOR(name) void name(v2 HidePos)
typedef PLATFORM_HIDE_CURSOR(platform_hide_cursor);

#define PLATFORM_SET_VSYNC(name) void name(bool32 VsyncOn)
typedef PLATFORM_SET_VSYNC(platform_set_vsync);

#define PLATFORM_SET_FULLSCREEN(name) void name(bool32 FullscreenOn)
typedef PLATFORM_SET_FULLSCREEN(platform_set_fullscreen);

typedef void platform_add_entry(platform_work_queue *Queue, platform_work_queue_callback *Callback, void *Data);
typedef void platform_complete_all_work(platform_work_queue *Queue);

typedef struct platform_api
{
    platform_create_buffer* CreateBuffer;
    platform_fill_buffer* FillBuffer;
    platform_allocate_texture* AllocateTexture;
    platform_create_fbo* CreateFbo;
    platform_resize_fbo* ResizeFbo;
    platform_imgui_setup* ImGuiSetup;
    platform_create_shader* CreateShader;

    platform_get_first_file_in_directory* GetFirstFileInDirectory;
    platform_get_next_file* GetNextFile;
    platform_get_first_subdirectory* GetFirstSubdirectory;
    platform_get_next_subdirectory* GetNextSubdirectory;

    platform_read_binary_file* ReadBinaryFile;
    platform_write_binary_file* WriteBinaryFile;
    platform_delete_directory* DeleteDirectory;
    platform_create_directory* CreateDirectory;
    platform_move_file* MoveFile;
    platform_file_exists* FileExists;
    platform_get_file_size* GetFileSize;
    platform_watch_for_file_changes* WatchForFileChanges;
    platform_dump_changed_file_notifications* DumpChangedFileNotifications;

    platform_set_window_position* SetWindowPosition;
    platform_get_window_position* GetWindowPosition;

    platform_create_socket* CreateSocket;
    platform_bind_socket* BindSocket;
    platform_sendto* SendTo;
    platform_recvfrom* RecvFrom;

    platform_query_time* QueryTime;

    platform_show_cursor* ShowCursor;
    platform_hide_cursor* HideCursor;

    platform_set_vsync* SetVsync;
    platform_set_fullscreen* SetFullscreen;
    
    platform_add_entry *AddEntry;
    platform_complete_all_work* CompleteAllWork;
} platform_api;

struct Prof_Report;

#include "renderdoc_app.h"

struct yam_profiling_scope
{
    uint32 ID; //"hash id"
    char Name[32];
    int64 StartTimestamp;
    int64 EndTimestamp;

    real64 TotalTime;

    real64 TotalTimeSelf;

    yam_profiling_scope* Parent;
    uint8 Depth;

    uint16 EntryCount; //num times scope hit

    yam_profiling_scope* Child;
    yam_profiling_scope* NextSibling;
};

/* 
  TODO(james): compute hash as combination of address of callee and callee's parent, if we have a collision
  re-hash until we find an open slot
  
  cache hash location as part of the Begin() function, since call-hierachy doesn't change much
*/
#define HASH_SCOPES_COUNT 300
struct yam_profiling
{
    yam_profiling_scope Scopes[HASH_SCOPES_COUNT];
    yam_profiling_scope InternalScope;

    platform_get_timestamp* GetTimestamp;
    int64 PerformanceFrequency;

    yam_profiling_scope* ScopeStack[16];
    uint8 ScopeStackDepth;

    yam_profiling_scope* Global;
    
    yam_profiling_scope* FrameTime;
    
    yam_profiling_scope* CurrentScope;
    
    real64 TargetFPS;
};

struct yam_profiling_report
{
    yam_profiling_scope Global;
    yam_profiling_scope InternalScope;
    
    bool32 SkippedFrame;
};

#if WWISE_SUPPORT
#include "wwise_api.h"

struct wwise
{
    wwise_api API;
};
#endif
typedef struct game_memory
{
    char ExecutableFolder[255]; //max_path

#if FMOD_SUPPORT
    FMOD Fmod; //maybe make this another param of GameUpdateAndRender?
#endif

#if WWISE_SUPPORT
    wwise Wwise;
#endif

    platform_api PlatformAPI;

    RENDERDOC_API_1_1_1* RenderDocAPI;

    bool32 ExecutableReloaded;

    void* PermanentStorage;
    size_t PermanentStorageSize;

    void* TransientStorage;
    size_t TransientStorageSize;
    
    platform_work_queue* WorkQueue;

    Prof_Report* ProfReport;

    yam_profiling* YamProfiling;
    yam_profiling_report* YamReport;
    yam_profiling_report* YamReportStart;
} game_memory;

struct directional_light
{
    matrix4 LightMatrix;
    fbo* Shadowmap;
    v3 LightDir;
    v4 LightColor;
};
/*
NOTE(james): I think when we have multiplayer, each player will have their own render_list
*/
struct render_assets;
typedef struct render_list
{
    bool32 ClearScreen; //For editor window only, this is a stop gap until there is better support for multiple render_lists

    render_assets* Assets;

    // This is in coordinates to the win32 window
    rectangle2 Viewport; //would be same as Input->ApplicationDim in fullscreen, smaller when in editor
    matrix4 CurrentView;
    matrix4 CurrentProjection;
    matrix4 CurrentViewProjection;
    
    v3 CameraForward;

    uint32 MaxPushBufferSize; //space allocated
    uint32 PushBufferUsed; //space actually used
    uint8* PushBufferBase; //pointer to allocated memory space

    uint32 PushBufferElementCount;

    #if 0
    //TODO(james): this sucks
    mesh* LinesMesh;
    v3 LinesVerts[2000];
    v4 LinesColors[2000];
     uint32 LinesElements[4000];
    uint32 LinesElementsCount;
    uint32 LinesVertsCount;
    #endif
    
    directional_light Light;
} render_list;


//
// FUNCTION POINTER STUBS
//

#define GAME_UPDATE_AND_RENDER(name) void name(game_memory* Memory, render_list* GameList, game_input* Input)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);

#ifdef __cplusplus
//}
#endif

#define PLATFORM_AGNOSTIC_H
#endif