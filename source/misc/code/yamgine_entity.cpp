

entity* FindEntity(linked_list* Master, char* Path, bool32 CheckSerializedPath = false)
{
    YamProf(FindEntity);
    Assert(Path[0] == '/');
    //could be optimized in child case
    
    linked_list_member* Member = NULL;
    entity* Entity = NULL;
    while (PROGRESS_LINKED_LIST(Master, Member, Entity))
    {
        char* EntityPath = CheckSerializedPath ? Entity->SerializedPath.Text : Entity->Path.Text;
        if (StringsAreEqualAbsolute(EntityPath, Path))
        {
            return Entity;
        }
    }
    return NULL;
}

char* GetNameFromPath(char* Path)
{
    uint32 LastSlash = FindLastOf(Path, '/');
    char* Result = &Path[LastSlash + 1];
    return Result;
}

// if an entity is created that has the same path as another entity,
// it must be provided a unique path.
void ResolveEntityPath(linked_list* Master, entity* Entity, char* Name = NULL)
{
    if (Entity->Parent == NULL)
    {
        STRING(NewPath, 128);
        AppendString(&NewPath, "/");
        if (Name == NULL)
        {
            char* EntityName = GetName(Entity);
            Assert(StringLength(EntityName) > 0);
            AppendString(&NewPath, EntityName);
        }
        else
        {
            AppendString(&NewPath, Name);
        }
        
        uint32 OriginalPathLength = StringLength(NewPath.Text);
        
        entity* PotentialMatchingEntity = FindEntity(Master, NewPath.Text);
        
        uint32 MatchesCount = 0;
        STRING(MatchIndexString, 5);
        STRING(PathWithNumericID, 128);
        while (PotentialMatchingEntity)
        {
            Assert(MatchesCount < 100);
            ClearString(&PathWithNumericID);
            AppendString(&PathWithNumericID, NewPath);
            
            AppendString(&PathWithNumericID, "_");
            
            ToString(&MatchIndexString, MatchesCount);
            AppendString(&PathWithNumericID, MatchIndexString.Text);
            
            PotentialMatchingEntity = FindEntity(Master, PathWithNumericID.Text);
            if (PotentialMatchingEntity)
            {
                MatchesCount++;
            }
            else
            {
                SetString(&NewPath, PathWithNumericID);
            }
        }
        
        SetString(&Entity->Path, NewPath);
    }
    else
    {
        STRING(EntityName, 128);
        if (Name != NULL)
        {
            SetString(&EntityName, Name);
        }
        else
        {
            SetString(&EntityName, GetName(Entity));
        }
        
        //
        // Make sure no sibling names collide
        //
        entity* Sibling = (entity*)Entity->Parent->Child;
        while (Sibling != NULL)
        {
            if (Sibling != Entity)
            {
                //TODO(james): do automatic numeric rename in this case
                Assert(!StringsAreEqualAbsolute(Name, GetName(Sibling)));
            }
            Sibling = (entity*)Sibling->NextSibling;
        }
        
        //
        // Create full path
        //
        entity* Parent = (entity*)Entity->Parent;
        Assert(Parent->Path.Text != NULL);
        ClearString(&Entity->Path);
        AppendStringEx(&Entity->Path, "%s/%s", Parent->Path.Text, EntityName.Text);
    }
}

// makes a child entity a root entity
void RemoveParent(linked_list* Master, entity* Entity)
{
    Assert(Entity->Parent != NULL);
    
    RemoveTransformParent(Entity);
    ResolveEntityPath(Master, Entity);
}

internal entity*
GetEntityFromList(linked_list* EntityMaster)
{
    linked_list_member* Member = GetFromList(EntityMaster);
    
    entity* NewEntity = (entity*)Member->Payload;
    
    NewEntity->EntityListMember = Member;
    
    return NewEntity;
}

void SetParent(linked_list* Master, entity* Entity, entity* Parent, char* EntityName = NULL)
{
    SetTransformParent((transform*)Entity, (transform*)Parent);
    ResolveEntityPath(Master, Entity, EntityName);
}

internal entity*
CreateEntity(linked_list* EntityMaster, char* Name = "Uninitialized", entity* Parent = NULL)
{
    YamProf(CreateEntity);
    Assert(FindFirstOf(Name, '/') == MAX_UINT32); //ensure name string contains no forward slashes

    entity* NewEntity = GetEntityFromList(EntityMaster);
    InitializeEntity(NewEntity, false);

    if (Parent != NULL)
    {
        SetParent(EntityMaster, NewEntity, Parent, Name);
    }
    else
    {
        ResolveEntityPath(EntityMaster, NewEntity, Name);
    }

    SetDirty(NewEntity);
    ResolveLocalToWorld(NewEntity);

    return NewEntity;
}

inline entity*
CreateOrFindEntity(linked_list* EntityMaster, char* Path, entity* Parent, bool32* IsNewEntity)
{
    YamProf(CreateOrFindEntity);
    entity* Result = FindEntity(EntityMaster, Path, true); //check against SerializedPath because name might have changed but then it was reloaded
    if (Result == NULL)
    {
        *IsNewEntity = true;
        char* Name = GetNameFromPath(Path);
        Result = CreateEntity(EntityMaster, Name, Parent);
    }
    return Result;
}

inline entity*
DuplicateEntity(linked_list* EntityMaster, entity* ToCopy, bool32 PreserveParent)
{
    YamProf(DuplicateEntity);
    entity* NewEntity = GetEntityFromList(EntityMaster);

    entity Storage = *NewEntity;

    *NewEntity = *ToCopy;

    NewEntity->Path.Text = NewEntity->__PathMemory;
    NewEntity->SerializedPath.Text = NewEntity->__SerializedPathMemory;
    NewEntity->ColliderMeshPath.Text = NewEntity->__ColliderMeshPathMemory;
    NewEntity->RenderMeshPath.Text = NewEntity->__RenderMeshPathMemory;

    ClearString(&NewEntity->SerializedPath);

    NewEntity->EntityListMember = Storage.EntityListMember;

    if (!PreserveParent)
    {
        NewEntity->Parent = NULL;
    }

    NewEntity->NextSibling = NULL;
    NewEntity->Child = NULL;

    ResolveEntityPath(EntityMaster, NewEntity);

    // Deep copy
    if (ToCopy->Child)
    {
        entity* CopyOfChild = DuplicateEntity(EntityMaster, (entity*)ToCopy->Child, false);
        SetParent(EntityMaster, CopyOfChild, NewEntity);

        entity* Sibling = (entity*)ToCopy->Child->NextSibling;
        while (Sibling != NULL)
        {
            entity* CopyOfSibling = DuplicateEntity(EntityMaster, Sibling, false);
            SetParent(EntityMaster, CopyOfSibling, NewEntity);
            Sibling = (entity*)Sibling->NextSibling;
        }
    }

    return NewEntity;
}

struct collision_group;

inline void
DeleteEntity(entity* Entity, linked_list* EntityMaster, collision_group* CollisionGroup)
{
    YamProf(DeleteEntity);
    if (Entity->Child)
    {
        while (Entity->Child)
        {
            entity* Child = (entity*)Entity->Child;
            DeleteEntity(Child, EntityMaster, CollisionGroup);
        }
    }

    RemoveFromList(EntityMaster, Entity->EntityListMember);

    if (Entity->Parent)
    {
        RemoveTransformParent(Entity);
    }

    if (Entity->CollisionGroupMember != NULL)
    {
        RemoveEntityFromCollisionList(CollisionGroup, Entity->CollisionGroupMember);
    }

    *Entity = entity();
}

void Enable(entity* Entity)
{
    //Assert(!Entity->Enabled);//dont try enabling something that was already enabled

    Entity->Enabled = true;
    entity* Child = (entity*)Entity->Child;
    while (Child != NULL)
    {
        Enable(Child);
        Child = (entity*)Child->NextSibling;
    }
}

void Disable(entity* Entity)
{
    //Assert(Entity->Enabled);

    Entity->Enabled = false;
    entity* Child = (entity*)Entity->Child;
    while (Child != NULL)
    {
        Disable(Child);
        Child = (entity*)Child->NextSibling;
    }
}
