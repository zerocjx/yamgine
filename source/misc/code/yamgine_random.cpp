
real32 GenRandom01()
{
    real32 Result = ((real32)rand()) / ((real32)RAND_MAX);
    return Result;
}

//between -1 and 1
real32 GenRandomCanonical()
{
    real32 Result = Lerp(-1, 1, GenRandom01());
    return Result;
}

real32 RandomRange(real32 A, real32 B)
{
    real32 Result = Lerp(A, B, GenRandom01());
    return Result;
}

v3 RandomPointOnCircle(v3 CirclePos, v3 CircleNormal, real32 CircleRadius)
{
    v3 PerpOfNormal = Normalize(RandomPerpendicular(CircleNormal)); //pointing out to radius edge

    real32 RandomTheta = GenRandom01() * (PI * 2);

    v4 Rotator = AngleAxisToQuaternion(RandomTheta, CircleNormal);

    v3 RandomPointOnPerimeter = RotateVectorByQuaternion(PerpOfNormal, Rotator);

    v3 Result = CirclePos + (RandomPointOnPerimeter * GenRandom01() * CircleRadius);

    return Result;
}

v3 RandomPointOnCircleEdge(v3 CirclePos, v3 CircleNormal, real32 CircleRadius)
{
    v3 PerpOfNormal = Normalize(RandomPerpendicular(CircleNormal)); //pointing out to radius edge
    
    real32 RandomTheta = GenRandom01() * (PI * 2);
    
    v4 Rotator = AngleAxisToQuaternion(RandomTheta, CircleNormal);
    
    v3 RandomPointOnPerimeter = RotateVectorByQuaternion(PerpOfNormal, Rotator);
    
    v3 Result = CirclePos + (RandomPointOnPerimeter * CircleRadius);
    
    return Result;
}

v3 RandomPointOnCircleSliceEdge(v3 CirclePos, v3 CircleNormal, real32 CircleRadius, 
                                real32 RadiansA, real32 RadiansB)
{
    v3 PerpOfNormal = Normalize(RandomPerpendicular(CircleNormal)); //pointing out to radius edge
    
    real32 RandomTheta = Lerp(RadiansA, RadiansB, GenRandom01());
    
    v4 Rotator = AngleAxisToQuaternion(RandomTheta, CircleNormal);
    
    v3 RandomPointOnPerimeter = RotateVectorByQuaternion(PerpOfNormal, Rotator);
    
    v3 Result = CirclePos + (RandomPointOnPerimeter * CircleRadius);
    
    return Result;
}

v4 RandomColor()
{
    v4 Result = V4(GenRandom01(), GenRandom01(), GenRandom01(), 1);
    return Result;
}


//TODO(james): stackoverflow gods say this isn't the best way... good enough for demo
//http://math.stackexchange.com/questions/44689/how-to-find-a-random-axis-or-unit-vector-in-3d
inline v3
GenRandomOnUnitSphere()
{
    real32 RandomTheta = GenRandom01() * (PI * 2);
    real32 RandomCanonical = GenRandomCanonical();
    real32 RandomCanonicalSquared = RandomCanonical * RandomCanonical;
    
    v3 Result = {};
    Result.x = SquareRoot(1 - (RandomCanonicalSquared)) * Cos(RandomTheta);
    Result.y = SquareRoot(1 - (RandomCanonicalSquared)) * Sin(RandomTheta);
    Result.z = RandomCanonical;
    
    return Result;
}
