#if !defined(YAMGINE_MEMORY_ARENA_H)

//
// Profiling forward declare stuff?
//
void YamProfEnd();
struct yam_profiling_auto
{
    inline ~yam_profiling_auto()
    {
        YamProfEnd();
    }
};
void YamProfBegin_(char* Name);
#define YamProfBegin(Name)           \
    global_variable uint32 __##Name; \
    YamProfBegin_(#Name) //, __##Name
#define YamProf(Name)                \
    yam_profiling_auto _Prof_##Name; \
    YamProfBegin(Name)

//
//
//

#include "emmintrin.h"
#define ZeroStruct(Instance) ZeroSize(sizeof(Instance), &(Instance))
#define ZeroArray(Count, Pointer) ZeroSize(Count * sizeof((Pointer)[0]), Pointer)
inline void
ZeroSize(memory_index Size, void* Ptr)
{
    //memory_index OrigSize = Size;
    __m128i* Row = (__m128i*)Ptr;
    __m128i Zero = _mm_setzero_si128();
    while (Size >= sizeof(__m128i))
    {
        //Assert(((unsigned char*)Row) <= ((unsigned char*)Ptr)+(OrigSize-sizeof(__m128i)));
        *Row++ = Zero;
        Size -= sizeof(__m128i);
    }

    uint8* Byte = (uint8*)Row;
    while (Size--)
    {
        *Byte++ = 0;
    }
}

inline void
InitializeArena(memory_arena* Arena, memory_index Size, void* Base)
{
    Arena->Size = Size;
    Arena->Base = (uint8*)Base;
    Arena->Used = 0;
    Arena->TempCount = 0;
}

inline memory_index
GetAlignmentOffset(memory_arena* Arena, memory_index Alignment)
{
    memory_index AlignmentOffset = 0;

    memory_index ResultPointer = (memory_index)Arena->Base + Arena->Used;
    memory_index AlignmentMask = Alignment - 1;
    if (ResultPointer & AlignmentMask)
    {
        AlignmentOffset = Alignment - (ResultPointer & AlignmentMask);
    }

    return (AlignmentOffset);
}

enum arena_push_flag
{
    ArenaFlag_ClearToZero = 0x1,
};

struct arena_push_params
{
    uint32 Flags;
    uint32 Alignment;
};

inline arena_push_params
DefaultArenaParams(void)
{
    arena_push_params Params;
    Params.Flags = ArenaFlag_ClearToZero;
    Params.Alignment = 4;
    return (Params);
}

inline arena_push_params
AlignNoClear(uint32 Alignment)
{
    arena_push_params Params = DefaultArenaParams();
    Params.Flags &= ~ArenaFlag_ClearToZero;
    Params.Alignment = Alignment;
    return (Params);
}

inline arena_push_params
Align(uint32 Alignment, bool32 Clear)
{
    arena_push_params Params = DefaultArenaParams();
    if (Clear)
    {
        Params.Flags |= ArenaFlag_ClearToZero;
    }
    else
    {
        Params.Flags &= ~ArenaFlag_ClearToZero;
    }
    Params.Alignment = Alignment;
    return (Params);
}

inline arena_push_params
NoClear(void)
{
    arena_push_params Params = DefaultArenaParams();
    Params.Flags &= ~ArenaFlag_ClearToZero;
    return (Params);
}

inline memory_index
GetArenaSizeRemaining(memory_arena* Arena, arena_push_params Params = DefaultArenaParams())
{
    memory_index Result = Arena->Size - (Arena->Used + GetAlignmentOffset(Arena, Params.Alignment));

    return (Result);
}

// TODO(casey): Optional "clear" parameter!!!!
#define PushStruct(Arena, type, ...) (type*)PushSize_(Arena, sizeof(type), ##__VA_ARGS__)
#define PushArray(Arena, Count, type, ...) (type*)PushSize_(Arena, (Count) * sizeof(type), ##__VA_ARGS__)
#define PushSize(Arena, Size, ...) PushSize_(Arena, Size, ##__VA_ARGS__)
#define PushCopy(Arena, Size, Source, ...) Copy(Size, Source, PushSize_(Arena, Size, ##__VA_ARGS__))
inline memory_index
GetEffectiveSizeFor(memory_arena* Arena, memory_index SizeInit, arena_push_params Params = DefaultArenaParams())
{
    memory_index Size = SizeInit;

    memory_index AlignmentOffset = GetAlignmentOffset(Arena, Params.Alignment);
    Size += AlignmentOffset;

    return (Size);
}

inline bool32
ArenaHasRoomFor(memory_arena* Arena, memory_index SizeInit, arena_push_params Params = DefaultArenaParams())
{
    memory_index Size = GetEffectiveSizeFor(Arena, SizeInit, Params);
    bool32 Result = ((Arena->Used + Size) <= Arena->Size);
    return (Result);
}

inline void*
PushSize_(memory_arena* Arena, memory_index SizeInit, arena_push_params Params = DefaultArenaParams())
{
    //YamProf(ArenaPushSize);
    memory_index Size = GetEffectiveSizeFor(Arena, SizeInit, Params);

    Assert((Arena->Used + Size) <= Arena->Size);

    memory_index AlignmentOffset = GetAlignmentOffset(Arena, Params.Alignment);
    void* Result = Arena->Base + Arena->Used + AlignmentOffset;
    Arena->Used += Size;

    Assert(Size >= SizeInit);

    if (Params.Flags & ArenaFlag_ClearToZero)
    {
        ZeroSize(SizeInit, Result);
    }

    return (Result);
}

void PushString(string* String, memory_arena* Arena, uint32 Size)
{
    String->Text = PushArray(Arena, Size, char);
    String->Size = Size;
}

inline void
CheckArena(memory_arena* Arena)
{
    Assert(Arena->TempCount == 0);
}

inline void
SubArena(memory_arena* Result, memory_arena* Arena, memory_index Size, arena_push_params Params = DefaultArenaParams())
{
    Result->Size = Size;
    Result->Base = (uint8*)PushSize_(Arena, Size, Params);
    Result->Used = 0;
    Result->TempCount = 0;
}

inline void*
Copy(memory_index Size, void* SourceInit, void* DestInit)
{
    YamProf(Arena_Copy);
    uint8* Source = (uint8*)SourceInit;
    uint8* Dest = (uint8*)DestInit;
    while (Size--)
    {
        *Dest++ = *Source++;
    }

    return (DestInit);
}

inline void
CopyArena(memory_arena* Source, memory_arena* Dest)
{
    Assert(Dest->Size >= Source->Size);
    Copy(Source->Used, Source->Base, Dest->Base);
    Dest->Used = Source->Used;
}

inline temporary_memory
BeginTemporaryMemory(memory_arena* Arena)
{
    temporary_memory Result;

    Result.Arena = Arena;
    Result.Used = Arena->Used;

    ++Arena->TempCount;

    return (Result);
}

inline void
EndTemporaryMemory(temporary_memory TempMem)
{
    memory_arena* Arena = TempMem.Arena;
    Assert(Arena->Used >= TempMem.Used);
    Arena->Used = TempMem.Used;
    Assert(Arena->TempCount > 0);
    --Arena->TempCount;
}

#define YAMGINE_MEMORY_ARENA_H
#endif