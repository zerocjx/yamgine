#include "yamgine_fbx_loading.h"

//NOTE(james): FBX SDK Documentation http://docs.autodesk.com/FBX/2014/ENU/FBX-SDK-Documentation/index.html

//typedef GLuint APIENTRY _glCreateShader (GLenum type);
//global_variable _glCreateShader *glCreateShader;

//NOTE(james): I thought these model space extents would be useful for AABB generation but I was mistaken.
//maybe they will be useful for something else?
/*
void CalculateExtents(mesh* Mesh)
{
	    
    // Find Extents
    float I = FLT_MAX;
    Mesh->Extents.Front =  V3( 0, 0,-I);
    Mesh->Extents.Top =    V3( 0,-I, 0);
    Mesh->Extents.Right =  V3(-I, 0, 0);
    Mesh->Extents.Back =   V3( 0, 0, I);
    Mesh->Extents.Bottom = V3( 0, I, 0);
    Mesh->Extents.Left =   V3( I, 0, 0);
    for (uint32 VertexIndex = 0;
        VertexIndex < Mesh->VertexCount;
        VertexIndex++)
    {
        v3 Vertex = Mesh->Vertices[VertexIndex];
        if (Vertex.z > Mesh->Extents.Front.z)
        {
            Mesh->Extents.Front = Vertex;
		}
        if (Vertex.y > Mesh->Extents.Top.y)
        {
            Mesh->Extents.Top = Vertex;
		}
        if (Vertex.x > Mesh->Extents.Right.x)
        {
            Mesh->Extents.Right = Vertex;
		}
        if (Vertex.z < Mesh->Extents.Back.z)
        {
            Mesh->Extents.Back = Vertex;
		}
        if (Vertex.y < Mesh->Extents.Bottom.y)
        {
            Mesh->Extents.Bottom = Vertex;
		}
        if (Vertex.x < Mesh->Extents.Left.x)
        {
            Mesh->Extents.Left = Vertex;
		}
	}
}
*/

//NOTE(james): blender triangularize http://blender.stackexchange.com/questions/19253/how-to-make-all-polygons-into-triangles

/*
Need to have two paths, one for indexed meshes and one without
1.
the indexed mesh path will need to take some sort of average of all the normals off that vertex, good for smooth models

2.
the non-index mesh path will have as many vertices as there are normals. 
*/

void LoadMesh(FbxMesh* FbxMesh, bool32 FlipWinding, transform* Parent,
              memory_arena* AssetArena, mesh* Mesh,
              platform_api* PlatformAPI)
{
    Mesh->MeshType = MeshType_Triangles;

    //
    //check that model is all triangles
    //
    uint32 PolygonCount = (uint32)FbxMesh->GetPolygonCount();
    for (uint32 PolygonIndex = 0; PolygonIndex < PolygonCount; PolygonIndex++)
    {
        int PolygonSize = FbxMesh->GetPolygonSize(PolygonIndex);
        if (PolygonSize != 3)
        {
            Assert(0);
        }
    }

    //
    // Basic Normals Info
    //
    //NOTE(james):FBX calls Elements vertices. Vertices are stored as ints. We are
    //storing them as uints so that's why have have to do this casting instead of a bulk copy

    //NOTE(james): blender normals export info http://polycount.com/discussion/155012/exporting-smoothing-information-from-blender-via-fbx
    //NOTE(james): fbx format per vertex and per face info http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/how-to-work-with-fbx-sdk-r3582
    FbxGeometryElementNormal* NormalAccessor = FbxMesh->GetElementNormal(0);
    Assert(NormalAccessor->GetReferenceMode() == FbxGeometryElement::eDirect);

    bool32 PerVertexNormals = (NormalAccessor->GetMappingMode() == FbxGeometryElement::eByControlPoint);

    //
    // Basic UV Info
    //
    bool32 PerVertexUVs = false;

    FbxStringList UVSetNameList;
    FbxMesh->GetUVSetNames(UVSetNameList);

    char* UVSetName = UVSetNameList.GetStringAt(0); //TODO(james): assumes all sets have same mapping style
    FbxGeometryElementUV* TestUVElement = FbxMesh->GetElementUV(UVSetName);
    if (TestUVElement)
    {
        Mesh->HasUVs = true;

        // only support mapping mode eByPolygonVertex and eByControlPoint
        Assert(TestUVElement->GetMappingMode() == FbxGeometryElement::eByPolygonVertex || TestUVElement->GetMappingMode() == FbxGeometryElement::eByControlPoint);

        PerVertexUVs = (TestUVElement->GetMappingMode() == FbxGeometryElement::eByControlPoint);
    }

    //
    // Load Data
    //
    bool32 CanBeIndexed = (PerVertexNormals && (!Mesh->HasUVs || (Mesh->HasUVs && PerVertexUVs)));
    if (CanBeIndexed)
    {
        Mesh->IsIndexedMesh = true;

        Assert(0); //NOTE(james): UV loading not implemented!

        //load vertices
        Mesh->VertexCount = FbxMesh->GetControlPointsCount();
        Mesh->Vertices = PushArray(AssetArena, Mesh->VertexCount, v3);

        // vertex positions appear to be in world space, so this brings them to local
        matrix4 WorldToLocal = InverseMatrix(Mesh->LocalToWorld);

        Assert(FlipWinding); //not implemented!
        for (uint32 VertexIndex = 0;
             VertexIndex < Mesh->VertexCount;
             VertexIndex++)
        {
            FbxVector4 FbxVert = FbxMesh->GetControlPointAt(VertexIndex);
            Mesh->Vertices[VertexIndex].x = (real32)FbxVert.mData[0];
            Mesh->Vertices[VertexIndex].y = (real32)FbxVert.mData[1];
            Mesh->Vertices[VertexIndex].z = (real32)FbxVert.mData[2];
            Mesh->Vertices[VertexIndex] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex]);
        }

        //load elements
        Mesh->ElementCount = FbxMesh->GetPolygonVertexCount();
        Mesh->Elements = PushArray(AssetArena, Mesh->ElementCount, uint32);

        int* FbxElements = FbxMesh->GetPolygonVertices();
        for (uint32 ElementIndex = 0;
             ElementIndex < Mesh->ElementCount;
             ElementIndex++)
        {
            Mesh->Elements[ElementIndex] = (uint32)FbxElements[ElementIndex];
        }

        //load normals
        //TODO(james): retrieve averaged normals here
        Mesh->HasNormals = true;
        Mesh->Normals = PushArray(AssetArena, Mesh->ElementCount, v3);
        FbxArray<FbxVector4> FbxNormals;

        if (FbxMesh->GetPolygonVertexNormals(FbxNormals))
        {
            Assert(FbxNormals.Size() == Mesh->ElementCount);
            for (uint32 NormalIndex = 0;
                 NormalIndex < Mesh->ElementCount;
                 NormalIndex++)
            {
                Mesh->Normals[NormalIndex].x = (float)FbxNormals[NormalIndex].mData[0];
                Mesh->Normals[NormalIndex].y = (float)FbxNormals[NormalIndex].mData[1];
                Mesh->Normals[NormalIndex].z = (float)FbxNormals[NormalIndex].mData[2];
            }
        }
        else
        {
            Assert(0);
        }

        /*
        // NOTE(james): how to load per vertex UVs
        
            if( UVElement->GetMappingMode() == FbxGeometryElement::eByControlPoint )
            {
                Assert(0);//per vertex element
                for( int PolyIndex = 0; PolyIndex < PolyCount; ++PolyIndex )
                {
                    // build the max index array that we need to pass into MakePoly
                    const int PolySize = FbxMesh->GetPolygonSize(PolyIndex);
                    for( int VertIndex = 0; VertIndex < PolySize; ++VertIndex )
                    {
                        FbxVector2 UVValue;
                        
                        //get the index of the current vertex in control points array
                        int PolyVertIndex = FbxMesh->GetPolygonVertex(PolyIndex,VertIndex);
                        
                        //the UV index depends on the reference mode
                        int UVIndex = UseIndex ? UVElement->GetIndexArray().GetAt(PolyVertIndex) : PolyVertIndex;
                        
                        UVValue = UVElement->GetDirectArray().GetAt(UVIndex);
                        
                        Mesh->UVs[UVIndex].x = UVValue[0];
                        Mesh->UVs[UVIndex].y = UVValue[1];
                    }
                }
            }
        */
    }
    else //some data is per element, so all indexed data must be expanded
    {
        matrix4 WorldToLocal = InverseMatrix(Mesh->LocalToWorld);

        //NOTE(james):
        Mesh->IsIndexedMesh = false;

        //
        // Vertices
        //
        Mesh->VertexCount = FbxMesh->GetPolygonVertexCount();
        Mesh->Vertices = PushArray(AssetArena, Mesh->VertexCount, v3);
        int* FbxElements = FbxMesh->GetPolygonVertices();
        for (uint32 VertexIndex = 0;
             VertexIndex < Mesh->VertexCount;
             VertexIndex += 3)
        {
            if (FlipWinding)
            {
                FbxVector4 FbxVert = FbxMesh->GetControlPointAt(FbxElements[VertexIndex]);
                Mesh->Vertices[VertexIndex].x = (real32)FbxVert.mData[0];
                Mesh->Vertices[VertexIndex].y = (real32)FbxVert.mData[1];
                Mesh->Vertices[VertexIndex].z = (real32)FbxVert.mData[2];
                Mesh->Vertices[VertexIndex] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex]);

                FbxVert = FbxMesh->GetControlPointAt(FbxElements[VertexIndex + 2]);
                Mesh->Vertices[VertexIndex + 2].x = (real32)FbxVert.mData[0];
                Mesh->Vertices[VertexIndex + 2].y = (real32)FbxVert.mData[1];
                Mesh->Vertices[VertexIndex + 2].z = (real32)FbxVert.mData[2];
                Mesh->Vertices[VertexIndex + 2] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex + 2]);

                FbxVert = FbxMesh->GetControlPointAt(FbxElements[VertexIndex + 1]);
                Mesh->Vertices[VertexIndex + 1].x = (real32)FbxVert.mData[0];
                Mesh->Vertices[VertexIndex + 1].y = (real32)FbxVert.mData[1];
                Mesh->Vertices[VertexIndex + 1].z = (real32)FbxVert.mData[2];
                Mesh->Vertices[VertexIndex + 1] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex + 1]);
            }
            else
            {
                FbxVector4 FbxVert = FbxMesh->GetControlPointAt(FbxElements[VertexIndex]);
                Mesh->Vertices[VertexIndex].x = (real32)FbxVert.mData[0];
                Mesh->Vertices[VertexIndex].y = (real32)FbxVert.mData[1];
                Mesh->Vertices[VertexIndex].z = (real32)FbxVert.mData[2];
                Mesh->Vertices[VertexIndex] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex]);

                FbxVert = FbxMesh->GetControlPointAt(FbxElements[VertexIndex + 1]);
                Mesh->Vertices[VertexIndex + 1].x = (real32)FbxVert.mData[0];
                Mesh->Vertices[VertexIndex + 1].y = (real32)FbxVert.mData[1];
                Mesh->Vertices[VertexIndex + 1].z = (real32)FbxVert.mData[2];
                Mesh->Vertices[VertexIndex + 1] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex + 1]);

                FbxVert = FbxMesh->GetControlPointAt(FbxElements[VertexIndex + 2]);
                Mesh->Vertices[VertexIndex + 2].x = (real32)FbxVert.mData[0];
                Mesh->Vertices[VertexIndex + 2].y = (real32)FbxVert.mData[1];
                Mesh->Vertices[VertexIndex + 2].z = (real32)FbxVert.mData[2];
                Mesh->Vertices[VertexIndex + 2] = MatrixByPosition(&WorldToLocal, Mesh->Vertices[VertexIndex + 2]);
            }
        }

        //
        // Normals
        //
        Mesh->Normals = PushArray(AssetArena, Mesh->VertexCount, v3);

        if (PerVertexNormals)
        {
            //Assert(NormalAccessor->GetDirectArray().GetCount() == Mesh->VertexCount);
            for (uint32 NormalIndex = 0;
                 NormalIndex < Mesh->VertexCount;
                 NormalIndex += 3)
            {
                if (FlipWinding)
                {
                    FbxVector4 FbxNormal = NormalAccessor->GetDirectArray().GetAt(FbxElements[NormalIndex]);
                    Mesh->Normals[NormalIndex].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(FbxElements[NormalIndex + 2]);
                    Mesh->Normals[NormalIndex + 1].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 1].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 1].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(FbxElements[NormalIndex + 1]);
                    Mesh->Normals[NormalIndex + 2].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 2].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 2].z = (float)FbxNormal.mData[2];
                }
                else
                {
                    FbxVector4 FbxNormal = NormalAccessor->GetDirectArray().GetAt(FbxElements[NormalIndex]);
                    Mesh->Normals[NormalIndex].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(FbxElements[NormalIndex + 1]);
                    Mesh->Normals[NormalIndex + 1].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 1].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 1].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(FbxElements[NormalIndex + 2]);
                    Mesh->Normals[NormalIndex + 2].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 2].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 2].z = (float)FbxNormal.mData[2];
                }
            }
        }
        else
        {
            //Assert(NormalAccessor->GetDirectArray().GetCount() == Mesh->VertexCount);
            for (uint32 NormalIndex = 0;
                 NormalIndex < Mesh->VertexCount;
                 NormalIndex += 3)
            {
                if (FlipWinding)
                {
                    FbxVector4 FbxNormal = NormalAccessor->GetDirectArray().GetAt(NormalIndex);
                    Mesh->Normals[NormalIndex].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(NormalIndex + 2);
                    Mesh->Normals[NormalIndex + 1].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 1].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 1].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(NormalIndex + 1);
                    Mesh->Normals[NormalIndex + 2].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 2].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 2].z = (float)FbxNormal.mData[2];
                }
                else
                {
                    FbxVector4 FbxNormal = NormalAccessor->GetDirectArray().GetAt(NormalIndex);
                    Mesh->Normals[NormalIndex].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(NormalIndex + 1);
                    Mesh->Normals[NormalIndex + 1].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 1].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 1].z = (float)FbxNormal.mData[2];

                    FbxNormal = NormalAccessor->GetDirectArray().GetAt(NormalIndex + 2);
                    Mesh->Normals[NormalIndex + 2].x = (float)FbxNormal.mData[0];
                    Mesh->Normals[NormalIndex + 2].y = (float)FbxNormal.mData[1];
                    Mesh->Normals[NormalIndex + 2].z = (float)FbxNormal.mData[2];
                }
            }
        }
        //
        // UVS
        //
        if (Mesh->HasUVs)
        {

            for (int UVSetIndex = 0;
                 UVSetIndex < UVSetNameList.GetCount();
                 UVSetIndex++)
            {
                Assert(UVSetNameList.GetCount() == 1); //NOTE(james): not handling more than 1 UV set yet

                Mesh->UVs = PushArray(AssetArena, Mesh->VertexCount, v2);
                Mesh->HasUVs = true;

                char* UVSetName = UVSetNameList.GetStringAt(0);
                FbxGeometryElementUV* UVElement = FbxMesh->GetElementUV(UVSetName);

                Assert(UVElement->GetMappingMode() == FbxGeometryElement::eByPolygonVertex);
                Assert(!FlipWinding);

                for (uint32 UVsIndex = 0;
                     UVsIndex < Mesh->VertexCount;
                     UVsIndex += 3)
                {
                    int UVElementsIndex = UVElement->GetIndexArray().GetAt((int)UVsIndex);
                    FbxVector2 UVValue = UVElement->GetDirectArray().GetAt(UVElementsIndex);
                    Mesh->UVs[UVsIndex].x = UVValue[0];
                    Mesh->UVs[UVsIndex].y = UVValue[1];

                    UVElementsIndex = UVElement->GetIndexArray().GetAt((int)UVsIndex + 1);
                    UVValue = UVElement->GetDirectArray().GetAt(UVElementsIndex);
                    Mesh->UVs[UVsIndex + 1].x = UVValue[0];
                    Mesh->UVs[UVsIndex + 1].y = UVValue[1];

                    UVElementsIndex = UVElement->GetIndexArray().GetAt((int)UVsIndex + 2);
                    UVValue = UVElement->GetDirectArray().GetAt(UVElementsIndex);
                    Mesh->UVs[UVsIndex + 2].x = UVValue[0];
                    Mesh->UVs[UVsIndex + 2].y = UVValue[1];
                }
            }
        }
    }

    //CalculateExtents(Mesh);

    PlatformAPI->AllocateMesh(Mesh);
}

void LoadNode(FbxNode* Node, mesh* Parent,
              memory_arena* AssetArena,
              platform_api* PlatformAPI)
{
    //PrintNode(Node);

    /*
         - Rotation offset (Roff)
     - Rotation pivot (Rp)
     - Pre-rotation (Rpre)
     - Post-rotation (Rpost)
     - Scaling offset (Soff)
     - Scaling pivot (Sp)
     - Geometric translation (Gt)
     - Geometric rotation (Gr)
     - Geometric scaling (Gs)
     
 These values combine in the matrix form to compute the World transform of the node 
 using the formula:
 
        World = ParentWorld * T * Roff * Rp * Rpre * R * Rpost * Rp-1 * Soff * Sp * S * Sp-1
    */

    // these are all of the values that can affect the final transform

    FbxDouble3 translation = Node->LclTranslation.Get();
    FbxDouble3 rotation = Node->LclRotation.Get();
    FbxDouble3 scaling = Node->LclScaling.Get();

    FbxDouble3 rotationOffset = Node->RotationOffset.Get();
    Assert(rotationOffset[0] + rotationOffset[1] + rotationOffset[2] == 0);

    FbxDouble3 rotationPivot = Node->RotationPivot.Get();
    Assert(rotationPivot[0] + rotationPivot[1] + rotationPivot[2] == 0);

    FbxDouble3 preRotation = Node->PreRotation.Get(); //might be the same as the local rotation
    //Assert(preRotation[0] + preRotation[1] + preRotation[2] == 0);

    FbxDouble3 postRotation = Node->PostRotation.Get();
    Assert(postRotation[0] + postRotation[1] + postRotation[2] == 0);

    FbxDouble3 scalingOffset = Node->ScalingOffset.Get();
    Assert(scalingOffset[0] + scalingOffset[1] + scalingOffset[2] == 0);

    FbxDouble3 scalingPivot = Node->ScalingPivot.Get();
    Assert(scalingPivot[0] + scalingPivot[1] + scalingPivot[2] == 0);

    FbxDouble3 geometricTranslation = Node->GeometricTranslation.Get();
    Assert(geometricTranslation[0] + geometricTranslation[1] + geometricTranslation[2] == 0);

    FbxDouble3 geometricRotation = Node->GeometricRotation.Get();
    Assert(geometricRotation[0] + geometricRotation[1] + geometricRotation[2] == 0);

    FbxDouble3 geometricScaling = Node->GeometricScaling.Get();
    Assert(geometricScaling[0] + geometricScaling[1] + geometricScaling[2] == 3);

    EFbxRotationOrder RotationOrder;
    Node->GetRotationOrder(FbxNode::eSourcePivot, RotationOrder);
    Assert(RotationOrder == FbxEuler::eOrderXYZ);

    mesh* Mesh = PushStruct(AssetArena, mesh);
    InitializeTransform(Mesh);

    SetString(&Mesh->Path, &Parent->Path);
    AppendString(&Mesh->Path, "/");
    AppendString(&Mesh->Path, (char*)Node->GetName());

    SetString(&Mesh->SerializedPath, &Mesh->Path);

    v3 GeometricTranslation = V3((float)geometricTranslation.mData[0],
                                 (float)geometricTranslation.mData[1],
                                 (float)geometricTranslation.mData[2]);
    Mesh->LocalPosition = V3((float)translation.mData[0],
                             (float)translation.mData[1],
                             (float)translation.mData[2]);

    bool32 UsePreRotation = true;
    if (UsePreRotation)
    {
        v4 PreRotation = EulerToQuaternion(V3((float)(preRotation.mData[0] * DEGREES_TO_RADIANS),
                                              (float)(preRotation.mData[1] * DEGREES_TO_RADIANS),
                                              (float)(preRotation.mData[2] * DEGREES_TO_RADIANS)));
        v4 Rotation = EulerToQuaternion(V3((float)(rotation.mData[0] * DEGREES_TO_RADIANS),
                                           (float)(rotation.mData[1] * DEGREES_TO_RADIANS),
                                           (float)(rotation.mData[2] * DEGREES_TO_RADIANS)));
        Mesh->LocalRotation = CombineQuaternions(PreRotation, Rotation);
    }
    else
    {
        Mesh->LocalRotation = EulerToQuaternion(V3((float)(rotation.mData[0] * DEGREES_TO_RADIANS),
                                                   (float)(rotation.mData[1] * DEGREES_TO_RADIANS),
                                                   (float)(rotation.mData[2] * DEGREES_TO_RADIANS)));
    }

    Mesh->LocalScale = V3((float)AbsoluteValue(scaling.mData[0]),
                          (float)AbsoluteValue(scaling.mData[1]),
                          (float)AbsoluteValue(scaling.mData[2]));

    SetTransformParent(Mesh, Parent);

    if (LengthSq(GeometricTranslation) > 0)
    {
        Mesh->LocalPosition += GeometricTranslation;
        SetDirty(Mesh);
        ResolveLocalToWorld(Mesh);
    }

    for (int i = 0; i < Node->GetNodeAttributeCount(); i++)
    {
        FbxNodeAttribute* Attribute = Node->GetNodeAttributeByIndex(i);
        if (Attribute->GetAttributeType() == FbxNodeAttribute::eMesh)
        {
            bool32 FlipWindingOrder = (scaling.mData[0] < 0 || scaling.mData[1] < 0 || scaling.mData[2] < 0);
            LoadMesh(Node->GetMesh(), FlipWindingOrder, Parent, AssetArena, Mesh, PlatformAPI);

            //
            // Load Material
            //
            int MaterialCount = Node->GetMaterialCount();
            if (MaterialCount == 1)
            {

                material* YamgineMaterial = PushStruct(AssetArena, material);

                FbxSurfaceMaterial* SurfaceMaterial = Node->GetMaterial(0);

                // Get Textures
                FbxProperty Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);
                int32 TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sDiffuseFactor);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sEmissive);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sEmissiveFactor);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sAmbient);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sAmbientFactor);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sSpecular);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                Property = SurfaceMaterial->FindProperty(FbxSurfaceMaterial::sSpecularFactor);
                TextureCount = Property.GetSrcObjectCount<FbxLayeredTexture>();
                if (TextureCount > 0)
                {
                    Assert(0);
                }

                // Get Shading Values
                if (SurfaceMaterial->GetClassId().Is(FbxSurfaceLambert::ClassId))
                {
                    FbxSurfaceLambert* Lambert = (FbxSurfaceLambert*)SurfaceMaterial;
                    FbxDouble3 Diffuse = Lambert->Diffuse;
                    YamgineMaterial->Color = V4(Diffuse[0], Diffuse[1], Diffuse[2], 1);
                }
                else if (SurfaceMaterial->GetClassId().Is(FbxSurfacePhong::ClassId))
                {
                    Assert(0);
                }
                else
                {
                    Assert(0);
                }

                Mesh->ImportedMaterial = YamgineMaterial;
            }
        }
        //...
        //load other properties here
    }

    //recurse
    for (int j = 0; j < Node->GetChildCount(); j++)
    {
        LoadNode(Node->GetChild(j), Mesh, AssetArena, PlatformAPI);
    }
}

//TODO(james): should I use this?
void NodePrep(FbxNode* Node)
{
    // Do this setup for each node (FbxNode).
    // We set up what we want to bake via ConvertPivotAnimationRecursive.
    // When the destination is set to 0, baking will occur.
    // When the destination value is set to the source�s value, the source values will be retained and not baked.
    FbxVector4 lZero(0, 0, 0);

    // Activate pivot converting
    Node->SetPivotState(FbxNode::eSourcePivot, FbxNode::ePivotActive);
    Node->SetPivotState(FbxNode::eDestinationPivot, FbxNode::ePivotActive);

    // We want to set all these to 0 and bake them into the transforms.
    Node->SetPostRotation(FbxNode::eDestinationPivot, lZero);
    Node->SetPreRotation(FbxNode::eDestinationPivot, lZero);
    Node->SetRotationOffset(FbxNode::eDestinationPivot, lZero);
    Node->SetScalingOffset(FbxNode::eDestinationPivot, lZero);
    Node->SetRotationPivot(FbxNode::eDestinationPivot, lZero);
    Node->SetScalingPivot(FbxNode::eDestinationPivot, lZero);

    Node->SetRotationOrder(FbxNode::eDestinationPivot, FbxEuler::eOrderXYZ);

    // Similarly, this is the case where geometric transforms are supported by the system.
    // If geometric transforms are not supported, set them to zero instead of
    // the source�s geometric transforms.
    // Geometric transform = local transform, not inherited by children.
    Node->SetGeometricTranslation(FbxNode::eDestinationPivot, Node->GetGeometricTranslation(FbxNode::eSourcePivot));
    Node->SetGeometricRotation(FbxNode::eDestinationPivot, Node->GetGeometricRotation(FbxNode::eSourcePivot));
    Node->SetGeometricScaling(FbxNode::eDestinationPivot, Node->GetGeometricScaling(FbxNode::eSourcePivot));

    // Idem for quaternions.
    //Node->SetUseQuaternionForInterpolation(FbxNode::eDestinationPivot, Node->GetUseQuaternionForInterpolation(FbxNode::eSourcePivot));

    //recurse
    for (int j = 0; j < Node->GetChildCount(); j++)
    {
        NodePrep(Node->GetChild(j));
    }
}

void LoadModel(FbxManager* MyFbxManager, mesh* Model, char* FilePath,
               float ImportScaling, // NOTE(james): fbx default unit is centimeters, so we will have to do 0.01f usually
               memory_arena* AssetArena,
               platform_api* PlatformAPI)
{
    // Create the IO settings object.
    FbxIOSettings* ios = FbxIOSettings::Create(MyFbxManager, IOSROOT);
    MyFbxManager->SetIOSettings(ios);

    // Create an importer using the SDK manager.
    FbxImporter* Importer = FbxImporter::Create(MyFbxManager, "");

    // Use the first argument as the filename for the importer.
    if (!Importer->Initialize(FilePath, -1, MyFbxManager->GetIOSettings()))
    {
        Assert(0);
    }

    // Create a new scene so that it can be populated by the imported file.
    FbxScene* Scene = FbxScene::Create(MyFbxManager, "myScene");

    // Import the contents of the file into the scene.
    Importer->Import(Scene);

    // The file is imported, so get rid of the importer.
    Importer->Destroy();
    /*
    // convert axis system if necessary
    FbxAxisSystem SceneAxisSystem = Scene->GetGlobalSettings().GetAxisSystem();
    // (UpVector = +Y, FrontVector = +Z, CoordSystem = +X (RightHanded)) 
    if (SceneAxisSystem != FbxAxisSystem::OpenGL) {
        FbxAxisSystem::OpenGL.ConvertScene(Scene);
    }
    */
    /*
    FbxSystemUnit SceneSystemUnit = Scene->GetGlobalSettings().GetSystemUnit();
    if (SceneSystemUnit.GetScaleFactor() != 1.0) {
        //The unit in this example is centimeter.
        FbxSystemUnit::cm.ConvertScene(Scene);
    }
    */

    //Get rid of any quads
    FbxGeometryConverter Converter(MyFbxManager);
    Assert(Converter.Triangulate(Scene, true));

    // Print the nodes of the scene and their attributes recursively.
    // Note that we are not printing the root node because it should
    // not contain any attributes.
    FbxNode* RootNode = Scene->GetRootNode();
    //mesh_info MeshInfo[];
    if (RootNode)
    {
        /*
        for(int i = 0; i < RootNode->GetChildCount(); i++)
        {
            NodePrep(RootNode->GetChild(i));
        }
        Scene->GetRootNode()->ConvertPivotAnimationRecursive(FbxNode::eDestinationPivot );
        */
        FbxDouble3 translation = RootNode->LclTranslation.Get();
        FbxDouble3 rotation = RootNode->LclRotation.Get();
        FbxDouble3 scaling = RootNode->LclScaling.Get();

        STRING(Filename, 32);
        ExtractFileName(&Filename, FilePath);

        InitializeTransform(Model);

        AppendString(&Model->Path, "/");
        AppendString(&Model->Path, Filename.Text);

        SetString(&Model->SerializedPath, &Model->Path);

        TransformTRSLocal(Model, V3((float)translation.mData[0], (float)translation.mData[1], (float)translation.mData[2]),
                          EulerToQuaternion(V3((float)rotation.mData[0] * DEGREES_TO_RADIANS, (float)rotation.mData[1] * DEGREES_TO_RADIANS, (float)rotation.mData[2] * DEGREES_TO_RADIANS)),
                          V3((float)scaling.mData[0], (float)scaling.mData[1], (float)scaling.mData[2]) * ImportScaling);

        for (int i = 0; i < RootNode->GetChildCount(); i++)
        {
            LoadNode(RootNode->GetChild(i), Model, AssetArena, PlatformAPI);
        }
    }
}