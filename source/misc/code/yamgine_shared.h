#if !defined(YAMGINE_SHARED_H)

#include "platform_agnostic.h"

#include "yamgine_transform.h"

#include "yamgine_render_list.h"


void* LoadBinaryFile(char* Filename, temporary_memory* Temp, platform_api* PlatformAPI, uint64* FileSizeOut=NULL)
{
    uint64 FileSize = PlatformAPI->GetFileSize(Filename);
    void* Data = PushSize(Temp->Arena, FileSize);
    PlatformAPI->ReadBinaryFile(Filename, Data);
    
    if (FileSizeOut != NULL)
    {
        *FileSizeOut = FileSize;
    }
    
    return Data;
}

char* LoadTextFile(char* Filename, temporary_memory* Temp, platform_api* PlatformAPI, uint64* FileSizeOut=NULL)
{
    uint64 FileSize = PlatformAPI->GetFileSize(Filename);
    void* Data = PushSize(Temp->Arena, FileSize+1);
    PlatformAPI->ReadBinaryFile(Filename, Data);
    
    if (FileSizeOut != NULL)
    {
        *FileSizeOut = FileSize+1;
    }
    
    char* Result = (char*)Data;
    Result[FileSize] = '\0';
    
    return Result;
}

rectangle2 TextureRect(texture* Texture)
{
    rectangle2 Result = RectMinDim(v2_ZERO, V2(Texture->Width, Texture->Height));
    return Result;
}

struct dumb_list
{
    void** Data;
    uint32 Max;
    uint32 Count;
};

dumb_list CreateList(uint32 Max, memory_arena* Arena)
{
    dumb_list List = {};
    List.Max = Max;
    List.Count = 0;
    List.Data = PushArray(Arena, List.Max, void*);
    return List;
}

void AddToList(dumb_list* DumbList, void* Entry)
{
    Assert(DumbList->Count < DumbList->Max);
    DumbList->Data[DumbList->Count++] = Entry;
}

void* GetFromList(dumb_list* DumbList, uint32 Index)
{
    Assert(Index < DumbList->Count);
    void* Result = DumbList->Data[Index];
    return Result;
}


#define CREATE_LIST_D(type, name, count) \
int max_##name = count; \
int count_##name = 0;\
void* array_##name[count] = {};\
type ** name = (type**)&array_##name[0];

#define PUSH_LIST_D(name, ptr)\
name[count_##name++] = ptr; \
Assert(count_##name < max_##name)

#define YAMGINE_SHARED_H
#endif