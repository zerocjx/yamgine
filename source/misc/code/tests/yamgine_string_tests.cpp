#include "../platform_agnostic.h"

#include "../yamgine_string.h"

#define TestAssert(Expression) \
    if (!(Expression))         \
    printf("String problem: %s, %d\n", __FILE__, __LINE__)

void StringTests()
{
    //SetString
    {
        //okay with extra mem at the end
        STRING(Hello, 10);
        SetString(&Hello, "Hello");
        TestAssert(StringsAreEqualAbsolute(Hello, "Hello"));
        TestAssert(StringsAreEqualUnion(Hello, "Hello"));
        TestAssert(StringLength(Hello) == 5);

        //okay with exact size
        STRING(Hello2, 6);
        SetString(&Hello2, "Hello2");
        TestAssert(StringsAreEqualAbsolute(Hello2, "Hello2"));
        TestAssert(StringsAreEqualUnion(Hello2, "Hello2"));
        TestAssert(StringLength(Hello2) == 6);

        //one char ok
        STRING(T, 1);

        // TODO(james): this kind of checking for asserts is tricky, because the asserts in the code
        // are often there to prevent memory access and writing violations, like in the following example. How do we
        // arbitrarily stop the execution of the function at the point of the assert? goto?
        //SetString(&T, "Overflow!");
        //CheckAssertOccurred();

        SetString(&T, "T");
        TestAssert(StringLength(T) == 1);

        //make sure these fail
        TestAssert(!StringsAreEqualAbsolute(Hello, T.Text));
        TestAssert(!StringsAreEqualUnion(Hello, T.Text));
    }

    // appends
    {
        STRING(Appender, 25);
        AppendString(&Appender, "Test");
        TestAssert(StringsAreEqualAbsolute(Appender, "Test"));

        AppendString(&Appender, "Test");
        TestAssert(StringsAreEqualAbsolute(Appender, "TestTest"));

        AppendSubstring(&Appender, "Test", 1);
        TestAssert(StringsAreEqualAbsolute(Appender, "TestTestT"));

        STRING(ToAppend, 10);
        SetString(&ToAppend, "Appended");
        AppendString(&Appender, ToAppend);
        TestAssert(StringsAreEqualAbsolute(Appender, "TestTestTAppended"));
    }
}