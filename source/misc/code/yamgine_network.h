//Port debugging tips - make sure app has permissions through Windows firewall. Use Port Checker app to test client and server heartbeats

struct network_state
{
#ifdef YAMGINE_CLIENT
    network_socket ClientSendSocket;
    network_socket ClientRecieveSocket;
#endif

#if YAMGINE_SERVER
    network_socket ServerSendSocket;
    network_socket ServerRecieveSocket;
    char ClientIP[16];
    uint16 ClientPort;
#endif

    real32 LastHeartbeatSendTime;
    real32 LastHeartbeatRecieveTime;

    bool32 HasConnection;
};
