#if !defined(YAMGINE_ENTITY_H)

#include "yamgine_render_list.h"

//TODO(james): macro or meta-programming solution for enum-to-string?
meta_string_enum enum collision_layer
{
    CollisionLayer_Default,
    CollisionLayer_Editor,
    CollisionLayer_PlayerBullet,
    CollisionLayer_Player,
    CollisionLayer_Enemy,
    CollisionLayer_EnemyBullet,
    CollisionLayer_BulletKillZone,

    CollisionLayer_Count
};

meta_string_enum enum collider_type
{
    ColliderType_None,
    ColliderType_Mesh,
    ColliderType_Ray,
    ColliderType_Sphere,
    ColliderType_Capsule,
    ColliderType_Cylinder,
    ColliderType_Quad,
    ColliderType_Frustrum,

    ColliderType_count
};

struct entity;
struct contact_info
{
    entity* A;
    entity* B;

    v3 PointWorldA;
    v3 PointWorldB;
    v3 PointLocalA;
    v3 PointLocalB;

    v3 NormalA; //points from Me to Other
    v3 NormalB; //points from Me to Other

    //v3 TangentA;
    //v3 TangentB;

    real32 PenetrationDepth;

    bool32 IsTriggerContact;
};

#define MAX_COLLISION_PAIRS 8
struct all_contacts
{
    contact_info Contacts[MAX_COLLISION_PAIRS];
    uint32 Count;
};

struct linked_list_member;

//rb rigidbody
struct rb_config : transform
{
    v3 Force;
    v3 Torque;

    //primary
    v3 AngularMomentum;

    //secondary
    v3 LinearVelocity;
    v3 AngularVelocity;

    matrix3 InverseWorldInertiaTensor;

    contact_info Contact;

    bool32 IsResting;
};

meta_string_enum enum entity_type
{
    EntityType_Default,

    EntityType_Bullet,
    EntityType_BulletKillZone,

    EntityType_EnemyShooter,
    EntityType_EnemyBullet,

    EntityType_EnemyChaser,

    EntityType_GameWinZone,
    EntityType_PlayerKillZone,

    EntityType_count
};

struct game_state;
#define ON_TRIGGER_HIT(name) void name(game_state* GameState, entity* Entity, contact_info Contact)
typedef ON_TRIGGER_HIT(on_trigger_hit);

#define ON_COLLISION_HIT(name) void name(game_state* GameState, entity* Entity, contact_info Contact)
typedef ON_COLLISION_HIT(on_collision_hit);

struct collision_group;
#define PHYSICS_UPDATE(name) void name(collision_group* CollisionGroup, game_state* GameState, entity* Entity)
typedef PHYSICS_UPDATE(physics_update);

introspect(category
           : "entity") struct entity : transform
{
    //
    // entity system
    //

    serialize("param") bool32 SerializeToFile = true; //TODO(james): does this actually have to be serialized?
    bool32 ShowInSceneGraph = true;

    linked_list_member* EntityListMember;

    bool32 EditorOnly = false; //stuff like handles and gizmos

    serialize("enum") entity_type EntityType = EntityType_Default;

    serialize("enum") bool32 Enabled = true;
    serialize("enum") bool32 DeletionPending = false;

    //
    // physics/collision
    //

    //collider
    linked_list_member* CollisionGroupMember = NULL;
    serialize("enum") collider_type ColliderType = ColliderType_None;

    serialize("param") bool32 IsCharacterController = false;

    serialize("enum") collision_layer CollisionLayer = CollisionLayer_Default;
    aabb ColliderAABB = {};

    serialize("param") char __ColliderMeshPathMemory[128] = {};
    string ColliderMeshPath = { __ColliderMeshPathMemory, 128 };

    mesh* ColliderMesh = {};
    v3* WorldspaceVertices;

    matrix4 ColliderMeshInstanceSpace = {};
    serialize("param") real32 SphereColliderRadius = 0;

    serialize("param") real32 CapsuleColliderRadius = 0;
    serialize("param") real32 CylinderColliderRadius = 0;

    serialize("param") real32 CapsuleColliderHeight = 0;
    serialize("param") real32 CylinderColliderHeight = 0;

    rb_config Configs[2];
    uint32 SourceConfig;
    uint32 TargetConfig;

    v3 LastCollisionStepPosition;
    real32 LastCollisionTime;

    serialize("param") real32 Mass = 0;
    serialize("param") bool32 IsTrigger = false;
    serialize("param") bool32 UseGravity = false;
    serialize("param") bool32 IsStatic = false;
    serialize("param") bool32 LockRotation = false;
    serialize("param") real32 CoefficientOfRestitution = 0;
    serialize("param") real32 XZFriction = 0;

    bool32 CollidedThisFrame = false;

    matrix3 InverseBodyInertiaTensor = {};

    on_trigger_hit* OnTriggerHit = NULL;
    on_collision_hit* OnCollisionHit = NULL;
    physics_update* PhysicsUpdate = NULL;

    //
    //rendering
    //
    mesh* RenderMesh = NULL;

    serialize("param") char __RenderMeshPathMemory[128] = {};
    string RenderMeshPath = { __RenderMeshPathMemory, 128 };

    serialize("param") material Material = {};
    aabb RenderAABB = {};

    //
    // gui
    //
    // NOTE(james): Rect position is the XY of transform,
    // Size is the bounds of the rect, while Pivot determines
    // how the bounds relate to the position.
    // So a pivot of (0.5,0.5) would mean the position is at the center
    // of the rect, (0, 0) would mean the position is at the bottom-left
    serialize("param") v2 Size;
    serialize("param") v2 Pivot;

    //
    //directional light
    //
    serialize("param") bool32 IsDirectionalLight = false;

    //
    //other
    //
    real32 CreationTime = 0;
    entity* Camera = NULL; //child camera on players

    serialize("param") real32 PlayerRotationX = 0;
    serialize("param") real32 PlayerRotationY = 0;

    real32 LastShootTime; //for player and enemy shooters

    serialize("param") bool32 IsPlayerGun;

    real32 LastJumpTime;

#if FMOD_SUPPORT
    FMOD_STUDIO_EVENTINSTANCE* ShootInstance;
    FMOD_STUDIO_EVENTINSTANCE* DeathInstance;
#endif
    real32 LastHitTime;
};

void InitializeEntity(entity* Entity, bool ClearLinkedList = true)
{
    //
    // entity system
    //
    InitializeTransform(Entity);
    Entity->SerializeToFile = true; //TODO(james): does this actually have to be serialized?
    Entity->ShowInSceneGraph = true;

    if (ClearLinkedList)
    {
        Entity->EntityListMember = NULL;
    }

    Entity->EditorOnly = false; //stuff like handles and gizmos

    Entity->EntityType = EntityType_Default;

    Entity->Enabled = true;
    Entity->DeletionPending = false;

    //
    // physics/collision
    //

    //collider
    Entity->CollisionGroupMember = NULL;
    Entity->ColliderType = ColliderType_None;

    Entity->IsCharacterController = false;

    Entity->CollisionLayer = CollisionLayer_Default;
    Entity->ColliderAABB = {};

    //TODO(james): memory problem on release builds?
    //ZeroArray(128, Entity->__ColliderMeshPathMemory);
    Entity->ColliderMeshPath = { Entity->__ColliderMeshPathMemory, 128 };

    Entity->ColliderMesh = {};
    Entity->ColliderMeshInstanceSpace = {};
    Entity->SphereColliderRadius = 0;

    Entity->CapsuleColliderRadius = 0;
    Entity->CylinderColliderRadius = 0;

    Entity->CapsuleColliderHeight = 0;
    Entity->CylinderColliderHeight = 0;

    ZeroArray(2, Entity->Configs);
    Entity->SourceConfig = 0;
    Entity->TargetConfig = 1;

    Entity->LastCollisionStepPosition = v3_ZERO;

    Entity->Mass = 0;
    Entity->IsTrigger = false;
    Entity->UseGravity = false;
    Entity->IsStatic = false;
    Entity->LockRotation = false;
    Entity->CoefficientOfRestitution = 0;

    Entity->CollidedThisFrame = false;

    Entity->InverseBodyInertiaTensor = {};

    Entity->OnTriggerHit = NULL;
    Entity->OnCollisionHit = NULL;

    //
    //rendering
    //
    Entity->RenderMesh = NULL;

    ZeroArray(128, Entity->__RenderMeshPathMemory);
    Entity->RenderMeshPath = { Entity->__RenderMeshPathMemory, 128 };

    Entity->Material = { { 1, 0, 1, 1 }, NULL, false, true, true };
    Entity->RenderAABB = {};
    //Entity->TargetRenderLayer = TargetRenderLayer_default;

    //
    //directional light
    //
    Entity->IsDirectionalLight = false;

    //
    //other
    //
    Entity->CreationTime = 0;
    Entity->Camera = NULL; //child camera on players

    Entity->PlayerRotationX = 0;
    Entity->PlayerRotationY = 0;
}

#define YAMGINE_ENTITY_H
#endif