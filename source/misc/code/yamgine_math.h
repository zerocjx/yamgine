#if !defined(HANDMADE_MATH_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */
#include "yamgine_intrinsics.h"

#define PI 3.1415926
#define DEGREES_TO_RADIANS PI / 180
#define RADIANS_TO_DEGREES 180 / PI

/*
   Coordinate Systems is Right-Handed,
   X+ to Left,
   Y+ Up, 
   Z+, out of screen
   
*/

struct rectangle3
{
    v3 Min;
    v3 Max;
};

struct plane
{
    real32 A;
    real32 B;
    real32 C;
    real32 D;
};

//
// Constructors
//

inline v2
V2i(int32 X, int32 Y)
{
    v2 Result = { (real32)X, (real32)Y };

    return (Result);
}

inline v2
V2i(uint32 X, uint32 Y)
{
    v2 Result = { (real32)X, (real32)Y };

    return (Result);
}

inline v2
V2(real32 X, real32 Y)
{
    v2 Result;

    Result.x = X;
    Result.y = Y;

    return (Result);
}

#define v2_ZERO V2(0, 0)
#define v2_ONE V2(1, 1)
#define v2_UP V2(0, 1)
#define v2_RIGHT V2(1, 0)

inline v3
V3(real32 X, real32 Y, real32 Z)
{
    v3 Result;

    Result.x = X;
    Result.y = Y;
    Result.z = Z;

    return (Result);
}

#define v3_ONE V3(1, 1, 1)
#define v3_ZERO V3(0, 0, 0)
#define v3_UP V3(0, 1, 0)
#define v3_DOWN V3(0, -1, 0)
#define v3_FORWARD V3(0, 0, 1)
#define v3_BACK V3(0, 0, -1)
#define v3_LEFT V3(-1, 0, 0)
#define v3_RIGHT V3(1, 0, 0)
#define v3_MAX V3(FLT_MAX, FLT_MAX, FLT_MAX)

inline v3
V3(v2 XY, real32 Z)
{
    v3 Result;

    Result.x = XY.x;
    Result.y = XY.y;
    Result.z = Z;

    return (Result);
}

inline v3
ToV3(v2 XY, real32 Z)
{
    v3 Result;

    Result = V3(XY, Z);

    return Result;
}

inline v3
V3i(int32 X, int32 Y, int32 Z)
{
    v3 Result;

    Result = { (real32)X, (real32)Y, (real32)Z };

    return Result;
}

inline v3
V3i(uint32 X, uint32 Y, uint32 Z)
{
    v3 Result;

    Result = { (real32)X, (real32)Y, (real32)Z };

    return Result;
}

inline v4
V4(real32 X, real32 Y, real32 Z, real32 W)
{
    v4 Result;

    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    Result.w = W;

    return (Result);
}

#define v4_ZERO V4(0, 0, 0, 0)

//colors
#define v4_WHITE V4(1, 1, 1, 1)
#define v4_GREY V4(0.5f, 0.5f, 0.5f, 1)
#define v4_BLACK V4(0, 0, 0, 1)
#define v4_RED V4(1, 0, 0, 1)
#define v4_GREEN V4(0, 1, 0, 1)
#define v4_BLUE V4(0, 0, 1, 1)
#define v4_MAGENTA V4(1, 0, 1, 1)
#define v4_ORANGE V4(1, 0.27f, 0, 1)
#define v4_CYAN V4(0, 1, 1, 1)
#define v4_YELLOW V4(1, 1, 0, 1)

#define v4_TRANSPARENT V4(1, 1, 1, 0)

#define v4_ZERO_ROTATION V4(0, 0, 0, 1)

inline v4
ToV4(v3 XYZ, float w)
{
    v4 Result;

    Result.x = XYZ.x;
    Result.y = XYZ.y;
    Result.z = XYZ.z;
    Result.w = w;

    return Result;
}

//
// NOTE(casey): Scalar operations
//

inline real32
Square(real32 A)
{
    real32 Result = A * A;

    return (Result);
}

inline real32
Lerp(real32 A, real32 B, real32 t)
{
    real32 Result = (1.0f - t) * A + t * B;

    return (Result);
}

inline real32
PingPong(real32 Magnitude, real32 T) //T would be something like ElapsedTime
{
    real32 Result = Magnitude - AbsoluteValue(fmod(T, (2 * Magnitude)) - Magnitude);
    return Result;
}

inline real32
InverseLerp(real32 A, real32 B, real32 value)
{
    real32 Result = 0;

    Result = (value - A) / (B - A);
    if (B < A)
    {
        if (value > A)
            Result = 1.0f;
        else if (value < B)
            Result = 0.0f;
    }
    else
    {
        if (value > B)
            Result = 1.0f;
        else if (value < A)
            Result = 0.0f;
    }

    return Result;
}

//TODO(james): intrinsic for
bool32 Is01(float A)
{
    bool32 Result = false;
    if (A >= 0 && A <= 1)
    {
        Result = true;
    }
    return Result;
}

real32 Map(real32 In, real32 InMin, real32 InMax, real32 OutMin, real32 OutMax)
{
    real32 Result;
    real32 In01 = InverseLerp(InMin, InMax, In);
    Result = Lerp(OutMin, OutMax, In01);

    return Result;
}

inline real32
Clamp(real32 Min, real32 Value, real32 Max)
{
    real32 Result = Value;

    if (Result < Min)
    {
        Result = Min;
    }
    else if (Result > Max)
    {
        Result = Max;
    }

    return (Result);
}

inline real32
Clamp01(real32 Value)
{
    real32 Result = Clamp(0.0f, Value, 1.0f);

    return (Result);
}

inline real32
SafeRatioN(real32 Numerator, real32 Divisor, real32 N)
{
    real32 Result = N;

    if (Divisor != 0.0f)
    {
        Result = Numerator / Divisor;
    }

    return (Result);
}

inline real32
SafeRatio0(real32 Numerator, real32 Divisor)
{
    real32 Result = SafeRatioN(Numerator, Divisor, 0.0f);

    return (Result);
}

inline real32
SafeRatio1(real32 Numerator, real32 Divisor)
{
    real32 Result = SafeRatioN(Numerator, Divisor, 1.0f);

    return (Result);
}

inline bool32
SolveQuadratic(real32 A, real32 B, real32 C, real32* X0, real32* X1)
{
    real32 Solvable = true;
    real32 Discr = B * B - 4 * A * C;
    if (Discr < 0)
    {
        Solvable = false;
    }
    else if (Discr == 0)
    {
        *X0 = *X1 = -0.5 * B / A;
    }
    else
    {
        float Q = 0;
        if (B > 0)
        {
            Q = -0.5 * (B + sqrt(Discr));
        }
        else
        {
            Q = -0.5 * (B - sqrt(Discr));
        }
        *X0 = Q / A;
        *X1 = C / Q;
    }

    return Solvable;
}

// NOTE(james): maybe make this variadic
inline real32
Max(real32 A, real32 B)
{
    real32 Result = B;
    if (A > B)
    {
        Result = A;
    }
    return Result;
}

// NOTE(james): maybe make this variadic
inline real32
Min(real32 A, real32 B)
{
    real32 Result = B;
    if (A < B)
    {
        Result = A;
    }
    return Result;
}

inline real32
Min3(real32 A, real32 B, real32 C)
{
    real32 Result = FLT_MAX;
    if (A < Result)
    {
        Result = A;
    }
    if (B < Result)
    {
        Result = B;
    }
    if (C < Result)
    {
        Result = C;
    }
    return Result;
}

// NOTE(james): absolute tolerance test, not good for when the range of numbers is not known in advance,
// large numbers will not handle a small epsilon well. Look up relative tolerance tests
inline bool32
ApproximatelyEqual(real32 A, real32 B, real32 Epsilon = FLT_EPSILON)
{
    bool32 Result = false;

    if (AbsoluteValue(A - B) <= Epsilon)
    {
        Result = true;
    }

    return Result;
}

//
// NOTE(casey): v2 operations
//

inline v2
Perp(v2 A)
{
    v2 Result = { -A.y, A.x };
    return (Result);
}

inline v2
operator*(real32 A, v2 B)
{
    v2 Result;

    Result.x = A * B.x;
    Result.y = A * B.y;

    return (Result);
}

inline v2
operator*(v2 B, real32 A)
{
    v2 Result = A * B;

    return (Result);
}

inline v2&
operator*=(v2& B, real32 A)
{
    B = A * B;

    return (B);
}

inline v2
operator-(v2 A)
{
    v2 Result;

    Result.x = -A.x;
    Result.y = -A.y;

    return (Result);
}

inline v2
operator+(v2 A, v2 B)
{
    v2 Result;

    Result.x = A.x + B.x;
    Result.y = A.y + B.y;

    return (Result);
}

inline bool
operator==(v2 A, v2 B)
{
    bool Result = false;
    if (A.x == B.x && A.y == B.y)
    {
        Result = true;
    }
    return (Result);
}

inline bool
operator!=(v2 A, v2 B)
{
    bool Result = false;
    Result = !(A == B);
    return (Result);
}

inline v2&
operator+=(v2& A, v2 B)
{
    A = A + B;

    return (A);
}

inline v2
operator-(v2 A, v2 B)
{
    v2 Result;

    Result.x = A.x - B.x;
    Result.y = A.y - B.y;

    return (Result);
}

inline v2&
operator-=(v2& A, v2 B)
{
    A = A - B;

    return (A);
}

inline v2
Lerp(v2 A, v2 B, real32 t)
{
    v2 Result = (1.0f - t) * A + t * B;

    return (Result);
}

inline v2
Hadamard(v2 A, v2 B)
{
    v2 Result = { A.x * B.x, A.y * B.y };

    return (Result);
}

inline real32
Inner(v2 A, v2 B)
{
    real32 Result = A.x * B.x + A.y * B.y;

    return (Result);
}

inline real32
LengthSq(v2 A)
{
    real32 Result = Inner(A, A);

    return (Result);
}

inline real32
Length(v2 A)
{
    real32 Result = SquareRoot(LengthSq(A));
    return (Result);
}

inline v2 Normalize(v2 A)
{
    v2 Result = V2(0, 0);
    if (LengthSq(A) > 0)
    {
        Result = A * (1 / Length(A));
    }

    return Result;
}

inline v2
Clamp01(v2 Value)
{
    v2 Result;

    Result.x = Clamp01(Value.x);
    Result.y = Clamp01(Value.y);

    return (Result);
}

//produces values from 0 to pi
inline float
AngleHalfCircleUnsigned(v2 A, v2 B)
{
    float Result = 0;
    float Top = Inner(A, B);
    float Bottom = Length(A) * Length(B);
    Result = ACos(Top / Bottom);
    return Result;
}

//TODO(james):finish this
//produces values from -pi to pi
inline float
AngleHalfCircleSigned(v2 A, v2 B)
{
    float Result = 0;
    float Top = Inner(A, B);
    float Bottom = Length(A) * Length(B);
    Result = ACos(Top / Bottom);
    return Result;
}

//produces values 0 to 2pi
inline float
AngleFullCircle(v2 A, v2 B)
{
    real32 Result = 0;
    real32 Top = Inner(A, B);
    real32 Bottom = Length(A) * Length(B);
    Result = ACos(Top / Bottom);
    //TODO(james):switch this to something more robust, whats the vector operation for determining if its left or right?
    if (B.y < A.y)
    {
        Result = ((real32)PI * 2) - Result;
    }
    return Result;
}

//
// NOTE(casey): v3 operations
//

inline v3
operator*(real32 A, v3 B)
{
    v3 Result;

    Result.x = A * B.x;
    Result.y = A * B.y;
    Result.z = A * B.z;

    return (Result);
}

inline v3
operator*(v3 B, real32 A)
{
    v3 Result = A * B;

    return (Result);
}

inline v3&
operator*=(v3& B, real32 A)
{
    B = A * B;

    return (B);
}

inline v3
operator-(v3 A)
{
    v3 Result;

    Result.x = -A.x;
    Result.y = -A.y;
    Result.z = -A.z;

    return (Result);
}

inline v3
operator+(v3 A, v3 B)
{
    v3 Result;

    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;

    return (Result);
}

inline v3&
operator+=(v3& A, v3 B)
{
    A = A + B;

    return (A);
}

inline v3
operator-(v3 A, v3 B)
{
    v3 Result;

    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;

    return (Result);
}

inline v3&
operator-=(v3& A, v3 B)
{
    A = A - B;

    return A;
}

inline bool
operator==(v3 A, v3 B)
{
    bool Result = false;
    if (A.x == B.x && A.y == B.y && A.z == B.z)
    {
        Result = true;
    }
    return Result;
}

inline bool
operator!=(v3 A, v3 B)
{
    bool Result = true;
    if (A == B)
    {
        Result = false;
    }
    return Result;
}

bool32 ApproximatelyEqualV3(v3 A, v3 B)
{
    bool32 Result = false;
    if (ApproximatelyEqual(A.x, B.x) && ApproximatelyEqual(A.y, B.y) && ApproximatelyEqual(A.z, B.z))
    {
        Result = true;
    }
    return Result;
}

inline v3
Hadamard(v3 A, v3 B)
{
    v3 Result = { A.x * B.x, A.y * B.y, A.z * B.z };

    return (Result);
}

//NOTE(james): also known as dot product
inline real32
Inner(v3 A, v3 B)
{
    real32 Result = A.x * B.x + A.y * B.y + A.z * B.z;

    return (Result);
}

inline v3 Cross(v3 A, v3 B)
{
    v3 Result;

    Result.x = A.y * B.z - B.y * A.z;
    Result.y = A.z * B.x - B.z * A.x;
    Result.z = A.x * B.y - B.x * A.y;

    return Result;
}

v3 TripleCrossProduct(v3 A, v3 B, v3 C)
{
    v3 Result;
    // expanded version of above formula
    // perform a.dot(c)
    real32 AC = A.x * C.x + A.y * C.y + A.z * C.z;
    // perform B.dot(C)
    real32 BC = B.x * C.x + B.y * C.y + B.z * C.z;
    // perform B * A.dot(C) - A * B.dot(C)
    Result.x = B.x * AC - A.x * BC;
    Result.y = B.y * AC - A.y * BC;
    Result.z = B.z * AC - A.z * BC;
    return Result;
}

inline v3
Lerp(v3 A, v3 B, real32 t)
{
    v3 Result = (1.0f - t) * A + t * B;

    return (Result);
}

inline v3
Reflect(v3 Direction, v3 Normal)
{
    v3 Result;

    Result = Direction - (2 * Normal * (Inner(Direction, Normal)));

    return Result;
}

inline real32
LengthSq(v3 A)
{
    real32 Result = Inner(A, A);

    return (Result);
}

inline real32
Length(v3 A)
{
    real32 Result = SquareRoot(LengthSq(A));
    return (Result);
}

inline real32
Angle(v3 A, v3 B)
{
    real32 Result = 0;
    real32 Top = Inner(A, B);
    real32 LengthA = Length(A);
    real32 LengthB = Length(B);
    real32 Bottom = LengthA * LengthB;
    Assert(Bottom != 0);
    Result = ACos(Top / Bottom);
    return Result;
}

inline v3 Normalize(v3 A)
{
    v3 Result = V3(0, 0, 0);
    if (LengthSq(A) > 0)
    {
        Result = A * (1 / Length(A));
    }

    return Result;
}

//https://keithmaggio.wordpress.com/2011/02/15/math-magician-lerp-slerp-and-nlerp/
v3 Slerp(v3 start, v3 end, float percent)
{
    // Dot product - the cosine of the angle between 2 vectors.
    float dot = Inner(start, end);
    // Clamp it to be in the range of Acos()
    // This may be unnecessary, but floating point
    // precision can be a fickle mistress.
    Clamp(dot, -1.0f, 1.0f);
    // Acos(dot) returns the angle between start and end,
    // And multiplying that by percent returns the angle between
    // start and the final result.
    float theta = ACos(dot) * percent;
    v3 RelativeVec = Normalize(end - start * dot);
    // The final result.
    return ((start * Cos(theta)) + (RelativeVec * Sin(theta)));
}

inline v3
Clamp01(v3 Value)
{
    v3 Result;

    Result.x = Clamp01(Value.x);
    Result.y = Clamp01(Value.y);
    Result.z = Clamp01(Value.z);

    return (Result);
}

//NOTE(james): doesn't guarantee output to be same length as input
inline v3
RandomPerpendicular(v3 In)
{
    v3 Result;
    Result.x = In.z;
    Result.y = In.z;
    Result.z = -In.x - In.y;

    //NOTE(james): will only enter this if input is (-1,1,0)
    if (LengthSq(Result) == 0)
    {
        Result.x = -In.y - In.z;
        Result.y = In.x;
        Result.z = -In.x;
    }
    return Result;
}

inline real32
Distance(v3 A, v3 B)
{
    real32 Result;

    Result = Length(A - B);

    return Result;
}

inline v3
ProjectDirectionOnDirection(v3 A, v3 B)
{
    v3 Result;

    v3 NormA = Normalize(A);
    v3 NormB = Normalize(B);
    real32 InnerProduct = Inner(NormA, NormB);
    Result = NormB * (Length(A) * InnerProduct);

    return Result;
}

//
// NOTE(casey): v4 operations
//

inline v4
operator*(real32 A, v4 B)
{
    v4 Result;

    Result.x = A * B.x;
    Result.y = A * B.y;
    Result.z = A * B.z;
    Result.w = A * B.w;

    return (Result);
}

inline v4
operator*(v4 B, real32 A)
{
    v4 Result = A * B;

    return (Result);
}

inline v4&
operator*=(v4& B, real32 A)
{
    B = A * B;

    return (B);
}

// NOTE(james):this is to make it negative
inline v4
operator-(v4 A)
{
    v4 Result;

    Result.x = -A.x;
    Result.y = -A.y;
    Result.z = -A.z;
    Result.w = -A.w;

    return (Result);
}

inline v4
operator+(v4 A, v4 B)
{
    v4 Result;

    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    Result.w = A.w + B.w;

    return (Result);
}

inline v4&
operator+=(v4& A, v4 B)
{
    A = A + B;

    return (A);
}

inline v4
operator+(v4 A, float B)
{
    v4 Result;

    Result.x = A.x + B;
    Result.y = A.y + B;
    Result.z = A.z + B;
    Result.w = A.w + B;
}

inline v4
operator+(float B, v4 A)
{
    v4 Result;

    Result = A + B;

    return Result;
}

inline v4&
operator+=(v4& A, float B)
{
    A = A + B;

    return (A);
}

inline v4
operator-(v4 A, v4 B)
{
    v4 Result;

    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;
    Result.w = A.w - B.w;

    return (Result);
}

inline v4&
operator-=(v4& A, v4 B)
{
    A = A - B;

    return (A);
}

inline v4
operator-(v4 A, float B)
{
    v4 Result;

    Result.x = A.x - B;
    Result.y = A.y - B;
    Result.z = A.z - B;
    Result.w = A.w - B;
}

inline v4
operator-(float B, v4 A)
{
    v4 Result;

    Result = A - B;

    return Result;
}

inline v4&
operator-=(v4& A, float B)
{
    A = A - B;

    return (A);
}

inline bool32
operator==(v4 A, v4 B)
{
    bool32 Result = false;
    if (A.x == B.x && A.y == B.y && A.z == B.z && A.w == B.w)
    {
        Result = true;
    }
    return Result;
}

inline bool32
operator!=(v4 A, v4 B)
{
    bool32 Result = true;
    if (A == B)
    {
        Result = false;
    }

    return Result;
}

inline v4
Hadamard(v4 A, v4 B)
{
    v4 Result = { A.x * B.x, A.y * B.y, A.z * B.z, A.w * B.w };

    return (Result);
}

/*
0 if // NOTE(james): perpendicular, 1 for same direction, -1 for opposite directions
*/
inline real32
Inner(v4 A, v4 B)
{
    real32 Result = A.x * B.x + A.y * B.y + A.z * B.z + A.w * B.w;

    return (Result);
}

inline real32
LengthSq(v4 A)
{
    real32 Result = Inner(A, A);

    return (Result);
}

inline real32
Length(v4 A)
{
    real32 Result = SquareRoot(LengthSq(A));
    return (Result);
}

inline v4
Clamp01(v4 Value)
{
    v4 Result;

    Result.x = Clamp01(Value.x);
    Result.y = Clamp01(Value.y);
    Result.z = Clamp01(Value.z);
    Result.w = Clamp01(Value.w);

    return (Result);
}

inline v4
Lerp(v4 A, v4 B, real32 t)
{
    v4 Result = (1.0f - t) * A + t * B;

    return (Result);
}

inline v4 Normalize(v4 A)
{
    v4 Result = {};
    if (LengthSq(A) > 0)
    {
        Result = A * (1 / Length(A));
    }

    return Result;
}

// Convert hsv floats ([0-1],[0-1],[0-1]) to rgb floats ([0-1],[0-1],[0-1]), from Foley & van Dam p593
// also http://en.wikipedia.org/wiki/HSL_and_HSV
 v3 ColorConvertHSVtoRGB(v3 HSV)
{
    float h = HSV.x;
    float s = HSV.y;
    float v = HSV.z;
    
    float out_r = 0;
    float out_g = 0;
    float out_b = 0;
    
    if (s == 0.0f)
    {
        // gray
        out_r = out_g = out_b = v;
        v3 Result = V3(out_r,out_g,out_b);
        return Result;
    }
    
    h = fmodf(h, 1.0f) / (60.0f/360.0f);
    int   i = (int)h;
    float f = h - (float)i;
    float p = v * (1.0f - s);
    float q = v * (1.0f - s * f);
    float t = v * (1.0f - s * (1.0f - f));
    
    switch (i)
    {
        case 0: out_r = v; out_g = t; out_b = p; break;
        case 1: out_r = q; out_g = v; out_b = p; break;
        case 2: out_r = p; out_g = v; out_b = t; break;
        case 3: out_r = p; out_g = q; out_b = v; break;
        case 4: out_r = t; out_g = p; out_b = v; break;
        case 5: default: out_r = v; out_g = p; out_b = q; break;
    }
    
    v3 Result = V3(out_r,out_g,out_b);
    return Result;
}

// Convert rgb floats ([0-1],[0-1],[0-1]) to hsv floats ([0-1],[0-1],[0-1]), from Foley & van Dam p592
// Optimized http://lolengine.net/blog/2013/01/13/fast-rgb-to-hsv
 v3 ColorConvertRGBtoHSV(v3 RGB)
{
    float r = RGB.x;
    float g = RGB.y;
    float b = RGB.z;
    
    float out_h;
    float out_s;
    float out_v;
    
    float K = 0.f;
    if (g < b)
    {
        const float tmp = g; g = b; b = tmp;
        K = -1.f;
    }
    if (r < g)
    {
        const float tmp = r; r = g; g = tmp;
        K = -2.f / 6.f - K;
    }
    
    const float chroma = r - (g < b ? g : b);
    out_h = fabsf(K + (g - b) / (6.f * chroma + 1e-20f));
    out_s = chroma / (r + 1e-20f);
    out_v = r;
    
    v3 Result = V3(out_h, out_s, out_v);
    return Result;
}

//
// NOTE(casey): Rectangle2
//

inline v2
GetMinCorner(rectangle2 Rect)
{
    v2 Result = Rect.Min;
    return (Result);
}

inline v2
GetMaxCorner(rectangle2 Rect)
{
    v2 Result = Rect.Max;
    return (Result);
}

inline v2
GetCenter(rectangle2 Rect)
{
    v2 Result = 0.5f * (Rect.Min + Rect.Max);
    return (Result);
}

//TODO(james):could be incorrect if max is less than min
inline v2
GetDim(rectangle2 Rect)
{
    v2 Result = Rect.Max - Rect.Min;
    //assert(Rect.Max.x > 0);
    return (Result);
}

inline rectangle2
RectMinMax(v2 Min, v2 Max)
{
    rectangle2 Result;

    Result.Min = Min;
    Result.Max = Max;

    return (Result);
}

inline rectangle2
RectMinDim(v2 Min, v2 Dim)
{
    rectangle2 Result;

    Result.Min = Min;
    Result.Max = Min + Dim;

    return (Result);
}

inline rectangle2
RectMinDim(real32 MinX, real32 MinY, real32 DimX, real32 DimY)
{
    rectangle2 Result = RectMinDim(V2(MinX, MinY), V2(DimX, DimY));
    
    return (Result);
}

inline rectangle2
RectCenterHalfDim(v2 Center, v2 HalfDim)
{
    rectangle2 Result;

    Result.Min = Center - HalfDim;
    Result.Max = Center + HalfDim;

    return (Result);
}

inline rectangle2
AddRadiusTo(rectangle2 A, v2 Radius)
{
    rectangle2 Result;
    Result.Min = A.Min - Radius;
    Result.Max = A.Max + Radius;

    return (Result);
}

inline rectangle2
SubRadiusFrom(rectangle2 A, v2 Radius)
{
    rectangle2 Result;
    Result.Min = A.Min + Radius;
    Result.Max = A.Max - Radius;

    return (Result);
}

inline rectangle2
RectCenterDim(v2 Center, v2 Dim)
{
    rectangle2 Result = RectCenterHalfDim(Center, 0.5f * Dim);

    return (Result);
}

inline bool32
IsInRectangle(rectangle2 Rectangle, v2 Test)
{
    bool32 Result = ((Test.x >= Rectangle.Min.x) && (Test.y >= Rectangle.Min.y) && (Test.x < Rectangle.Max.x) && (Test.y < Rectangle.Max.y));

    return (Result);
}


inline bool32
IsInRectangle(rectangle2 BigRect, rectangle2 SmallRect)
{
    bool32 Result = IsInRectangle(BigRect, SmallRect.Min) && IsInRectangle(BigRect, SmallRect.Max);
    
    return (Result);
}

inline v2
GetBarycentric(rectangle2 A, v2 P)
{
    v2 Result;

    Result.x = SafeRatio0(P.x - A.Min.x, A.Max.x - A.Min.x);
    Result.y = SafeRatio0(P.y - A.Min.y, A.Max.y - A.Min.y);

    return (Result);
}

inline rectangle2
Offset(rectangle2 A, v2 Offset)
{
    rectangle2 Result;

    Result.Min = A.Min + Offset;
    Result.Max = A.Max + Offset;

    return (Result);
}

//
// NOTE(casey): Rectangle3
//

inline v3
GetMinCorner(rectangle3 Rect)
{
    v3 Result = Rect.Min;
    return (Result);
}

inline v3
GetMaxCorner(rectangle3 Rect)
{
    v3 Result = Rect.Max;
    return (Result);
}

inline v3
GetCenter(rectangle3 Rect)
{
    v3 Result = 0.5f * (Rect.Min + Rect.Max);
    return (Result);
}

inline rectangle3
RectMinMax(v3 Min, v3 Max)
{
    rectangle3 Result;

    Result.Min = Min;
    Result.Max = Max;

    return (Result);
}

inline rectangle3
RectMinDim(v3 Min, v3 Dim)
{
    rectangle3 Result;

    Result.Min = Min;
    Result.Max = Min + Dim;

    return (Result);
}

inline rectangle3
RectCenterHalfDim(v3 Center, v3 HalfDim)
{
    rectangle3 Result;

    Result.Min = Center - HalfDim;
    Result.Max = Center + HalfDim;

    return (Result);
}

inline rectangle3
AddRadiusTo(rectangle3 A, v3 Radius)
{
    rectangle3 Result;

    Result.Min = A.Min - Radius;
    Result.Max = A.Max + Radius;

    return (Result);
}

inline rectangle3
Offset(rectangle3 A, v3 Offset)
{
    rectangle3 Result;

    Result.Min = A.Min + Offset;
    Result.Max = A.Max + Offset;

    return (Result);
}

inline rectangle3
RectCenterDim(v3 Center, v3 Dim)
{
    rectangle3 Result = RectCenterHalfDim(Center, 0.5f * Dim);

    return (Result);
}

inline bool32
IsInRectangle(rectangle3 Rectangle, v3 Test)
{
    bool32 Result = ((Test.x >= Rectangle.Min.x) && (Test.y >= Rectangle.Min.y) && (Test.z >= Rectangle.Min.z) && (Test.x < Rectangle.Max.x) && (Test.y < Rectangle.Max.y) && (Test.z < Rectangle.Max.z));

    return (Result);
}

inline bool32
RectanglesIntersect(rectangle3 A, rectangle3 B)
{
    bool32 Result = !((B.Max.x <= A.Min.x) || (B.Min.x >= A.Max.x) || (B.Max.y <= A.Min.y) || (B.Min.y >= A.Max.y) || (B.Max.z <= A.Min.z) || (B.Min.z >= A.Max.z));
    return (Result);
}

inline v3
GetBarycentric(rectangle3 A, v3 P)
{
    v3 Result;

    Result.x = SafeRatio0(P.x - A.Min.x, A.Max.x - A.Min.x);
    Result.y = SafeRatio0(P.y - A.Min.y, A.Max.y - A.Min.y);
    Result.z = SafeRatio0(P.z - A.Min.z, A.Max.z - A.Min.z);

    return (Result);
}

inline rectangle2
ToRectangleXY(rectangle3 A)
{
    rectangle2 Result;

    Result.Min = A.Min.xy;
    Result.Max = A.Max.xy;

    return (Result);
}

//
// Matrix4
//

/*
translation matrix:
1 0 0 x
0 1 0 y
0 0 1 z
0 0 0 1

scaling matrix
x 0 0 0
0 y 0 0
0 0 z 0
0 0 0 1

Should do Scale -> Rotate -> Translate
Result = Translate * Rotate * Scale * Vector

 */

//matrix by single float

//probably wont use these much so only did add and subtract,
//multiply and divide would just be a straight distribution
//as well

inline matrix4
operator+(matrix4 A, float B)
{
    matrix4 Result = A;
    //NOTE(james):seems like a classic spot for SIMD acceleration
    //could just make these (v4 + float) and SIMD that
    Result.Row0 += B;
    Result.Row1 += B;
    Result.Row2 += B;
    Result.Row3 += B;
    return (Result);
}

inline matrix4
operator+(float B, matrix4 A)
{
    matrix4 Result;

    Result = A + B;

    return Result;
}

inline matrix4&
operator+=(matrix4& A, float B)
{
    A = A + B;

    return (A);
}

inline matrix4
operator-(matrix4 A, float B)
{
    matrix4 Result = A;
    //NOTE(james):seems like a classic spot for SIMD acceleration
    //could just make these (v4 + float) and SIMD that
    Result.Row0 -= B;
    Result.Row1 -= B;
    Result.Row2 -= B;
    Result.Row3 -= B;
    return (Result);
}

inline matrix4
operator-(float B, matrix4 A)
{
    matrix4 Result;

    Result = A - B;

    return Result;
}

inline matrix4&
operator-=(matrix4& A, float B)
{
    A = A - B;

    return (A);
}

// Matrix by Vector Maths
//definition here https://open.gl/transformations
inline v4
operator*(matrix4 A, v4 B)
{
    v4 Result = {};

    Result.x = Inner(A.Row0, B);
    Result.y = Inner(A.Row1, B);
    Result.z = Inner(A.Row2, B);
    Result.w = Inner(A.Row3, B);

    return Result;
}

#if 0
inline v4 &
operator*=
#endif

/*
  0  4  8  12    0  4  8  12
  1  5  9  13    1  5  9  13
  2  6  10 14    2  6  10 14
  3  7  11 15    3  7  11 15
 */

/*
 0  1  2  3
 4  5  6  7
 8  9 10 11
12 13 14 15
*/

//Matrix by Matrix
inline matrix4
operator*(matrix4 A, matrix4 B)
{
    matrix4 Result;

    Result.E[0] = A.E[0] * B.E[0] + A.E[1] * B.E[4] + A.E[2] * B.E[8] + A.E[3] * B.E[12];
    Result.E[1] = A.E[0] * B.E[1] + A.E[1] * B.E[5] + A.E[2] * B.E[9] + A.E[3] * B.E[13];
    Result.E[2] = A.E[0] * B.E[2] + A.E[1] * B.E[6] + A.E[2] * B.E[10] + A.E[3] * B.E[14];
    Result.E[3] = A.E[0] * B.E[3] + A.E[1] * B.E[7] + A.E[2] * B.E[11] + A.E[3] * B.E[15];

    Result.E[4] = A.E[4] * B.E[0] + A.E[5] * B.E[4] + A.E[6] * B.E[8] + A.E[7] * B.E[12];
    Result.E[5] = A.E[4] * B.E[1] + A.E[5] * B.E[5] + A.E[6] * B.E[9] + A.E[7] * B.E[13];
    Result.E[6] = A.E[4] * B.E[2] + A.E[5] * B.E[6] + A.E[6] * B.E[10] + A.E[7] * B.E[14];
    Result.E[7] = A.E[4] * B.E[3] + A.E[5] * B.E[7] + A.E[6] * B.E[11] + A.E[7] * B.E[15];

    Result.E[8] = A.E[8] * B.E[0] + A.E[9] * B.E[4] + A.E[10] * B.E[8] + A.E[11] * B.E[12];
    Result.E[9] = A.E[8] * B.E[1] + A.E[9] * B.E[5] + A.E[10] * B.E[9] + A.E[11] * B.E[13];
    Result.E[10] = A.E[8] * B.E[2] + A.E[9] * B.E[6] + A.E[10] * B.E[10] + A.E[11] * B.E[14];
    Result.E[11] = A.E[8] * B.E[3] + A.E[9] * B.E[7] + A.E[10] * B.E[11] + A.E[11] * B.E[15];

    Result.E[12] = A.E[12] * B.E[0] + A.E[13] * B.E[4] + A.E[14] * B.E[8] + A.E[15] * B.E[12];
    Result.E[13] = A.E[12] * B.E[1] + A.E[13] * B.E[5] + A.E[14] * B.E[9] + A.E[15] * B.E[13];
    Result.E[14] = A.E[12] * B.E[2] + A.E[13] * B.E[6] + A.E[14] * B.E[10] + A.E[15] * B.E[14];
    Result.E[15] = A.E[12] * B.E[3] + A.E[13] * B.E[7] + A.E[14] * B.E[11] + A.E[15] * B.E[15];

    return Result;
}

inline matrix4&
operator*=(matrix4& B, matrix4 A)
{
    B = A * B;

    return (B);
}

v3 MatrixByPosition(matrix4* A, v3 Position)
{
    v4 Escalate = ToV4(Position, 1);
    Escalate = *A * Escalate;

    return Escalate.xyz;
}

v3 MatrixByDirection(matrix4* A, v3 Direction)
{
    v4 Escalate = ToV4(Direction, 0);
    Escalate = *A * Escalate;

    return Escalate.xyz;
}

//Matrix Helper Functions

matrix4 IdentityMatrix()
{
    matrix4 Result = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    return Result;
}

inline matrix4
CreateScaleMatrix(v3 A)
{
    matrix4 Result = {
        A.x, 0, 0, 0,
        0, A.y, 0, 0,
        0, 0, A.z, 0,
        0, 0, 0, 1
    };

    return Result;
}

inline void
SetScale(matrix4* Ptr, v3 A)
{
    //matrix4 Result = *Ptr;

    Ptr->Row0.x = A.x;
    Ptr->Row1.y = A.y;
    Ptr->Row2.z = A.z;

    //*Ptr = Result;
}

inline matrix4
Translate(v3 A)
{
    matrix4 Result = {
        1, 0, 0, A.x,
        0, 1, 0, A.y,
        0, 0, 1, A.z,
        0, 0, 0, 1
    };

    return Result;
}

inline void
SetTranslate(matrix4* Matrix, v3 A)
{
    matrix4 Result = *Matrix;

    Result.Row0.w = A.x;
    Result.Row1.w = A.y;
    Result.Row2.w = A.z;

    *Matrix = Result;
}

inline matrix4 RotationX(float Radians)
{
    matrix4 Result = IdentityMatrix();

    Result.Row0 = V4(1, 0, 0, 0);
    Result.Row1 = V4(0, Cos(Radians), -Sin(Radians), 0);
    Result.Row2 = V4(0, Sin(Radians), Cos(Radians), 0);
    Result.Row3 = V4(0, 0, 0, 1);

    return Result;
}

//TODO(james): rotates in the wrong direction I think,
// but we shouldn't be using this function anymore
inline matrix4 RotationY(float Radians)
{
    matrix4 Result = IdentityMatrix();

    Result.Row0 = V4(Cos(Radians), 0, Sin(Radians), 0);
    Result.Row1 = V4(0, 1, 0, 0);
    Result.Row2 = V4(-Sin(Radians), 0, Cos(Radians), 0);
    Result.Row3 = V4(0, 0, 0, 1);

    return Result;
}

inline matrix4 RotationZ(float Radians)
{
    matrix4 Result = {
        Cos(Radians), -Sin(Radians), 0, 0,
        Sin(Radians), Cos(Radians), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };

    return Result;
}

//TODO(james): this is separate from ObjectLookAt because a CameraLookAt is inverted? I dont reallyget it
//TODO(james):could compute one of these from a Cross
inline matrix4 CameraLookAt(v3 RightDirection, v3 UpDirection, v3 ForwardDirection, v3 Position)
{
    Assert(AbsoluteValue(Inner(RightDirection, UpDirection)) < 0.0001f);
    Assert(AbsoluteValue(Inner(RightDirection, ForwardDirection)) < 0.0001f);
    Assert(AbsoluteValue(Inner(UpDirection, ForwardDirection)) < 0.0001f);

    v3& R = RightDirection;
    v3& U = UpDirection;
    v3& L = ForwardDirection;
    v3& P = Position;

    //TODO(james): should be able to premultiply these http://stackoverflow.com/questions/349050/calculating-a-lookat-matrix
    matrix4 MatrixA = {
        R.x, R.y, R.z, 0,
        U.x, U.y, U.z, 0,
        L.x, L.y, L.z, 0,
        0, 0, 0, 1
    };

    matrix4 MatrixB = {
        1, 0, 0, -P.x,
        0, 1, 0, -P.y,
        0, 0, 1, -P.z,
        0, 0, 0, 1
    };
    matrix4 Result = MatrixA * MatrixB;

    return Result;
}

//TODO(james): Test this
inline matrix4 ObjectLookAt(v3 RightDirection, v3 UpDirection, v3 ForwardDirection, v3 Position)
{
    real32 Ang0 = AbsoluteValue(Inner(RightDirection, UpDirection));
    real32 Ang1 = AbsoluteValue(Inner(RightDirection, ForwardDirection));
    real32 Ang2 = AbsoluteValue(Inner(UpDirection, ForwardDirection));

    Assert(Ang0 < 0.0001f);
    Assert(Ang1 < 0.0001f);
    Assert(Ang2 < 0.0001f);

    v3& R = RightDirection;
    v3& U = UpDirection;
    v3& L = ForwardDirection;
    v3& P = Position;

    matrix4 MatrixA = {
        R.x, U.x, L.x, 0,
        R.y, U.y, L.y, 0,
        R.z, U.z, L.z, 0,
        0, 0, 0, 1
    };

    matrix4 MatrixB = {
        1, 0, 0, P.x,
        0, 1, 0, P.y,
        0, 0, 1, P.z,
        0, 0, 0, 1
    };
    matrix4 Result = MatrixA * MatrixB;

    return Result;
}

//TODO(james): test this guy, seemed like when I translated the view, objects got smaller
inline matrix4
Ortho(real32 Left, real32 Right, real32 Bottom, real32 Top, real32 NearPlane, real32 FarPlane)
{
    matrix4 Result;

    Result.Row0 = V4(2 / (Right - Left), 0, 0, -((Right + Left) / (Right - Left)));
    Result.Row1 = V4(0, 2 / (Top - Bottom), 0, -((Top + Bottom) / (Top - Bottom)));
    Result.Row2 = { 0, 0, ((-2) / (FarPlane - NearPlane)), -((FarPlane + NearPlane) / (FarPlane - NearPlane)) };
    Result.Row3 = V4(0, 0, 0, 1);

    return Result;
}

inline matrix4
Transpose(matrix4 A)
{
    matrix4 Result;

    Result = {
        A.E[0], A.E[4], A.E[8], A.E[12],
        A.E[1], A.E[5], A.E[9], A.E[13],
        A.E[2], A.E[6], A.E[10], A.E[14],
        A.E[3], A.E[7], A.E[11], A.E[15]
    };

    return Result;
}

//NOTE(james): from  http://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/building-basic-perspective-projection-matrix
//NOTE(james): great article on difference between RH and LH systems in DX and OpenGL http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/perspective-projections-in-lh-and-rh-systems-r3598
inline matrix4
Perspective(real32 FieldOfView, real32 AspectRatio, real32 NearPlane, real32 FarPlane)
{
    matrix4 Result;

    real32 scale = Tan(FieldOfView * (real32)0.5 * (real32)PI / (real32)180) * NearPlane;
    real32 t = scale;
    real32 b = -scale;

    real32 l = -AspectRatio * scale;
    real32 r = AspectRatio * scale;

    real32 n = NearPlane;
    real32 f = FarPlane;

    Result.E[0] = 2 * n / (r - l);
    Result.E[1] = 0;
    Result.E[2] = 0;
    Result.E[3] = 0;

    Result.E[4] = 0;
    Result.E[5] = 2 * n / (t - b);
    Result.E[6] = 0;
    Result.E[7] = 0;

    Result.E[8] = (r + l) / (r - l);
    Result.E[9] = (t + b) / (t - b);
    Result.E[10] = -(f + n) / (f - n);
    Result.E[11] = -1; // -1 for right hand

    Result.E[12] = 0;
    Result.E[13] = 0;
    Result.E[14] = -2 * f * n / (f - n); //02 for right hand
    Result.E[15] = 0;

    Result = Transpose(Result);

    return Result;
}

//NOTE(james): marked with a superscript -1 in formal math
inline matrix4
InverseMatrix(matrix4 A)
{
    //
    // Inversion by Cramer's rule.  Code taken from an Intel publication
    //
    real32 Result[4][4];
    real32 tmp[12]; /* temp array for pairs */
    //real32 src[16]; /* array of transpose source matrix */
    real32 det; /* determinant */
    /* transpose matrix */
    matrix4 src = Transpose(A);
    /* calculate pairs for first 8 elements (cofactors) */
    tmp[0] = src.E[10] * src.E[15];
    tmp[1] = src.E[11] * src.E[14];
    tmp[2] = src.E[9] * src.E[15];
    tmp[3] = src.E[11] * src.E[13];
    tmp[4] = src.E[9] * src.E[14];
    tmp[5] = src.E[10] * src.E[13];
    tmp[6] = src.E[8] * src.E[15];
    tmp[7] = src.E[11] * src.E[12];
    tmp[8] = src.E[8] * src.E[14];
    tmp[9] = src.E[10] * src.E[12];
    tmp[10] = src.E[8] * src.E[13];
    tmp[11] = src.E[9] * src.E[12];
    /* calculate first 8 elements (cofactors) */
    Result[0][0] = tmp[0] * src.E[5] + tmp[3] * src.E[6] + tmp[4] * src.E[7];
    Result[0][0] -= tmp[1] * src.E[5] + tmp[2] * src.E[6] + tmp[5] * src.E[7];
    Result[0][1] = tmp[1] * src.E[4] + tmp[6] * src.E[6] + tmp[9] * src.E[7];
    Result[0][1] -= tmp[0] * src.E[4] + tmp[7] * src.E[6] + tmp[8] * src.E[7];
    Result[0][2] = tmp[2] * src.E[4] + tmp[7] * src.E[5] + tmp[10] * src.E[7];
    Result[0][2] -= tmp[3] * src.E[4] + tmp[6] * src.E[5] + tmp[11] * src.E[7];
    Result[0][3] = tmp[5] * src.E[4] + tmp[8] * src.E[5] + tmp[11] * src.E[6];
    Result[0][3] -= tmp[4] * src.E[4] + tmp[9] * src.E[5] + tmp[10] * src.E[6];
    Result[1][0] = tmp[1] * src.E[1] + tmp[2] * src.E[2] + tmp[5] * src.E[3];
    Result[1][0] -= tmp[0] * src.E[1] + tmp[3] * src.E[2] + tmp[4] * src.E[3];
    Result[1][1] = tmp[0] * src.E[0] + tmp[7] * src.E[2] + tmp[8] * src.E[3];
    Result[1][1] -= tmp[1] * src.E[0] + tmp[6] * src.E[2] + tmp[9] * src.E[3];
    Result[1][2] = tmp[3] * src.E[0] + tmp[6] * src.E[1] + tmp[11] * src.E[3];
    Result[1][2] -= tmp[2] * src.E[0] + tmp[7] * src.E[1] + tmp[10] * src.E[3];
    Result[1][3] = tmp[4] * src.E[0] + tmp[9] * src.E[1] + tmp[10] * src.E[2];
    Result[1][3] -= tmp[5] * src.E[0] + tmp[8] * src.E[1] + tmp[11] * src.E[2];
    /* calculate pairs for second 8 elements (cofactors) */
    tmp[0] = src.E[2] * src.E[7];
    tmp[1] = src.E[3] * src.E[6];
    tmp[2] = src.E[1] * src.E[7];
    tmp[3] = src.E[3] * src.E[5];
    tmp[4] = src.E[1] * src.E[6];
    tmp[5] = src.E[2] * src.E[5];

    tmp[6] = src.E[0] * src.E[7];
    tmp[7] = src.E[3] * src.E[4];
    tmp[8] = src.E[0] * src.E[6];
    tmp[9] = src.E[2] * src.E[4];
    tmp[10] = src.E[0] * src.E[5];
    tmp[11] = src.E[1] * src.E[4];
    /* calculate second 8 elements (cofactors) */
    Result[2][0] = tmp[0] * src.E[13] + tmp[3] * src.E[14] + tmp[4] * src.E[15];
    Result[2][0] -= tmp[1] * src.E[13] + tmp[2] * src.E[14] + tmp[5] * src.E[15];
    Result[2][1] = tmp[1] * src.E[12] + tmp[6] * src.E[14] + tmp[9] * src.E[15];
    Result[2][1] -= tmp[0] * src.E[12] + tmp[7] * src.E[14] + tmp[8] * src.E[15];
    Result[2][2] = tmp[2] * src.E[12] + tmp[7] * src.E[13] + tmp[10] * src.E[15];
    Result[2][2] -= tmp[3] * src.E[12] + tmp[6] * src.E[13] + tmp[11] * src.E[15];
    Result[2][3] = tmp[5] * src.E[12] + tmp[8] * src.E[13] + tmp[11] * src.E[14];
    Result[2][3] -= tmp[4] * src.E[12] + tmp[9] * src.E[13] + tmp[10] * src.E[14];
    Result[3][0] = tmp[2] * src.E[10] + tmp[5] * src.E[11] + tmp[1] * src.E[9];
    Result[3][0] -= tmp[4] * src.E[11] + tmp[0] * src.E[9] + tmp[3] * src.E[10];
    Result[3][1] = tmp[8] * src.E[11] + tmp[0] * src.E[8] + tmp[7] * src.E[10];
    Result[3][1] -= tmp[6] * src.E[10] + tmp[9] * src.E[11] + tmp[1] * src.E[8];
    Result[3][2] = tmp[6] * src.E[9] + tmp[11] * src.E[11] + tmp[3] * src.E[8];
    Result[3][2] -= tmp[10] * src.E[11] + tmp[2] * src.E[8] + tmp[7] * src.E[9];
    Result[3][3] = tmp[10] * src.E[10] + tmp[4] * src.E[8] + tmp[9] * src.E[9];
    Result[3][3] -= tmp[8] * src.E[9] + tmp[11] * src.E[10] + tmp[5] * src.E[8];
    /* calculate determinant */
    det = src.E[0] * Result[0][0] + src.E[1] * Result[0][1] + src.E[2] * Result[0][2] + src.E[3] * Result[0][3];
    /* calculate matrix inverse */
    det = 1.0f / det;

    matrix4 FloatResult;
    for (uint32 i = 0; i < 4; i++)
    {
        for (uint32 j = 0; j < 4; j++)
        {
            FloatResult.E[(i * 4) + j] = Result[i][j] * det;
        }
    }
    return FloatResult;
}

inline v4
AngleAxisToQuaternion(float Angle, v3 Axis)
{
    Assert(Axis != V3(0, 0, 0));

    v4 Result = V4(0, 0, 0, 1);

    Axis = Normalize(Axis);
    Assert(Angle < PI * 2 && Angle > -PI * 2);

    float AxisDenominator = (Sin(Angle / 2));

    Result.x = Axis.x * AxisDenominator;
    Result.y = Axis.y * AxisDenominator;
    Result.z = Axis.z * AxisDenominator;
    Result.w = Cos(Angle / 2);

    Result = Result * (SafeRatio0(1, Length(Result)));

    Assert(ApproximatelyEqual(Length(Result), 1));

    return Result;
}

//http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/
inline v4
Slerp(v4 A, v4 B, real32 Percent)
{
    v4 Result = {};
    // Calculate angle between them.
    double cosHalfTheta = A.w * B.w + A.x * B.x + A.y * B.y + A.z * B.z;
    // if A=B or A=-B then theta = 0 and we can return A
    if (AbsoluteValue(cosHalfTheta) >= 1.0)
    {
        Result.w = A.w;
        Result.x = A.x;
        Result.y = A.y;
        Result.z = A.z;
        return Result;
    }
    // Calculate temporary values.
    double halfTheta = acos(cosHalfTheta);
    double sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);
    // if theta = 180 degrees then result is not fully defined
    // we could rotate around any axis normal to A or B
    if (AbsoluteValue(sinHalfTheta) < 0.001)
    { // fabs is floating point absolute
        Result.w = (A.w * 0.5 + B.w * 0.5);
        Result.x = (A.x * 0.5 + B.x * 0.5);
        Result.y = (A.y * 0.5 + B.y * 0.5);
        Result.z = (A.z * 0.5 + B.z * 0.5);
        return Result;
    }
    double ratioA = sin((1 - Percent) * halfTheta) / sinHalfTheta;
    double ratioB = sin(Percent * halfTheta) / sinHalfTheta;
    //calculate Quaternion.
    Result.w = (A.w * ratioA + B.w * ratioB);
    Result.x = (A.x * ratioA + B.x * ratioB);
    Result.y = (A.y * ratioA + B.y * ratioB);
    Result.z = (A.z * ratioA + B.z * ratioB);
    return Result;
}

//http://web.archive.org/web/20060914224155/http://web.archive.org/web/20041029003853/http://www.j3d.org/matrix_faq/matrfaq_latest.html#Q50
inline void
QuaternionToAngleAxis(v4 Quaternion, real32* Angle, v3* Axis)
{
    Assert(Quaternion.w < 1); //non-normalized

    if (Quaternion.w == 1)
    {
        *Angle = 0;
        *Axis = v3_FORWARD;
    }
    else
    {
        real32 CosA = Quaternion.w;
        *Angle = acos(CosA) * 2;
        real32 SinA = SquareRoot(1 - CosA * CosA);
        Assert(SinA);
        Axis->x = SafeRatio0(Quaternion.x, SinA);
        Axis->y = SafeRatio0(Quaternion.y, SinA);
        Axis->z = SafeRatio0(Quaternion.z, SinA);
    }
}

inline v4
CombineQuaternions(v4 InA, v4 InB)
{
#if 0
	float A, B, C, D, E, F, G, H;

	A = (InA.w + InA.x)*(InB.w + InB.x);
	B = (InA.z - InA.y)*(InB.y - InB.z);
	C = (InA.w - InA.x)*(InB.y + InB.z);
	D = (InA.y + InA.z)*(InB.w - InB.x);
	E = (InA.x + InA.z)*(InB.x + InB.y);
	F = (InA.x - InA.z)*(InB.x - InB.y);
	G = (InA.w + InA.y)*(InB.w - InB.z);
	H = (InA.w - InA.y)*(InB.w + InB.z);

    v4 Result = {};
	Result.w = B + (H - E - F + G)*0.5;
	Result.x = A - (E + F + G + H)*0.5;
	Result.y = C + (E - F + G - H)*0.5;
	Result.z = D + (E - F - G + H)*0.5;
#endif

    v4 Result = V4(InA.y * InB.z - InA.z * InB.y + InA.w * InB.x + InA.x * InB.w,
                   InA.z * InB.x - InA.x * InB.z + InA.w * InB.y + InA.y * InB.w,
                   InA.x * InB.y - InA.y * InB.x + InA.w * InB.z + InA.z * InB.w,
                   InA.w * InB.w - Inner(V3(InA.x, InA.y, InA.z), V3(InB.x, InB.y, InB.z)));

    return Result;
}

inline v4
InverseQuaternion(v4 A)
{
    v4 Result = V4(-A.x, -A.y, -A.z, A.w);
    return Result;
}

//TODO(james): Doesn't work
inline v4
EulerToQuaternion(v3 A)
{
    //accepts radians, not degrees
    Assert(A.x < PI * 2 && A.x > -PI * 2);
    Assert(A.y < PI * 2 && A.y > -PI * 2);
    Assert(A.z < PI * 2 && A.z > -PI * 2);

    v4 Result;
#if 0
    real32 c1 = Cos(A.x/2);
    real32 s1 = Sin(A.x/2);
    real32 c2 = Cos(A.y/2);
    real32 s2 = Sin(A.y/2);
    real32 c3 = Cos(A.z/2);
    real32 s3 = Sin(A.z/2);
    real32 c1c2 = c1*c2;
    real32 s1s2 = s1*s2;
    Result.w =c1c2*c3 - s1s2*s3;
    Result.x =c1c2*s3 + s1s2*c3;
	Result.y =s1*c2*c3 + c1*s2*s3;
	Result.z =c1*s2*c3 - s1*c2*s3;
#endif

#if 1
    v4 XAxis = AngleAxisToQuaternion(A.x, V3(1, 0, 0));
    v4 YAxis = AngleAxisToQuaternion(A.y, V3(0, 1, 0));
    v4 ZAxis = AngleAxisToQuaternion(A.z, V3(0, 0, 1));
    Result = Normalize(CombineQuaternions(CombineQuaternions(XAxis, YAxis), ZAxis));
#endif

#if 0
    v4 QuaternionX = V4(Sin(A.x/2), 0, 0, Cos(A.x/2));
    v4 QuaternionY = V4(0, Sin(A.y/2), 0, Cos(A.y/2));
    v4 QuaternionZ = V4(0, 0, Sin(A.z/2), Cos(A.z/2));
    Result = CombineQuaternions(CombineQuaternions(QuaternionX, QuaternionY), QuaternionZ);
#endif

    return Result;
}

//http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToEuler/
inline v3
AngleAxisToEuler(real32 Angle, v3 Axis)
{
    double s = Sin(Angle);
    double c = Cos(Angle);
    double t = 1 - c;
    //  if aAxis.xis is not alreadAxis.y normalised then uncomment this
    // double magnitude = Math.sqrt(Axis.x*Axis.x + Axis.y*Axis.y + Axis.z*Axis.z);
    // if (magnitude==0) throw error;
    // Axis.x /= magnitude;
    // Axis.y /= magnitude;
    // Axis.z /= magnitude;
    v3 Result;
    if ((Axis.x * Axis.y * t + Axis.z * s) > 0.998)
    { // north pole singularitAxis.y detected
        Result.x = 2 * atan2(Axis.x * sin(Angle / 2), cos(Angle / 2));
        Result.y = PI / 2;
        Result.z = 0;
        return Result;
    }
    if ((Axis.x * Axis.y * t + Axis.z * s) < -0.998)
    { // south pole singularitAxis.y detected
        Result.x = -2 * atan2(Axis.x * sin(Angle / 2), cos(Angle / 2));
        Result.y = -PI / 2;
        Result.z = 0;
        return Result;
    }

    Result.x = atan2(Axis.y * s - Axis.x * Axis.z * t, 1 - (Axis.y * Axis.y + Axis.z * Axis.z) * t);
    Result.y = asin(Axis.x * Axis.y * t + Axis.z * s);
    Result.z = atan2(Axis.x * s - Axis.y * Axis.z * t, 1 - (Axis.x * Axis.x + Axis.z * Axis.z) * t);
    return Result;
}

// in rads
real32 FixUpNegativeEulerAngles(real32 Angle)
{
    real32 Result = Angle;
    if (Angle < 0)
    {
        Result = Angle + (PI * 2);
    }

    return Result;
}

//TODO(james): doesn't work
//http://bediyap.com/programming/convert-quaternion-to-euler-rotations/
inline v3
QuaternionToEuler(v4 Quaternion)
{
    Assert(ApproximatelyEqual(Length(Quaternion), 1.0f, 0.001f));
    v4 q = Quaternion;

    real32 r11 = -2 * (q.y * q.z - q.w * q.x);
    real32 r12 = q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z;
    real32 r21 = 2 * (q.x * q.z + q.w * q.y);
    real32 r31 = -2 * (q.x * q.y - q.w * q.z);
    real32 r32 = q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z;

    v3 Result = {};

    //if zero is passed for both arguments of atan2, a domain error occurs
    if (r11 == 0 && r12 == 0)
    {
        Result.x = 0;
    }
    else
    {
        Result.x = atan2(r11, r12);
    }

    // values outside of [-1,1] will be a domain error
    r21 = Clamp(-1, r21, 1);

    Result.y = asin(r21);

    if (r31 == 0 && r32 == 0)
    {
        Result.z = 0;
    }
    else
    {
        Result.z = atan2(r31, r32);
    }

    return Result;
}

//http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/jay.htm
inline matrix4
QuaternionToMatrix(v4 A)
{
    matrix4 PartA = {
        A.w, A.z, -A.y, A.x,
        -A.z, A.w, A.x, A.y,
        A.y, -A.x, A.w, A.z,
        -A.x, -A.y, -A.z, A.w
    };
    matrix4 PartB = {
        A.w, A.z, -A.y, -A.x,
        -A.z, A.w, A.x, -A.y,
        A.y, -A.x, A.w, -A.z,
        A.x, A.y, A.z, A.w
    };
    return PartA * PartB;
}

inline matrix4
QuaternionToMatrixVersion3(v4 Q)
{
    matrix4 M;
    float x2 = Q.E[0] + Q.E[0];
    float y2 = Q.E[1] + Q.E[1];
    float z2 = Q.E[2] + Q.E[2];
    {
        float xx2 = Q.E[0] * x2;
        float yy2 = Q.E[1] * y2;
        float zz2 = Q.E[2] * z2;
        M.E[0 * 4 + 0] = 1.0f - yy2 - zz2;
        M.E[1 * 4 + 1] = 1.0f - xx2 - zz2;
        M.E[2 * 4 + 2] = 1.0f - xx2 - yy2;
    }
    {
        float yz2 = Q.E[1] * z2;
        float wx2 = Q.E[3] * x2;
        M.E[2 * 4 + 1] = yz2 - wx2;
        M.E[1 * 4 + 2] = yz2 + wx2;
    }
    {
        float xy2 = Q.E[0] * y2;
        float wz2 = Q.E[3] * z2;
        M.E[1 * 4 + 0] = xy2 - wz2;
        M.E[0 * 4 + 1] = xy2 + wz2;
    }
    {
        float xz2 = Q.E[0] * z2;
        float wy2 = Q.E[3] * y2;
        M.E[0 * 4 + 2] = xz2 - wy2;
        M.E[2 * 4 + 0] = xz2 + wy2;
    }
    return M;
}

//https://www.fd.cvut.cz/personal/voracsar/GeometriePG/PGR020/matrix2quaternions.pdf
inline matrix4
QuaternionToMatrixVersion2(v4 A)
{
    matrix4 Result;

    float XSquared = A.x * A.x;
    float YSquared = A.y * A.y;
    float ZSquared = A.z * A.z;
    Result = {
        1 - (2 * YSquared) - (2 * ZSquared), 2 * A.x * A.y + 2 * A.w * A.z, 2 * A.x * A.z - 2 * A.w * A.y, 0,
        2 * A.x * A.y - 2 * A.w * A.z, 1 - (2 * XSquared) - (2 * ZSquared), 2 * A.y * A.z + 2 * A.w * A.x, 0,
        2 * A.x * A.z + 2 * A.w * A.y, 2 * A.y * A.z - 2 * A.w * A.x, 1 - (2 * XSquared) - (2 * YSquared), 0,
        0, 0, 0, 1
    };
    return Result;
}

v3 ScaleFromTRSMatrix(matrix4* M)
{
    v3 Scale;
    Scale.x = Length(V3(M->Row0.x, M->Row0.y, M->Row0.z));
    Scale.y = Length(V3(M->Row1.x, M->Row1.y, M->Row1.z));
    Scale.z = Length(V3(M->Row2.x, M->Row2.y, M->Row2.z));
    return Scale;
}

inline float SIGN(float x)
{
    return (x >= 0.0f) ? +1.0f : -1.0f;
}
inline v4
MatrixToQuaternion(matrix4 A)
{
    v4 Result = {};

    // NOTE(james): incoming matrix is TRS, so we must decompose.
    // Extract scaling data and apply it to rotation data
    v3 Scale = ScaleFromTRSMatrix(&A);

    A.Row0.x /= Scale.x;
    A.Row0.y /= Scale.x;
    A.Row0.z /= Scale.x;

    A.Row1.x /= Scale.y;
    A.Row1.y /= Scale.y;
    A.Row1.z /= Scale.y;

    A.Row2.x /= Scale.z;
    A.Row2.y /= Scale.z;
    A.Row2.z /= Scale.z;

    // actual rotation extraction
    real32 q0 = (A.E[0] + A.E[5] + A.E[10] + 1.0f) / 4.0f;
    real32 q1 = (A.E[0] - A.E[5] - A.E[10] + 1.0f) / 4.0f;
    real32 q2 = (-A.E[0] + A.E[5] - A.E[10] + 1.0f) / 4.0f;
    real32 q3 = (-A.E[0] - A.E[5] + A.E[10] + 1.0f) / 4.0f;
    if (q0 < 0.0f)
        q0 = 0.0f;
    if (q1 < 0.0f)
        q1 = 0.0f;
    if (q2 < 0.0f)
        q2 = 0.0f;
    if (q3 < 0.0f)
        q3 = 0.0f;
    q0 = sqrt(q0);
    q1 = sqrt(q1);
    q2 = sqrt(q2);
    q3 = sqrt(q3);
    if (q0 >= q1 && q0 >= q2 && q0 >= q3)
    {
        q0 *= +1.0f;
        q1 *= SIGN(A.E[9] - A.E[6]);
        q2 *= SIGN(A.E[2] - A.E[8]);
        q3 *= SIGN(A.E[4] - A.E[1]);
    }
    else if (q1 >= q0 && q1 >= q2 && q1 >= q3)
    {
        q0 *= SIGN(A.E[9] - A.E[6]);
        q1 *= +1.0f;
        q2 *= SIGN(A.E[4] + A.E[1]);
        q3 *= SIGN(A.E[2] + A.E[8]);
    }
    else if (q2 >= q0 && q2 >= q1 && q2 >= q3)
    {
        q0 *= SIGN(A.E[2] - A.E[8]);
        q1 *= SIGN(A.E[4] + A.E[1]);
        q2 *= +1.0f;
        q3 *= SIGN(A.E[9] + A.E[6]);
    }
    else if (q3 >= q0 && q3 >= q1 && q3 >= q2)
    {
        q0 *= SIGN(A.E[4] - A.E[1]);
        q1 *= SIGN(A.E[8] + A.E[2]);
        q2 *= SIGN(A.E[9] + A.E[6]);
        q3 *= +1.0f;
    }
    else
    {
        Assert(0);
    }
    Result.x = q1;
    Result.y = q2;
    Result.z = q3;
    Result.w = q0;
    Result = Normalize(Result);

    return Result;
}

inline matrix4
MatrixTRS(v3 WorldPosition, v4 WorldRotation, v3 WorldScale)
{
    float XSquared = WorldRotation.x * WorldRotation.x;
    float YSquared = WorldRotation.y * WorldRotation.y;
    float ZSquared = WorldRotation.z * WorldRotation.z;
    matrix4 Result = {
        (1 - (2 * YSquared) - (2 * ZSquared)) * WorldScale.x, 2 * WorldRotation.x * WorldRotation.y + 2 * WorldRotation.w * WorldRotation.z, 2 * WorldRotation.x * WorldRotation.z - 2 * WorldRotation.w * WorldRotation.y, WorldPosition.x,
        2 * WorldRotation.x * WorldRotation.y - 2 * WorldRotation.w * WorldRotation.z, (1 - (2 * XSquared) - (2 * ZSquared), 2 * WorldRotation.y * WorldRotation.z + 2 * WorldRotation.w * WorldRotation.x) * WorldScale.y, WorldPosition.y,
        2 * WorldRotation.x * WorldRotation.z + 2 * WorldRotation.w * WorldRotation.y, 2 * WorldRotation.y * WorldRotation.z - 2 * WorldRotation.w * WorldRotation.x, (1 - (2 * XSquared) - (2 * YSquared)) * WorldScale.z, WorldPosition.z,
        0, 0, 0, 1
    };
    return Result;
}

inline v3
RotateVectorByQuaternion(v3 Direction, v4 Quaternion)
{
    v3 Result;

    v4 Direction4 = ToV4(Direction, 0);

    float QuaternionNorm = (Quaternion.w * Quaternion.w) + (Quaternion.x * Quaternion.x) + (Quaternion.y * Quaternion.y) + (Quaternion.z * Quaternion.z);
    v4 Conjugate = V4(-Quaternion.x, -Quaternion.y, -Quaternion.z, Quaternion.w);
    v4 Inverse = Conjugate * (1 / QuaternionNorm);
    v4 Result4 = CombineQuaternions(CombineQuaternions(Quaternion, Direction4), Inverse);
    Result = V3(Result4.x, Result4.y, Result4.z);

    return Result;
}

inline v4
InvertQuaternion(v4 Q)
{
    v4 Result;
    Result = V4(-Q.x, -Q.y, -Q.z, Q.w);
    return Result;
}

//
// Screen Utilities
//

/*
ViewProjection = Perspective(..) * List->Camera
Depth is a value -1 to 1
ScreenSpacePos is in range of [-1,1]
*/
inline v3
CameraToWorldPosition(matrix4 ViewProjection, v2 ScreenSpacePos, real32 Depth)
{
    v3 Result;

    Assert(ViewProjection.E[0] != 0);
    matrix4 InverseViewProjection = InverseMatrix(ViewProjection);

    v4 WorldPosFull = InverseViewProjection * V4(ScreenSpacePos.x, ScreenSpacePos.y, Depth, 1);
    Result = WorldPosFull.xyz * (1.0f / WorldPosFull.w);

    return Result;
}

inline v3
CameraToWorldPosition(matrix4 ViewProjection,
                      v2 ApplicationDim, rectangle2 ViewportRect,
                      v2 MousePos, real32 Depth)
{
    v3 Result;

    matrix4 InverseViewProjection = InverseMatrix(ViewProjection);

    //get mouse in -1 to 1 coordinates, flip Y
    //real32 MinYFlipped = ApplicationDim.y - ViewportRect.Max.y;
    //real32 MaxYFlipped = ApplicationDim.y - ViewportRect.Min.y;

    v2 CanonicalMouse = {};

    CanonicalMouse.x = Map(MousePos.x, ViewportRect.Min.x, ViewportRect.Max.x, -1, 1);
    CanonicalMouse.y = Map(MousePos.y, ViewportRect.Min.y, ViewportRect.Max.y, -1, 1);

    Result = CameraToWorldPosition(ViewProjection, CanonicalMouse, Depth);

    return Result;
}

inline bool32
MouseInViewport(v2 ApplicationDim, rectangle2 ViewportRect, v2 MousePos)
{
    bool32 Result = true;

    //flip Y of viewport rect
    //real32 MinYFlipped = ApplicationDim.y - ViewportRect.Max.y;
    //real32 MaxYFlipped = ApplicationDim.y - ViewportRect.Min.y;

    if (MousePos.x > ViewportRect.Max.x || MousePos.x < ViewportRect.Min.x || MousePos.y > ViewportRect.Max.y || MousePos.y < ViewportRect.Min.y)
    {
        Result = false;
    }
    return Result;
}

inline v2
ScreenCoordToViewportCoord(rectangle2 ViewportRect, v2 MousePos)
{
    v2 Result = V2(0, 0);

    Result = MousePos - ViewportRect.Min;

    return Result;
}

// matrix3

union matrix3 {
    real32 Array[3][3];
    struct
    {
        v3 A;
        v3 B;
        v3 C;
    };
};

inline v3
operator*(matrix3 A, v3 B)
{
    v3 Result;
    Result.x = Inner(A.A, B);
    Result.y = Inner(A.B, B);
    Result.z = Inner(A.C, B);

    return Result;
}

//janked from ogre
inline matrix3
operator*(matrix3 A, matrix3 B)
{
    matrix3 Result;

    for (size_t iRow = 0; iRow < 3; iRow++)
    {
        for (size_t iCol = 0; iCol < 3; iCol++)
        {
            Result.Array[iRow][iCol] = A.Array[iRow][0] * B.Array[0][iCol] + A.Array[iRow][1] * B.Array[1][iCol] + A.Array[iRow][2] * B.Array[2][iCol];
        }
    }
    return Result;
}

//janked from ogre
inline matrix3
QuaternionToMatrix3(v4 Quat)
{
    real32 fTx = Quat.x + Quat.x;
    real32 fTy = Quat.y + Quat.y;
    real32 fTz = Quat.z + Quat.z;
    real32 fTwx = fTx * Quat.w;
    real32 fTwy = fTy * Quat.w;
    real32 fTwz = fTz * Quat.w;
    real32 fTxx = fTx * Quat.x;
    real32 fTxy = fTy * Quat.x;
    real32 fTxz = fTz * Quat.x;
    real32 fTyy = fTy * Quat.y;
    real32 fTyz = fTz * Quat.y;
    real32 fTzz = fTz * Quat.z;

    matrix3 kRot;
    kRot.Array[0][0] = 1.0f - (fTyy + fTzz);
    kRot.Array[0][1] = fTxy - fTwz;
    kRot.Array[0][2] = fTxz + fTwy;
    kRot.Array[1][0] = fTxy + fTwz;
    kRot.Array[1][1] = 1.0f - (fTxx + fTzz);
    kRot.Array[1][2] = fTyz - fTwx;
    kRot.Array[2][0] = fTxz - fTwy;
    kRot.Array[2][1] = fTyz + fTwx;
    kRot.Array[2][2] = 1.0f - (fTxx + fTyy);

    return kRot;
}

inline matrix3 Transpose(matrix3 In)
{
    matrix3 Return;

    for (int Counter = 0; Counter < 3; Counter++)
    {
        Return.Array[0][Counter] = In.Array[Counter][0];
        Return.Array[1][Counter] = In.Array[Counter][1];
        Return.Array[2][Counter] = In.Array[Counter][2];
    }

    return Return;
}

////////////////////////////////////////
// v6 Header
////////////////////////////////////////

#if 0
union v6
{
    real32 Array[6];
    struct
    {
        v3 Linear;
        v3 Angular;
    };
    struct
    {
        real32 A;
        real32 B;
        real32 C;
        real32 D;
        real32 E;
        real32 F;
    };  
};

union matrix6
{
    real32 Array[36];
    struct
    {
        v6 A;
        v6 B;
        v6 C;
        v6 D;
        v6 E;
        v6 F;
    };
};

v6 Hadamard(v6* A, v6* B)
{
    v6 Result;
    Result.A = A->A * B->A;
    Result.B = A->B * B->B;
    Result.C = A->C * B->C;
    Result.D = A->D * B->D;
    Result.E = A->E * B->E;
    Result.F = A->F * B->F;
    return Result;
}

real32 Inner(v6* A, v6* B)
{
    real32 Result = 
        A->A*B->A + 
        A->B*B->B +
        A->C*B->C +
        A->D*B->D +
        A->E*B->E +
        A->F*B->F;
    return Result;
}

inline v6 
operator+(v6 A, v6 B)
{
    v6 Result;
    Result.A = A.A + B.A;
    Result.B = A.B + B.B;
    Result.C = A.C + B.C;
    Result.D = A.D + B.D;
    Result.E = A.E + B.E;
    Result.F = A.F + B.F;
    
    return Result;
}

inline v6
operator*(v6 A, real32 B)
{
    v6 Result;
    Result.A = A.A * B;
    Result.B = A.B * B;
    Result.C = A.C * B;
    Result.D = A.D * B;
    Result.E = A.E * B;
    Result.F = A.F * B;
    
    return Result;
}

inline v6
operator*(real32 A, v6 B)
{
    v6 Result = B * A;
    
    return Result;
}

//https://chi3x10.wordpress.com/2008/05/28/calculate-matrix-inversion-in-c/
// calculate the cofactor of element (row,col)
int GetMinor(float **src, float **dest, int row, int col, int order)
{
    // indicate which col and row is being copied to dest
    int colCount=0,rowCount=0;
 
    for(int i = 0; i < order; i++ )
    {
        if( i != row )
        {
            colCount = 0;
            for(int j = 0; j < order; j++ )
            {
                // when j is not the element
                if( j != col )
                {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }
            rowCount++;
        }
    }
 
    return 1;
}
 
// Calculate the determinant recursively.
double CalcDeterminant( float **mat, int order)
{
    // order must be >= 0
    // stop the recursion when matrix is a single element
    if( order == 1 )
        return mat[0][0];
 
    // the determinant value
    float det = 0;
 
    // allocate the cofactor matrix
    float **minor;
    minor = new float*[order-1];
    for(int i=0;i<order-1;i++)
        minor[i] = new float[order-1];
 
    for(int i = 0; i < order; i++ )
    {
        // get minor of element (0,i)
        GetMinor( mat, minor, 0, i , order);
        // the recusion is here!
 
        det += (i%2==1?-1.0:1.0) * mat[0][i] * CalcDeterminant(minor,order-1);
        //det += pow( -1.0, i ) * mat[0][i] * CalcDeterminant( minor,order-1 );
    }
 
    // release memory
    for(int i=0;i<order-1;i++)
        delete [] minor[i];
    delete [] minor;
 
    return det;
}

// matrix inversioon
// the result is put in Y
void MatrixInversion(real32 **A, int order, real32 **Y)
{
    // get the determinant of a
    double det = 1.0/CalcDeterminant(A,order);
 
    // memory allocation
    real32 *temp = new real32[(order-1)*(order-1)];
    real32 **minor = new real32*[order-1];
    for(int i=0;i<order-1;i++)
        minor[i] = temp+(i*(order-1));
 
    for(int j=0;j<order;j++)
    {
        for(int i=0;i<order;i++)
        {
            // get the co-factor (matrix) of A(j,i)
            GetMinor(A,minor,j,i,order);
            Y[i][j] = det*CalcDeterminant(minor,order-1);
            if( (i+j)%2 == 1)
                Y[i][j] = -Y[i][j];
        }
    }
 
    // release memory
    //delete [] minor[0];
    delete [] temp;
    delete [] minor;
}

matrix6 Inverse(matrix6* A)
{
    matrix6 Result;
    real32* ArrayStart = A->Array;
    real32* ArrayResult = Result.Array;
    MatrixInversion(&ArrayStart, 6, &ArrayResult);
    
    return Result;
}

//assumes v6 is column
v6 Matrix6ByV6(matrix6* Matrix, v6* Vector)
{
    v6 Result;
    
    Result.A = Inner(&Matrix->A, Vector);
    Result.B = Inner(&Matrix->B, Vector);
    Result.C = Inner(&Matrix->C, Vector);
    Result.D = Inner(&Matrix->D, Vector);
    Result.E = Inner(&Matrix->E, Vector);
    Result.F = Inner(&Matrix->F, Vector);
    
    return Result;
}
/*
v6 Matrix6ByV6Row(matrix6* Matrix, v6* Vector)
{
    v6 Result;
    
    v6 ColumnA = {Matrix->A.A,0,0,0,0,0};
    
    return Result;
}

*/

#endif

    inline plane
    PlaneFromPositionAndNormal(v3 Position, v3 Normal)
{
    Assert(ApproximatelyEqual(Length(Normal), 1.0f));

    plane Result = {};

    Result.A = Normal.x;
    Result.B = Normal.y;
    Result.C = Normal.z;
    Result.D = -Result.A * Position.x - Result.B * Position.y - Result.C * Position.z;

    return Result;
}

#define HANDMADE_MATH_H
#endif
