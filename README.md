This is a personal project 3d game engine by James Fulop. Please check out the "tags" 
section for executable downloads and videos of features. Much of the design
of this project is inspired by Handmade Hero. This is currently not intended for 
general use, but may serve as a useful reference for others. Feel free to message me. 
